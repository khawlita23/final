/**
 * Created by Hamster on 21.05.2017.
 */
var modal = document.getElementById('authModal');
// var btn = document.getElementById('logInBtn');
var close = document.getElementsByClassName('modal-close')[0];

// btn.onclick = function() {
//     modal.style.display = "block";
// }

function showModal() {
    modal.style.display = "block";
}

close.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}