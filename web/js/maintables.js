/**
 * Created by Hamster on 26.03.2017.
 */
/*For tables*/
var pop = document.getElementById('pop');
var soon = document.getElementById('soon');
var popTable = document.getElementById('popTable');
var soonTable = document.getElementById('soonTable');

pop.onclick = function() {
    popTable.style.display = "block";
    soonTable.style.display = "none";
    this.setAttribute("class", "active");
    soon.setAttribute("class", "");
}

soon.onclick = function() {
    soonTable.style.display = "block";
    popTable.style.display = "none";
    this.setAttribute("class", "active");
    pop.setAttribute("class", "");
}
