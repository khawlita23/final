/**
 * Created by Hamster on 04.06.2017.
 */
/**
 * Created by Hamster on 04.06.2017.
 */
function validateForm() {
    var result = true;

    var formId = "editMatchForm";
    var editMatchForm = document.getElementById(formId);

    var errMName = document.getElementById("errMName"),
        errMDate = document.getElementById("errMDate");

    var mName = editMatchForm["matchName"].value,
        mDate = editMatchForm["matchDate"].value;

    errMName.innerHTML = "";
    errMDate.innerHTML = "";

    //Check match name
    if (!mName) {
        errMName.innerHTML = FILL_FIELD;
        result = false;
    }
    if (mName && mName.search(/[А-ЯЁа-яё]+/) !== 0) {
        errMName.innerHTML = MATCH_NAME;
        result = false;
    }

    //Check date
    if (!mDate) {
        errMDate.innerHTML = FILL_FIELD;
        result = false;
    }
    if (mDate && mDate.search(/^(20\d{2})-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])\s(00|0?[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])$/) !== 0) {
        errMDate.innerHTML = MATCH_DATE;
        result = false;
    }

    return result;
}
