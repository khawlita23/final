/**
 * Created by Hamster on 21.05.2017.
 */
/*var changePassBtn = document.getElementById("changePassBtn");
var block = document.getElementById("changePassBlock");

changePassBtn.onclick = function () {
    if (block.style.display == "block") {
        block.style.display = "none";
    } else {
        block.style.display = "block";
    }
}*/

function showBetWindow(bet) {

}

function showChangePass() {
    var block = document.getElementById("changePassBlock");
    if (block.style.display == "block") {
        block.style.display = "none";
    } else {
        block.style.display = "block";
    }
}

function showBets() {
    var betsTable = document.getElementById("betsTable");

    if (betsTable.style.display == "block") {
        betsTable.style.display = "none";
    } else {
        betsTable.style.display = "block";
    }

}

function showUserInfo() {
    var userInfo = document.getElementById("userInfo");
    var block = document.getElementById("changePassBlock");
    if (userInfo.style.display == "block") {
        userInfo.style.display = "none";
        block.style.display = "none";
    } else {
        userInfo.style.display = "block";
    }
}

function validateForm() {

    var result = true;

   /*var FILL_FIELD = "Заполните поле",
        WRONG_PWD = "Пароль должен содержать не менее одной буквы в каждом регистре и не менее одной цифры",
        AT_LEAST_6 = "Пароль должен быть не короче 6 символов",
        EQUAL = "Пароли должны быть разные";*/

    var errCurrPswrd = document.getElementById("errCurrPswrd"),
        errNewPswrd = document.getElementById("errNewPswrd");

    var currentPswrd = document.forms["changePswrd"]["currentPassword"].value,
        newPswrd = document.forms["changePswrd"]["newPassword"].value;

    errCurrPswrd.innerHTML = "";
    errNewPswrd.innerHTML = "";

    if (!currentPswrd) {
        errCurrPswrd.innerHTML = FILL_FIELD;
        result = false;
    }

    if (!newPswrd) {
        errNewPswrd.innerHTML = FILL_FIELD;
        result = false;
    }
    if (newPswrd && (newPswrd.search(/[a-z]+/) == -1 || newPswrd.search(/[A-Z]+/) == -1 || newPswrd.search(/[0-9]+/) == -1)) {
        errNewPswrd.innerHTML = WRONG_PWD;
        result = false;
    }
    if (newPswrd && newPswrd.search(/[\w]{6,}/) == -1) {
        errNewPswrd.innerHTML = AT_LEAST_6;
        result = false;
    }
    if (currentPswrd && newPswrd && currentPswrd == newPswrd) {
        errNewPswrd.innerHTML = EQUAL;
        document.forms["changePswrd"]["currentPassword"].value = "";
        document.forms["changePswrd"]["newPassword"].value = "";
        result = false;
    }
    return result;
}