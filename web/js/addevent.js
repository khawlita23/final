/**
 * Created by Hamster on 04.06.2017.
 */
function validateForm() {
    var result = true;

    var formId = "addEventForm";
    var addEventForm = document.getElementById(formId);

    var errCategory = document.getElementById("errCategory"),
        errEName = document.getElementById("errEName"),
        errEPlace = document.getElementById("errEPlace");

    var category = addEventForm["category"].value,
        eName = addEventForm["eventName"].value,
        ePlace = addEventForm["eventPlace"].value;

    errCategory.innerHTML = "";
    errEName.innerHTML = "";
    errEPlace.innerHTML = "";

    //Check category
    if (!category) {
        errCategory.innerHTML = FILL_FIELD;
        result = false;
    }

    //Check event name
    if (!eName) {
        errEName.innerHTML = FILL_FIELD;
        result = false;
    }
    /*if (eName && eName.search(/[А-ЯЁа-яё]+/) !== 0) {
        errEName.innerHTML = EVENT_NAME;
        result = false;
    }*/

    //Check place
    if (!ePlace) {
        errEPlace.innerHTML = FILL_FIELD;
        result = false;
    }
    if (ePlace && ePlace.search(/[А-ЯЁа-яё]+/) !== 0) {
        errEPlace.innerHTML = EVENT_PLACE;
        result = false;
    }

    return result;
}
