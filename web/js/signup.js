/**
 * Created by Hamster on 26.03.2017.
 */
function validateForm() {

    var result = true;

    /*var FILL_FIELD = "${fillFieldMsg}", /!*Заполните поле*!/
        AT_LEAST_5 = "Логин должен быть не короче 5 символов",
        LOGIN = "Логин должен состоять из латинских букв, цифр и нижнего подчеркивания, " +
            "первый символ - латинская буква",
        F_NAME = "Пожалуйста, укажите Ваше настоящее имя",
        L_NAME = "Пожалуйста, укажите Вашу настоящую фамилию",
        PWD_NOT_EQUAL = "Не совпадают значения паролей",
        WRONG_PWD = "Пароль должен содержать не менее одной буквы в каждом регистре и не менее одной цифры",
        AT_LEAST_6 = "Пароль должен быть не короче 6 символов",
        INVALID_EMAIL = "Некорректный e-mail";*/

    var formId = "signUpForm";
    var signUpForm = document.getElementById("signUpForm");

    var errFname = document.getElementById("errFname"),
        errLname = document.getElementById("errLname"),
        errBirthdate = document.getElementById("errBirthdate"),
        errEmail = document.getElementById("errEmail"),
        errLogin = document.getElementById("errLogin"),
        errPswrd1 = document.getElementById("errPswrd1"),
        errPswrd2 = document.getElementById("errPswrd2");

    var fname = signUpForm["fname"].value,
        lname = signUpForm["lname"].value,
        bday = signUpForm["bday"].value,
        bmonth = signUpForm["bmonth"].value,
        byear = signUpForm["byear"].value,
        emails = document.getElementsByName("email"),
        login = signUpForm["login"].value,
        pswrd1 = signUpForm["pswrd1"].value,
        pswrd2 = signUpForm["pswrd2"].value;

    var pswrdInput1 = signUpForm["pswrd1"],
        pswrdInput2 = signUpForm["pswrd2"];

    errFname.innerHTML = "";
    errLname.innerHTML = "";
    errBirthdate.innerHTML = "";
    var i;
    for (i = 0; i < emails.length; i++) {
        emails[i].parentNode.nextElementSibling.nextElementSibling.innerHTML = "";
    }
    errLogin.innerHTML = "";
    errPswrd1.innerHTML = "";
    errPswrd2.innerHTML = "";

    //Check the first name
    if (!fname) {
        errFname.innerHTML = FILL_FIELD;
        pswrdInput1.value = "";
        pswrdInput2.value = "";
        result = false;
    }
    if (fname && fname.search(/^[A-ZА-ЯЁ]{1}[a-zа-яё]+$/) !== 0) {
        errFname.innerHTML = F_NAME;
        pswrdInput1.value = "";
        pswrdInput2.value = "";
        result = false;
    }

    //Check the last name (same as with the first name)
    if (!lname) {
        errLname.innerHTML = FILL_FIELD;
        pswrdInput1.value = "";
        pswrdInput2.value = "";
        result = false;
    }
    if (lname && lname.search(/^[A-ZА-ЯЁ]{1}[a-zа-яё]+$/) !== 0) {
        errLname.innerHTML = L_NAME;
        pswrdInput1.value = "";
        pswrdInput2.value = "";
        result = false;
    }

    //Check the birthdate
    if (!bday || !bmonth || !byear) {
        errBirthdate.innerHTML = FILL_FIELD;
        pswrdInput1.value = "";
        pswrdInput2.value = "";
        result = false;
    }

    //Check the e-mail
    for (i = 0; i < emails.length; i++) {
        var value = emails[i].value;
        if (!value) {
            emails[i].parentNode.nextElementSibling.nextElementSibling.innerHTML = FILL_FIELD;
            pswrdInput1.value = "";
            pswrdInput2.value = "";
            result = false;
        }
        if (value && value.search(/^[a-zA-Z0-9.,_%+-]+@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}$/i) !== 0) {
            emails[i].parentNode.nextElementSibling.nextElementSibling.innerHTML = INVALID_EMAIL;
            pswrdInput1.value = "";
            pswrdInput2.value = "";
            result = false;
        }
    }

    //Check the login
    if (!login) {
        errLogin.innerHTML = FILL_FIELD;
        pswrdInput1.value = "";
        pswrdInput2.value = "";
        result = false;
    }
    if (login && login.search(/[\w]{5,}/i) !== 0) {
        errLogin.innerHTML = AT_LEAST_5;
        pswrdInput1.value = "";
        pswrdInput2.value = "";
        result = false;
    }
    if (login && login.search(/^[a-zA-Z]+\w+/) !== 0) {
        errLogin.innerHTML = LOGIN;
        pswrdInput1.value = "";
        pswrdInput2.value = "";
        result = false;
    }

    //Check passwords
    if (!pswrd1) {
        errPswrd1.innerHTML = FILL_FIELD;
        result = false;
    }
    if (pswrd1 && (pswrd1.search(/[a-z]+/) == -1 || pswrd1.search(/[A-Z]+/) == -1 || pswrd1.search(/[0-9]+/) == -1)) {
        errPswrd1.innerHTML = WRONG_PWD;
        result = false;
    }
    if (pswrd1 && pswrd1.search(/[\w]{6,}/) == -1) {
        errPswrd1.innerHTML = AT_LEAST_6;
        result = false;
    }
    if (!pswrd2) {
        errPswrd2.innerHTML = FILL_FIELD;
        result = false;
    }
    if (pswrd1 && pswrd2 && pswrd1 !== pswrd2) {
        errPswrd2.innerHTML = PWD_NOT_EQUAL;
        pswrdInput1.value = "";
        pswrdInput2.value = "";
        result = false;
    }

    return result;
}


function addEmail() {

    var label = document.createElement("label"),
        input = document.createElement("input"),
        button = document.createElement("button"),
        span = document.createElement("span"),
        br = document.createElement("br");

    input.setAttribute("type", "text");
    input.setAttribute("name", "email");

    label.innerHTML = EMAIL_LABEL + ":";
    label.appendChild(input);

    button.setAttribute("onclick", "return deleteEmail(this)");
    button.innerHTML = DELETE_EMAIL_BTN;

    span.setAttribute("class", "error");

    var loginField = document.getElementById("loginField");
    var form = document.getElementById("signUpForm");

    form.insertBefore(label, loginField);
    form.insertBefore(button, loginField);
    form.insertBefore(span, loginField);
    form.insertBefore(br, loginField);

    return false;
}


function deleteEmail(elem) {

    elem.previousElementSibling.remove();
    var span = elem.nextElementSibling;
    span.nextElementSibling.remove();
    span.remove();
    elem.remove();

    return false;
}
