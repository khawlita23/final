<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 04.06.2017
  Time: 22:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="title.errorpage" var="errorPageTitle"/>
<fmt:message bundle="${locale}" key="message.servererror" var="serverErrorMsg"/>
<fmt:message bundle="${locale}" key="text.requesturi" var="requestURIText"/>
<fmt:message bundle="${locale}" key="text.servletname" var="servletNameText"/>
<fmt:message bundle="${locale}" key="text.statuscode" var="statusCodeText"/>
<fmt:message bundle="${locale}" key="text.throwable" var="throwableText"/>
<fmt:message bundle="${locale}" key="text.throwablemessage" var="throwableMessageText"/>
<fmt:message bundle="${locale}" key="button.gotomain" var="goToMainBtn"/>

<html>
<head>
    <title>${errorPageTitle}</title>
</head>
<body>
<h1>${serverErrorMsg}</h1>
<h2>${requestScope.errorInfo}</h2>
<p>
    ${requestURIText}: ${pageContext.errorData.requestURI}
</p>
<p>
    ${servletNameText}: ${pageContext.errorData.servletName}
</p>
<p>
    ${statusCodeText}: ${pageContext.errorData.statusCode}
</p>
<c:if test="${pageContext.errorData.throwable ne null}">
    <p>
            ${throwableText}: ${pageContext.errorData.throwable}
    </p>
</c:if>
<c:if test="${pageContext.errorData.throwable.message ne null}">
    <p>
            ${throwableMessageText}: ${pageContext.errorData.throwable.message}
    </p>
</c:if>
<div class="button-wrap">
    <form action="${pageContext.request.contextPath}/controller" method="POST">
        <input type="hidden" name="command" value="gotomain">
        <input type="submit" value="${goToMainBtn}">
    </form>
</div>
</body>
</html>
