<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 12.05.2017
  Time: 16:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="text.matches" var="matchesText"/>
<fmt:message bundle="${locale}" key="text.date" var="dateText"/>
<fmt:message bundle="${locale}" key="text.name" var="nameText"/>
<fmt:message bundle="${locale}" key="text.place" var="placeText"/>
<fmt:message bundle="${locale}" key="text.outcome" var="outcomeText"/>
<fmt:message bundle="${locale}" key="text.stake" var="stakeText"/>
<fmt:message bundle="${locale}" key="text.makeabet" var="makeABetText"/>
<fmt:message bundle="${locale}" key="button.makeabet" var="makeABetBtn"/>
<fmt:message bundle="${locale}" key="message.betduplicate" var="betDuplicateMsg"/>
<fmt:message bundle="${locale}" key="message.servererror" var="serverErrorMsg"/>
<fmt:message bundle="${locale}" key="button.results" var="resultsBtn"/>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/headers.css">
    <link rel="stylesheet" href="../css/table.css">
    <link rel="stylesheet" href="../css/matches.css">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather|Roboto+Slab|Tinos" rel="stylesheet">
    <title>${makeABetText}</title>
</head>
<body>
<%@include file="reusable/headers.jsp" %>
<div class="row clearfix">
    <div class="main col-12">
        <div class="header-w-button clearfix">
            <h2>${matchesText} - ${sessionScope.category}</h2>
            <form action="controller" method="POST">
                <input type="hidden" name="command" value="gotoresults">
                <input type="hidden" name="category" value="${sessionScope.category}">
                <input type="submit" value="${resultsBtn}" class="bet-button">
            </form>
        </div>
        <c:choose>
            <c:when test="${requestScope.alreadyExists eq true}">
                <h3 class="error">${betDuplicateMsg}</h3>
            </c:when>
            <c:when test="${requestScope.serverError ne null}">
                <h3 class="error">${serverErrorMsg}</h3>
            </c:when>
        </c:choose>
        <div class="tables">
            <table>
                <tr>
                    <th rowspan="2">${dateText}</th>
                    <th rowspan="2">${nameText}</th>
                    <th rowspan="2">${placeText}</th>
                    <th colspan="3">${makeABetText}</th>
                </tr>
                <tr>
                    <th>${outcomeText}</th>
                    <th>${stakeText}</th>
                    <th></th>
                </tr>
                <c:forEach items="${sessionScope.matchesList}" var="match">
                    <tr>
                        <td>
                            <c:out value="${fn:substring(match.date, 0, 16)}"/>
                        </td>
                        <td>
                            <div class="cat"><c:out value="${match.event.name}"/></div>
                            <c:out value="${match.name}"/>
                        </td>
                        <td>
                            <c:out value="${match.event.place}"/>
                        </td>
                        <td>
                            <form action="controller"
                                  method="POST" id="form${match.matchId}">
                                <input type="hidden" name="command" value="makeabet"/>
                                <input type="hidden" name="matchId" value="${match.matchId}"/>
                                <select name="outcomeId" form="form${match.matchId}">
                                    <option></option>
                                    <c:forEach items="${sessionScope.outcomesList}" var="outcome">
                                        <option value="${outcome.outcomeId}">${outcome.value}</option>
                                    </c:forEach>
                                </select>
                            </form>
                        </td>
                        <td>
                            <input type="text" name="stake" form="form${match.matchId}"/>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${sessionScope.user != null}">
                                    <c:set var="betBtnType" value="submit" scope="page"/>
                                    <c:set var="betBtnOnclick" value="" scope="page"/>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="betBtnType" value="button" scope="page"/>
                                    <c:set var="betBtnOnclick" value="showModal()" scope="page"/>
                                </c:otherwise>
                            </c:choose>
                            <input type="${pageScope.betBtnType}" onclick="${pageScope.betBtnOnclick}"
                                   value="${makeABetBtn}" class="bet-button"
                                   form="form${match.matchId}"/>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
<script src="../js/modal.js"></script>
</body>
</html>
