<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 21.05.2017
  Time: 13:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="button.personaldata" var="personalDataBtn"/>
<fmt:message bundle="${locale}" key="text.category" var="categoryText"/>
<fmt:message bundle="${locale}" key="text.match" var="matchText"/>
<fmt:message bundle="${locale}" key="text.stake" var="stakeText"/>
<fmt:message bundle="${locale}" key="text.outcome" var="outcomeText"/>
<fmt:message bundle="${locale}" key="label.firstname" var="firstNameLabel"/>
<fmt:message bundle="${locale}" key="label.lastname" var="lastNameLabel"/>
<fmt:message bundle="${locale}" key="label.birthdate" var="birthdateLabel"/>
<fmt:message bundle="${locale}" key="label.email" var="emailLabel"/>
<fmt:message bundle="${locale}" key="label.login" var="loginLabel"/>
<fmt:message bundle="${locale}" key="label.password" var="passwordLabel"/>
<fmt:message bundle="${locale}" key="label.currentpassword" var="currentPasswordLabel"/>
<fmt:message bundle="${locale}" key="label.newpassword" var="newPasswordLabel"/>
<fmt:message bundle="${locale}" key="button.changepassword" var="changePasswordBtn"/>
<fmt:message bundle="${locale}" key="message.fillfield" var="fillFieldMsg"/>
<fmt:message bundle="${locale}" key="message.passwordrestriction" var="passwordRestrictionMsg"/>
<fmt:message bundle="${locale}" key="message.passwordlength" var="passwordLengthMsg"/>
<fmt:message bundle="${locale}" key="message.passwordsequal" var="passwordsEqualMsg"/>
<fmt:message bundle="${locale}" key="title.userprofile" var="userProfileTitle"/>
<fmt:message bundle="${locale}" key="message.nobetsyet" var="noBetsYet"/>
<fmt:message bundle="${locale}" key="message.betdeleted" var="betDeletedMsg"/>
<fmt:message bundle="${locale}" key="message.betnotdeleted" var="betNotDeletedMsg"/>
<c:choose>
    <c:when test="${sessionScope.role ne 'admin'}">
        <fmt:message bundle="${locale}" key="button.mybets" var="betsListBtn"/>
    </c:when>
    <c:otherwise>
        <fmt:message bundle="${locale}" key="button.clientsbets" var="betsListBtn"/>
    </c:otherwise>
</c:choose>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/headers.css">
    <link rel="stylesheet" href="../css/menus.css">
    <link rel="stylesheet" href="../css/table.css">
    <link rel="stylesheet" href="../css/userprofile.css">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather|Roboto+Slab|Tinos" rel="stylesheet">
    <title>${userProfileTitle}</title>
</head>
<body>
<%@include file="reusable/headers.jsp" %>
<div class="row clearfix">
    <%@include file="reusable/leftmenu.jsp" %>
    <div class="main col-6">
        <h2>${sessionScope.client.firstName} ${sessionScope.client.lastName}</h2>
        <span>${requestScope.passwordChanged}</span>
        <input class="show-btn" type="button" value="${betsListBtn}" onclick="showBets()">
        <div class="bets-table" id="betsTable">
            <%--<h2>Ваши ставки</h2>--%>
            <c:choose>
                <c:when test="${not empty sessionScope.betsList}">
                    <c:choose>
                        <c:when test="${requestScope.success eq true}">
                            <span>${betDeletedMsg}</span>
                        </c:when>
                        <c:when test="${requestScope.success eq false}">
                            <span>${betNotDeletedMsg}</span>
                        </c:when>
                    </c:choose>
                    <table>
                        <tr>
                            <th>${categoryText}</th>
                            <th>${matchText}</th>
                            <th>${stakeText}</th>
                            <th>${outcomeText}</th>
                            <c:if test="${sessionScope.role ne 'admin'}">
                                <th></th>
                            </c:if>
                        </tr>
                        <c:forEach items="${sessionScope.betsList}" var="bet">
                            <tr>
                                <td>
                                    <c:out value="${bet.match.event.category}"/>
                                </td>
                                <td>
                                    <div class="cat"><c:out value="${bet.match.event.name}"/></div>
                                    <c:out value="${bet.match.name}"/>
                                </td>
                                <td>
                                    <c:out value="${bet.stake}"/>
                                </td>
                                <td>
                                    <c:out value="${bet.outcome.value}"/>
                                </td>
                                <c:if test="${sessionScope.role ne 'admin'}">
                                    <td>
                                        <form action="controller" method="POST">
                                            <input type="hidden" name="command" value="deletebet">
                                            <input type="hidden" name="clientId" value="${sessionScope.client.clientId}">
                                            <input type="hidden" name="matchId" value="${bet.match.matchId}">
                                            <input class="delete-bet-btn" type="submit" value="×">
                                        </form>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                    </table>
                </c:when>
                <c:otherwise>
                    <span>${noBetsYet}</span>
                </c:otherwise>
            </c:choose>
        </div>
        <input class="show-btn" type="button" value="${personalDataBtn}" onclick="showUserInfo()">
        <div class="user-info" id="userInfo">
            <span>${requestScope.wrongPassword}</span>
            <table>
                <tr>
                    <td>${firstNameLabel}</td>
                    <td>${sessionScope.client.firstName}</td>
                </tr>
                <tr>
                    <td>${lastNameLabel}</td>
                    <td>${sessionScope.client.lastName}</td>
                </tr>
                <tr>
                    <td>${birthdateLabel}</td>
                    <td>${sessionScope.client.birthDate}</td>
                </tr>
                <tr>
                    <td>${emailLabel}</td>
                    <td>${sessionScope.client.email}</td>
                </tr>
                <tr>
                    <td>${loginLabel}</td>
                    <td>${sessionScope.client.login}</td>
                </tr>
                <c:if test="${sessionScope.role ne 'admin'}">
                    <tr>
                        <td>${passwordLabel}</td>
                        <td>
                            <div class="button-wrap">
                                <input type="button" name="showChangePassword" value="${changePasswordBtn}"
                                       onclick="showChangePass()">
                            </div>
                        </td>
                    </tr>
                </c:if>
            </table>
        </div>
        <div class="change-pass" id="changePassBlock">
            <form action="controller" method="POST"
                  name="changePswrd" onsubmit="return validateForm()">
                <input type="hidden" name="command" value="changepassword">
                <table>
                    <tr>
                        <td><label for="current">${currentPasswordLabel}:</label></td>
                        <td><input type="password" name="currentPassword" id="current"></td>
                        <td><span id="errCurrPswrd" class="error"></span></td>
                    </tr>
                    <tr>
                        <td><label for="new">${newPasswordLabel}:</label></td>
                        <td><input type="password" name="newPassword" id="new"></td>
                        <td><span id="errNewPswrd" class="error"></span></td>
                    </tr>
                </table>
                <div class="button-wrap">
                    <input name="changePassword" type="submit" value="${changePasswordBtn}">
                </div>
            </form>
        </div>

    </div>
    <%@include file="reusable/rightmenu.jsp" %>
</div>
<script src="../js/modal.js"></script>
<script>
    var FILL_FIELD = "${fillFieldMsg}",
        WRONG_PWD = "${passwordRestrictionMsg}",
        AT_LEAST_6 = "${passwordLengthMsg}",
        EQUAL = "${passwordsEqualMsg}";
</script>
<script src="../js/userprofile.js"></script>
</body>
</html>
