<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 09.05.2017
  Time: 19:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="text.logintoaccount" var="logInToAccountText"/>
<fmt:message bundle="${locale}" key="placeholder.login" var="loginPlaceholder"/>
<fmt:message bundle="${locale}" key="placeholder.password" var="passwordPlaceholder"/>
<fmt:message bundle="${locale}" key="button.login" var="logInBtn"/>
<fmt:message bundle="${locale}" key="label.loginasadmin" var="logInAsAdminLabel"/>
<fmt:message bundle="${locale}" key="message.signedup" var="signedUpMsg"/>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/headers.css">
    <link rel="stylesheet" href="../css/menus.css">
    <link rel="stylesheet" href="../css/login.css">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather|Roboto+Slab|Tinos" rel="stylesheet">
    <title>${logInToAccountText}</title>
</head>
<body>
<%@include file="reusable/headers.jsp"%>
<div class="row clearfix">
    <%@include file="reusable/leftmenu.jsp"%>
    <div class="main col-6">
        <h2>${logInToAccountText}</h2>
        <c:if test="${requestScope.success eq true}">
            <span style="margin-left: 30px;">${signedUpMsg}</span>
        </c:if>
        <form name="loginForm" action="controller" method="POST">
            <input type="hidden" name="command" value="login"/>
            <div class="form">
                <input type="text" name="login" placeholder="${loginPlaceholder}"/>
            </div>
            <div class="form">
                <input type="password" name="password" placeholder="${passwordPlaceholder}"/>
            </div>
            <div class="form">
                <label><input type="checkbox" name="admin" value="asAdmin"/>${logInAsAdminLabel}</label>
            </div>
            <br/>
            <div class="message">${requestScope.errorLoginPassMessage}</div>
            <br/>
            <input type="submit" name="loginBtn" value="${logInBtn}"/>
        </form>
    </div>
    <%@include file="reusable/rightmenu.jsp"%>
</div>
<script src="../js/modal.js"></script>
</body>
</html>