<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 03.06.2017
  Time: 22:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="text.matches" var="matchesText"/>
<fmt:message bundle="${locale}" key="text.date" var="dateText"/>
<fmt:message bundle="${locale}" key="text.name" var="nameText"/>
<fmt:message bundle="${locale}" key="text.place" var="placeText"/>
<fmt:message bundle="${locale}" key="text.outcome" var="outcomeText"/>
<fmt:message bundle="${locale}" key="text.totalstakes" var="totalStakesText"/>
<fmt:message bundle="${locale}" key="text.odds" var="oddsText"/>
<fmt:message bundle="${locale}" key="button.edit" var="editBtn"/>
<fmt:message bundle="${locale}" key="text.matchnotfinished" var="matchNotFinishedText"/>
<fmt:message bundle="${locale}" key="button.addmatch" var="addMatchBtn"/>
<fmt:message bundle="${locale}" key="button.addevent" var="addEventBtn"/>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/headers.css">
    <link rel="stylesheet" href="../css/table.css">
    <link rel="stylesheet" href="../css/matches.css">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather|Roboto+Slab|Tinos" rel="stylesheet">
    <title>${matchesText}</title>
</head>
<body>
<%@include file="reusable/headers.jsp" %>
<div class="row clearfix">
    <div class="main col-12">
        <h2>${matchesText} - ${sessionScope.category}</h2>
        <div class="tables">
            <table>
                <tr>
                    <th>${dateText}</th>
                    <th>${nameText}</th>
                    <th>${placeText}</th>
                    <th>${outcomeText}</th>
                    <th>${totalStakesText}</th>
                    <th></th>
                </tr>
                <c:forEach items="${sessionScope.matchesList}" var="match">
                    <tr>
                        <td>
                            <c:out value="${fn:substring(match.date, 0, 16)}"/>
                        </td>
                        <td>
                            <div class="cat"><c:out value="${match.event.name}"/></div>
                            <c:out value="${match.name}"/>
                        </td>
                        <td>
                            <c:out value="${match.event.place}"/>
                        </td>
                        <td>
                            <c:set var="outcomeValue" value="${match.outcome.value}"/>
                            <c:choose>
                                <c:when test="${outcomeValue != null}">
                                    <c:out value="${outcomeValue}"/>
                                </c:when>
                                <c:otherwise>
                                    <c:out value="${matchNotFinishedText}"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <c:set var="totalStakes" value="${sessionScope.totalStakesMap.get(match.matchId)}"/>
                            <c:choose>
                                <c:when test="${totalStakes != null}">
                                    <c:out value="${totalStakes}"/>
                                </c:when>
                                <c:otherwise>
                                    <c:out value="0"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <form action="controller" method="POST">
                                <input type="hidden" name="command" value="gotoeditmatch"/>
                                <input type="hidden" name="matchId" value="${match.matchId}"/>
                                <input type="submit" value="${editBtn}" class="bet-button"/>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <div class="button-wrap">
                <form action="controller" method="POST">
                    <input type="hidden" name="command" value="gotoaddmatch">
                    <input type="submit" value="${addMatchBtn}" class="bet-button">
                </form>
                <form action="controller" method="POST">
                    <input type="hidden" name="command" value="gotoaddevent">
                    <input type="submit" value="${addEventBtn}" class="bet-button">
                </form>
            </div>
        </div>
    </div>
</div>
<script src="../js/modal.js"></script>
</body>
</html>

