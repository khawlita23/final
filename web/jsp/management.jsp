<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 03.06.2017
  Time: 21:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="title.management" var="managementTitle"/>
<fmt:message bundle="${locale}" key="text.firstname" var="firstNameText"/>
<fmt:message bundle="${locale}" key="text.lastname" var="lastNameText"/>
<fmt:message bundle="${locale}" key="text.birthdate" var="birthDateText"/>
<fmt:message bundle="${locale}" key="text.email" var="emailText"/>
<fmt:message bundle="${locale}" key="text.login" var="loginText"/>
<html>
<head>
    <meta charset="utf-8">
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/headers.css">
    <link rel="stylesheet" href="../css/table.css">
    <link rel="stylesheet" href="../css/clients.css">
    <link rel="stylesheet" href="../css/pagination.css">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather|Roboto+Slab|Tinos" rel="stylesheet">
    <title>${managementTitle}</title>
</head>
<body>
<%@include file="reusable/headers.jsp" %>
<div class="row clearfix">
    <div class="row clearfix">
        <div class="main col-12">
            <div class="tables">
                <%--<c:set var="headings" value="${[loginText, firstNameText, lastNameText, birthDateText, emailText]}"/>--%>
                <%--<ctg:clients-table headings="${headings}" clientslist="${requestScope.clientsList}"/>--%>
                <table>
                    <tr>
                        <th>${loginText}</th>
                        <th>${firstNameText}</th>
                        <th>${lastNameText}</th>
                        <th>${birthDateText}</th>
                        <th>${emailText}</th>
                    </tr>
                    <c:forEach items="${requestScope.clientsList}" var="client">
                        <tr>
                            <td>
                                <form action="controller" method="POST">
                                    <input type="hidden" name="command" value="gotouserprofile">
                                    <input type="hidden" name="clientId" value="${client.clientId}">
                                    <input type="submit" value="${client.login}" class="login-link">
                                </form>
                                    <%--<c:out value="${client.login}"/>--%>
                            </td>
                            <td><c:out value="${client.firstName}"/></td>
                            <td><c:out value="${client.lastName}"/></td>
                            <td><c:out value="${client.birthDate}"/></td>
                            <td><c:out value="${client.email}"/></td>
                        </tr>
                    </c:forEach>
                </table>

            </div>
            <div class="pagination">
                <form action="controller" method="POST">
                    <div class="centered clearfix">
                        <input type="hidden" name="command" value="gotomanagement">
                        <c:if test="${requestScope.currentPage ne 1}">
                            <button type="submit" name="toPage" value="${requestScope.currentPage - 1}">&laquo;</button>
                        </c:if>
                        <c:forEach begin="1" end="${requestScope.numberOfPages}" var="i">
                            <c:choose>
                                <c:when test="${requestScope.currentPage eq i}">
                                    <input class="active" type="button" name="" value="${i}">
                                </c:when>
                                <c:otherwise>
                                    <input type="submit" name="toPage" value="${i}">
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${requestScope.currentPage < requestScope.numberOfPages}">
                            <button type="submit" name="toPage" value="${requestScope.currentPage + 1}">&raquo;</button>
                        </c:if>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="../js/modal.js"></script>
</body>
</html>
