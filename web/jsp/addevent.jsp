<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 04.06.2017
  Time: 15:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="title.addevent" var="addEventTitle"/>
<fmt:message bundle="${locale}" key="text.addevent" var="addEventText"/>
<fmt:message bundle="${locale}" key="label.eventname" var="eventNameLabel"/>
<fmt:message bundle="${locale}" key="label.eventplace" var="eventPlaceLabel"/>
<fmt:message bundle="${locale}" key="label.category" var="categoryLabel"/>
<fmt:message bundle="${locale}" key="button.add" var="addBtn"/>
<fmt:message bundle="${locale}" key="message.fillfield" var="fillFieldMsg"/>
<fmt:message bundle="${locale}" key="message.incorrecteventname" var="incorrectEventNameMsg"/>
<fmt:message bundle="${locale}" key="message.incorrecteventplace" var="incorrectEventPlaceMsg"/>
<fmt:message bundle="${locale}" key="message.eventadded" var="eventAddedMsg"/>
<fmt:message bundle="${locale}" key="message.eventalreadyexists" var="eventAlreadyExistsMsg"/>
<fmt:message bundle="${locale}" key="text.tennis" var="tennisText"/>
<fmt:message bundle="${locale}" key="text.football" var="footballText"/>
<fmt:message bundle="${locale}" key="text.volleyball" var="volleyballText"/>
<fmt:message bundle="${locale}" key="text.basketball" var="basketballText"/>
<fmt:message bundle="${locale}" key="text.handball" var="handballText"/>
<fmt:message bundle="${locale}" key="text.hockey" var="hockeyText"/>
<fmt:message bundle="${locale}" key="text.esports" var="esportsText"/>
<fmt:message bundle="${locale}" key="text.cycling" var="cyclingText"/>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/headers.css">
    <link rel="stylesheet" href="../css/addmatch.css">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather|Roboto+Slab|Tinos" rel="stylesheet">
    <title>${addEventTitle}</title>
</head>
<body>
<%@include file="reusable/headers.jsp" %>
<div class="row clearfix">
    <div class="col-2"></div>
    <div class="main col-8">
        <h2>${addEventText}</h2>
        <div class="form-wrap">
            <c:if test="${requestScope.alreadyExists == true}">
                <div style="text-align: center; color: #f00;">
                    <span>${eventAlreadyExistsMsg}</span>
                </div>
            </c:if>
            <form onsubmit="return validateForm()" action="controller"
                  method="POST" class="clearfix" id="addEventForm">
                <input type="hidden" name="command" value="addevent"/>
                <label>${categoryLabel}:
                    <select name="category">
                        <option value=""></option>
                        <option value="Теннис">${tennisText}</option>
                        <option value="Футбол">${footballText}</option>
                        <option value="Волейбол">${volleyballText}</option>
                        <option value="Баскетбол">${basketballText}</option>
                        <option value="Гандбол">${handballText}</option>
                        <option value="Хоккей">${hockeyText}</option>
                        <option value="Киберспорт">${esportsText}</option>
                        <option value="Велоспорт">${cyclingText}</option>
                    </select>
                </label>
                <span class="error" id="errCategory"></span>
                <br>
                <label>${eventNameLabel}:<input type="text" name="eventName"></label>
                <span class="error" id="errEName"></span>
                <br>
                <label>${eventPlaceLabel}:<input type="text" name="eventPlace"></label>
                <span class="error" id="errEPlace"></span>
                <br>
                <input type="submit" value="${addBtn}" class="addmatch-button">
            </form>

            <c:if test="${requestScope.success == true}">
                <div style="text-align: center;">
                    <span>${eventAddedMsg}</span>
                </div>
            </c:if>

        </div>
    </div>
    <div class="col-2"></div>
</div>
<script>
    var FILL_FIELD = "${fillFieldMsg}",
        EVENT_NAME = "${incorrectEventNameMsg}",
        EVENT_PLACE = "${incorrectEventPlaceMsg}";
</script>
<script src="../js/modal.js"></script>
<script src="../js/addevent.js"></script>
</body>
</html>
