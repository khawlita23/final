<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 09.06.2017
  Time: 17:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="title.results" var="resultsTitle"/>
<fmt:message bundle="${locale}" key="text.results" var="resultsText"/>
<fmt:message bundle="${locale}" key="text.date" var="dateText"/>
<fmt:message bundle="${locale}" key="text.name" var="nameText"/>
<fmt:message bundle="${locale}" key="text.place" var="placeText"/>
<fmt:message bundle="${locale}" key="text.outcome" var="outcomeText"/>
<fmt:message bundle="${locale}" key="text.totalstakes" var="totalStakesText"/>
<fmt:message bundle="${locale}" key="message.nofinishedmatches" var="noFinishedMatchesMsg"/>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/headers.css">
    <link rel="stylesheet" href="../css/table.css">
    <link rel="stylesheet" href="../css/matches.css">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather|Roboto+Slab|Tinos" rel="stylesheet">
    <title>${resultsTitle}</title>
</head>
<body>
<%@include file="reusable/headers.jsp" %>
<div class="row clearfix">
    <div class="main col-12">
        <h2>${resultsText} - ${requestScope.category}</h2>
        <div class="tables">
            <c:choose>
                <c:when test="${not empty requestScope.matchesList}">
                    <c:set var="headings" value="${[dateText, nameText, placeText, outcomeText, totalStakesText]}"/>
                    <ctg:results-table headings="${headings}" resultslist="${requestScope.matchesList}"
                                       totalstakes="${requestScope.totalStakesMap}"/>
                </c:when>
                <c:otherwise>
                    <h3 style="margin-left: 15px;">${noFinishedMatchesMsg}</h3>
                </c:otherwise>
            </c:choose>
            <%--<table>
                <tr>
                    <th>${dateText}</th>
                    <th>${nameText}</th>
                    <th>${placeText}</th>
                    <th>${outcomeText}</th>
                    <th>${totalStakesText}</th>
                </tr>
                <c:forEach items="${requestScope.matchesList}" var="match">
                    <tr>
                        <td>
                            <c:out value="${fn:substring(match.date, 0, 16)}"/>
                        </td>
                        <td>
                            <div class="cat"><c:out value="${match.event.name}"/></div>
                            <c:out value="${match.name}"/>
                        </td>
                        <td>
                            <c:out value="${match.event.place}"/>
                        </td>
                        <td>
                            <c:out value="${match.outcome.value}"/>
                        </td>
                        <td style="border: 1px solid #000;">
                            <c:set var="totalStakes" value="${requestScope.totalStakesMap.get(match.matchId)}"/>
                            <c:choose>
                                <c:when test="${totalStakes != null}">
                                    <c:out value="${totalStakes}"/>
                                </c:when>
                                <c:otherwise>
                                    <c:out value="0.0"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:forEach>
            </table>--%>
        </div>
    </div>
</div>

</body>
</html>
