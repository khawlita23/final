<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 01.05.2017
  Time: 20:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="title.main" var="mainTitle"/>
<fmt:message bundle="${locale}" key="text.matches" var="matchesText"/>
<fmt:message bundle="${locale}" key="button.soon" var="soonBtn"/>
<fmt:message bundle="${locale}" key="button.popular" var="popularBtn"/>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/headers.css">
    <link rel="stylesheet" href="../css/menus.css">
    <link rel="stylesheet" href="../css/table.css">
    <link rel="stylesheet" href="../css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather|Roboto+Slab|Tinos" rel="stylesheet">
    <title>${mainTitle}</title>
</head>
<body>
<%@include file="reusable/headers.jsp" %>
<div class="row clearfix">
    <%@include file="reusable/leftmenu.jsp" %>
    <div class="main col-6">
        <div class="header-w-options clearfix">
            <h2>${matchesText}</h2>
            <div>
                <a id="pop" class="active">${popularBtn}</a>
                <a id="soon">${soonBtn}</a>
            </div>
        </div>
        <div class="tables">
            <div id="popTable">
                <table>
                    <tr>
                        <td>12.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Испания. Примера дивизион</div>
                            <a href="#">Реал Мадрид - Бетис</a>
                        </td>
                        <td>$130</td>
                    </tr>
                    <tr>
                        <td>12.03 22:00</td>
                        <td>
                            <div class="cat">Футбол. Франция. Лига 1</div>
                            <a href="#">Лорьян - ПСЖ</a>
                        </td>
                        <td>$200</td>
                    </tr>
                    <tr>
                        <td>12.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Италия. Серия А</div>
                            <a href="#">Палермо - Рома</a>
                        </td>
                        <td>$156</td>
                    </tr>
                    <tr>
                        <td>14.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Лига Чемпионов УЕФА</div>
                            <a href="#">Ювентус(ITA) - Порту(PRT)</a>
                        </td>
                        <td>$120</td>
                    </tr>
                    <tr>
                        <td>15.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Лига Чемпионов УЕФА</div>
                            <a href="#">Атлетико Мадрид(ESP) - Байер Л.(DEU)</a>
                        </td>
                        <td>$245</td>
                    </tr>
                    <tr>
                        <td>15.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Лига Чемпионов УЕФА</div>
                            <a href="#">Монако(FRA) - Манчестер Сити(ENG)</a>
                        </td>
                        <td>$187</td>
                    </tr>
                    <tr>
                        <td>14.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Лига Чемпионов УЕФА</div>
                            <a href="#">Лестер(ENG) - Севилья(ESP)</a>
                        </td>
                        <td>$130</td>
                    </tr>

                    <!--Повтор предыдущих-->
                    <tr>
                        <td>12.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Испания. Примера дивизион</div>
                            <a href="#">Реал Мадрид - Бетис</a>
                        </td>
                        <td>$130</td>
                    </tr>
                    <tr>
                        <td>12.03 22:00</td>
                        <td>
                            <div class="cat">Футбол. Франция. Лига 1</div>
                            <a href="#">Лорьян - ПСЖ</a>
                        </td>
                        <td>$200</td>
                    </tr>
                    <tr>
                        <td>12.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Италия. Серия А</div>
                            <a href="#">Палермо - Рома</a>
                        </td>
                        <td>$156</td>
                    </tr>
                    <tr>
                        <td>14.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Лига Чемпионов УЕФА</div>
                            <a href="#">Ювентус(ITA) - Порту(PRT)</a>
                        </td>
                        <td>$120</td>
                    </tr>
                    <tr>
                        <td>15.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Лига Чемпионов УЕФА</div>
                            <a href="#">Атлетико Мадрид(ESP) - Байер Л.(DEU)</a>
                        </td>
                        <td>$245</td>
                    </tr>
                    <tr>
                        <td>15.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Лига Чемпионов УЕФА</div>
                            <a href="#">Монако(FRA) - Манчестер Сити(ENG)</a>
                        </td>
                        <td>$187</td>
                    </tr>
                    <tr>
                        <td>14.03 21:45</td>
                        <td>
                            <div class="cat">Футбол. Лига Чемпионов УЕФА</div>
                            <a href="#">Лестер(ENG) - Севилья(ESP)</a>
                        </td>
                        <td>$130</td>
                    </tr>
                </table>
            </div>
            <div id="soonTable">
                <table>
                    <tr>
                        <td>00:02</td>
                        <td>
                            <div class="cat">Регби-7. Мировая Серия. Ванкувер</div>
                            <a href="#">Новая Зеландия-7 - Австралия-7</a>
                        </td>
                        <td>$450</td>
                    </tr>
                    <tr>
                        <td>00:06</td>
                        <td>
                            <div class="cat">Теннис. WTA. Индиан-Уэллс. Хард</div>
                            <a href="#">Свитолина Элина - Гаврилова Дарья</a>
                        </td>
                        <td>$370</td>
                    </tr>
                    <tr>
                        <td>00:15</td>
                        <td>
                            <div class="cat">Футбол. Коста-Рика. Примера дивизион</div>
                            <a href="#">Картахинес - УКР</a>
                        </td>
                        <td>$470</td>
                    </tr>
                    <tr>
                        <td>00:15</td>
                        <td>
                            <div class="cat">Хоккей. WHL</div>
                            <a href="#">Калгари Хитмен - Эдмонтон Ойл Кингз</a>
                        </td>
                        <td>$520</td>
                    </tr>
                    <tr>
                        <td>00:15</td>
                        <td>
                            <div class="cat">Хоккей. AHL</div>
                            <a href="#">Онтарио Рейгн - Тусон Роадраннерс</a>
                        </td>
                        <td>$455</td>
                    </tr>
                    <tr>
                        <td>00:16</td>
                        <td>
                            <div class="cat">Баскетбол. NBA. Статистика матча</div>
                            <a href="#">Бруклин Нетс - Нью-Йорк Никс</a>
                        </td>
                        <td>$587</td>
                    </tr>
                    <tr>
                        <td>00:16</td>
                        <td>
                            <div class="cat">Баскетбол. NBA. Игроки</div>
                            <a href="#">Индиана Пэйсерс - Майами Хит</a>
                        </td>
                        <td>$630</td>
                    </tr>

                    <!--Повтор предыдущих значений-->
                    <tr>
                        <td>00:02</td>
                        <td>
                            <div class="cat">Регби-7. Мировая Серия. Ванкувер</div>
                            <a href="#">Новая Зеландия-7 - Австралия-7</a>
                        </td>
                        <td>$450</td>
                    </tr>
                    <tr>
                        <td>00:06</td>
                        <td>
                            <div class="cat">Теннис. WTA. Индиан-Уэллс. Хард</div>
                            <a href="#">Свитолина Элина - Гаврилова Дарья</a>
                        </td>
                        <td>$370</td>
                    </tr>
                    <tr>
                        <td>00:15</td>
                        <td>
                            <div class="cat">Футбол. Коста-Рика. Примера дивизион</div>
                            <a href="#">Картахинес - УКР</a>
                        </td>
                        <td>$470</td>
                    </tr>
                    <tr>
                        <td>00:15</td>
                        <td>
                            <div class="cat">Хоккей. WHL</div>
                            <a href="#">Калгари Хитмен - Эдмонтон Ойл Кингз</a>
                        </td>
                        <td>$520</td>
                    </tr>
                    <tr>
                        <td>00:15</td>
                        <td>
                            <div class="cat">Хоккей. AHL</div>
                            <a href="#">Онтарио Рейгн - Тусон Роадраннерс</a>
                        </td>
                        <td>$455</td>
                    </tr>
                    <tr>
                        <td>00:16</td>
                        <td>
                            <div class="cat">Баскетбол. NBA. Статистика матча</div>
                            <a href="#">Бруклин Нетс - Нью-Йорк Никс</a>
                        </td>
                        <td>$587</td>
                    </tr>
                    <tr>
                        <td>00:16</td>
                        <td>
                            <div class="cat">Баскетбол. NBA. Игроки</div>
                            <a href="#">Индиана Пэйсерс - Майами Хит</a>
                        </td>
                        <td>$630</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <%@include file="reusable/rightmenu.jsp" %>

</div>
<script src="../js/modal.js"></script>
<script src="../js/maintables.js"></script>

</body>
</html>
