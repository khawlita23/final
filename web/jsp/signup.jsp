<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 26.03.2017
  Time: 19:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="title.signup" var="signUpTitle"/>
<fmt:message bundle="${locale}" key="text.registration" var="registrationText"/>
<fmt:message bundle="${locale}" key="label.firstname" var="firstNameLabel"/>
<fmt:message bundle="${locale}" key="label.lastname" var="lastNameLabel"/>
<fmt:message bundle="${locale}" key="label.birthdate" var="birthdateLabel"/>
<fmt:message bundle="${locale}" key="label.email" var="emailLabel"/>
<fmt:message bundle="${locale}" key="label.login" var="loginLabel"/>
<fmt:message bundle="${locale}" key="label.password" var="passwordLabel"/>
<fmt:message bundle="${locale}" key="label.submitpassword" var="submitPasswordLabel"/>
<fmt:message bundle="${locale}" key="button.addemail" var="addEmailBtn"/>
<fmt:message bundle="${locale}" key="button.deleteemail" var="deleteEmailBtn"/>
<fmt:message bundle="${locale}" key="message.fillfield" var="fillFieldMsg"/>
<fmt:message bundle="${locale}" key="message.loginlength" var="loginLengthMsg"/>
<fmt:message bundle="${locale}" key="message.loginrestriction" var="loginRestrictionMsg"/>
<fmt:message bundle="${locale}" key="message.correctfname" var="correctFirstNameMsg"/>
<fmt:message bundle="${locale}" key="message.correctlname" var="correctLastNameMsg"/>
<fmt:message bundle="${locale}" key="message.passwordsnotequal" var="passwordsNotEqualMsg"/>
<fmt:message bundle="${locale}" key="message.passwordrestriction" var="passwordRestrictionMsg"/>
<fmt:message bundle="${locale}" key="message.passwordlength" var="passwordLengthMsg"/>
<fmt:message bundle="${locale}" key="message.invalidemail" var="invalidEmailMsg"/>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/headers.css">
    <link rel="stylesheet" href="../css/signup.css">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather|Roboto+Slab|Tinos" rel="stylesheet">
    <title>${signUpTitle}</title>
</head>
<body>
<%@include file="reusable/headers.jsp"%>
<div class="row clearfix">
    <div class="col-3"></div>
    <div class="main col-6">
        <h2>${registrationText}</h2>
        <div class="form-wrap">
            <span style="color: #f00;">${requestScope.signinerror}</span>
            <form onsubmit="return validateForm()" action="controller"
                  method="POST" class="clearfix" id="signUpForm">
                <input type="hidden" name="command" value="signup"/>
                <label>${firstNameLabel}:<input type="text" name="fname"/></label><span class="error" id="errFname"></span>
                <br>
                <label>${lastNameLabel}:<input type="text" name="lname"/></label><span class="error" id="errLname"></span>
                <br>
                <label>${birthdateLabel}:
                    <select name="bday">
                        <c:set var="days" value="${['','1','2','3','4','5','6','7','8','9','10','11','12','13',
                        '14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']}"
                               scope="page"/>
                        <c:forEach items="${pageScope.days}" var="day">
                            <option value="${day}">${day}</option>
                        </c:forEach>
                    </select>
                    <select name="bmonth">
                        <option>&nbsp;</option>
                        <option value="01">Январь</option>
                        <option value="02">Февраль</option>
                        <option value="03">Март</option>
                        <option value="04">Апрель</option>
                        <option value="05">Май</option>
                        <option value="06">Июнь</option>
                        <option value="07">Июль</option>
                        <option value="08">Август</option>
                        <option value="09">Сентябрь</option>
                        <option value="10">Октябрь</option>
                        <option value="11">Ноябрь</option>
                        <option value="12">Декабрь</option>
                    </select>
                    <select name="byear">
                        <c:set var="years" value="${['','1999','1998','1997','1996','1995','1994','1993','1992',
                        '1991','1990','1989','1988','1987','1986','1985','1984','1983','1982','1981','1980',
                        '1979','1978','1977','1976','1975','1974','1973','1972','1971','1970','1969',
                        '1968','1967','1966','1965','1964','1963','1962','1961','1960','1959','1958',
                        '1957','1956','1955','1954','1953','1952','1951','1950','1949','1948','1947',
                        '1946','1945','1944','1943','1942','1941','1940','1939','1938','1937','1936']}" scope="page"/>
                        <c:forEach items="${pageScope.years}" var="year">
                            <option value="${year}">${year}</option>
                        </c:forEach>
                    </select>
                </label><span class="error" id="errBirthdate"></span>
                <br/>
                <%--<label>Возраст:<input type="number" name="age"/></label><span class="error" id="errAge"></span>--%>
                <%--<br>--%>
                <label>${emailLabel}:<input type="text" name="email"/></label>
                <button onclick="return addEmail()">${addEmailBtn}</button>
                <span class="error" id="errEmail"></span>
                <br>
                <label id="loginField">${loginLabel}:<input type="text" name="login"/></label>
                <span class="error" id="errLogin">${requestScope.loginduplicateerror}</span>
                <br>
                <label>${passwordLabel}:<input type="password" name="pswrd1"/></label><span class="error" id="errPswrd1"></span>
                <br>
                <label>${submitPasswordLabel}:<input type="password" name="pswrd2"/></label><span class="error"
                                                                                            id="errPswrd2"></span>
                <br>
                <input type="submit" value="${signUpBtn}" class="signup-button"/>
            </form>
        </div>
    </div>
    <div class="col-3"></div>
</div>
<script>
    var FILL_FIELD = "${fillFieldMsg}",
        AT_LEAST_5 = "${loginLengthMsg}",
        LOGIN = "${loginRestrictionMsg}",
        F_NAME = "${correctFirstNameMsg}",
        L_NAME = "${correctLastNameMsg}",
        PWD_NOT_EQUAL = "${passwordsNotEqualMsg}",
        WRONG_PWD = "${passwordRestrictionMsg}",
        AT_LEAST_6 = "${passwordLengthMsg}",
        INVALID_EMAIL = "${invalidEmailMsg}",
        DELETE_EMAIL_BTN = "${deleteEmailBtn}",
        EMAIL_LABEL = "${emailLabel}";
</script>
<script src="../js/signup.js"></script>
<script src="../js/modal.js"></script>
</body>
</html>
