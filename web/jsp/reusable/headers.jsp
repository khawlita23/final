<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 20.05.2017
  Time: 12:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="button.login" var="logInBtn"/>
<fmt:message bundle="${locale}" key="button.signup" var="signUpBtn"/>
<fmt:message bundle="${locale}" key="button.profile" var="profileBtn"/>
<fmt:message bundle="${locale}" key="button.logout" var="logOutBtn"/>
<fmt:message bundle="${locale}" key="message.greeting" var="greetingMsg"/>
<fmt:message bundle="${locale}" key="text.totalisator" var="totalisatorText"/>
<fmt:message bundle="${locale}" key="text.authorisation" var="authorisationText"/>
<fmt:message bundle="${locale}" key="placeholder.login" var="loginPlaceholder"/>
<fmt:message bundle="${locale}" key="placeholder.password" var="passwordPlaceholder"/>
<fmt:message bundle="${locale}" key="label.loginasadmin" var="logInAsAdminLabel"/>
<fmt:message bundle="${locale}" key="button.management" var="managementButton"/>
<c:set var="user" value="${sessionScope.user}" scope="session"/>
<c:set var="role" value="${sessionScope.role}" scope="session"/>
<c:choose>
    <c:when test="${user != null}">
        <c:choose>
            <c:when test="${role == 'admin'}">
                <c:set var="leftBtnCommand" value="gotomanagement" scope="session"/>
                <c:set var="leftBtn" value="${managementButton}" scope="session"/>
            </c:when>
            <c:when test="${role == 'client'}">
                <c:set var="leftBtnCommand" value="gotouserprofile" scope="session"/>
                <%--<c:set var="leftBtnId" value="" scope="session"/>--%>
                <c:set var="leftBtn" value="${profileBtn}" scope="session"/>
            </c:when>
        </c:choose>
        <c:set var="leftBtnType" value="submit" scope="session"/>
        <c:set var="leftBtnOnclick" value="" scope="session"/>
        <c:set var="rightBtnCommand" value="logout" scope="session"/>
        <c:set var="rightBtn" value="${logOutBtn}" scope="session"/>
    </c:when>
    <c:otherwise>
        <c:set var="leftBtnCommand" value="gotologin" scope="session"/>
        <c:set var="leftBtnType" value="button" scope="session"/>
        <%--<c:set var="leftBtnId" value="logInBtn" scope="session"/>--%>
        <c:set var="leftBtnOnclick" value="showModal()" scope="session"/>
        <c:set var="leftBtn" value="${logInBtn}" scope="session"/>
        <c:set var="rightBtnCommand" value="gotosignup" scope="session"/>
        <c:set var="rightBtn" value="${signUpBtn}" scope="session"/>
    </c:otherwise>
</c:choose>
<div class="header col-12 clearfix">
    <div class="lang">
        <form action="controller" method="POST" class="clearfix">
            <input type="hidden" name="command" value="changelang">
            <input type="submit" value="RU" name="lang">
            <input type="submit" value="EN" name="lang">
            <input type="submit" value="DE" name="lang">
        </form>
    </div>
    <div class="auth clearfix">
        <form action="controller" method="POST" class="inline">
            <input type="hidden" name="command" value="${sessionScope.leftBtnCommand}"/>
            <input type="${sessionScope.leftBtnType}" onclick="${sessionScope.leftBtnOnclick}" value="${sessionScope.leftBtn}">
        </form>
        <form action="controller" method="POST" class="inline">
            <input type="hidden" name="command" value="${sessionScope.rightBtnCommand}"/>
            <input type="submit" value="${sessionScope.rightBtn}">
        </form>
    </div>
    <c:if test="${user != null}">
        <ctg:greeting username="${user}" greeting="${greetingMsg}"/>
    </c:if>
</div>
<div class="header-2 col-12 clearfix">
    <a href="index.jsp"><h1>${totalisatorText}</h1></a>
</div>
<div id="authModal" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="modal-close">&times;</span>
            <h3>${authorisationText}</h3>
        </div>
        <div class="modal-body">
            <form name="loginForm" action="controller" method="POST">
                <input type="hidden" name="command" value="login"/>
                <%--<input name="target" type="hidden"/>--%>
                <div class="form">
                    <input type="text" name="login" placeholder="${loginPlaceholder}"/>
                </div>
                <div class="form">
                    <input type="password" name="password" placeholder="${passwordPlaceholder}"/>
                </div>
                <br/>
                <%--${requestScope.errorLoginPassMessage}--%>
                <label><input type="checkbox" name="admin" value="asAdmin"/>${logInAsAdminLabel}</label>
                <br/>
                <input type="submit" name="loginBtn" value="${logInBtn}"/>
            </form>
        </div>
    </div>
</div>
<%--<script src="../../js/modal.js"></script>--%>
<%--from "main.jsp"--%>
<%--<div class="header-2 col-12 clearfix">
    <a href="main.jsp"><h1>Тотализатор</h1></a>
    <span class="error">${requestScope.error}</span>
</div>--%>

