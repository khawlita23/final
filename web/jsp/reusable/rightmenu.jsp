<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 20.05.2017
  Time: 22:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="text.menu" var="menuText"/>
<fmt:message bundle="${locale}" key="button.rules" var="rulesBtn"/>
<fmt:message bundle="${locale}" key="button.addmatch" var="addMatchBtn"/>
<fmt:message bundle="${locale}" key="button.addevent" var="addEventBtn"/>
<fmt:message bundle="${locale}" key="button.clients" var="clientsBtn"/>
<fmt:message bundle="${locale}" key="button.signup" var="signUpBtn"/>
<c:set var="role" value="${sessionScope.role}" scope="session"/>
<div class="right-menu col-3">
    <h2>${menuText}</h2>
    <ul>
        <c:choose>
            <c:when test="${role == 'admin'}">
                <li>
                    <form action="controller" method="POST" style="margin: 0;">
                        <input type="hidden" name="command" value="gotoaddmatch">
                        <input type="submit" value="${addMatchBtn}">
                    </form>
                </li>
                <li>
                    <form action="controller" method="POST" style="margin: 0;">
                        <input type="hidden" name="command" value="gotoaddevent">
                        <input type="submit" value="${addEventBtn}">
                    </form>
                </li>
                <li>
                    <form action="controller" method="POST" style="margin: 0;">
                        <input type="hidden" name="command" value="gotomanagement">
                        <input type="submit" value="${clientsBtn}">
                    </form>
                </li>
            </c:when>
            <c:otherwise>
                <li>
                    <form action="controller" method="POST" style="margin: 0;">
                        <input type="hidden" name="command" value="gotorules">
                        <input type="submit" value="${rulesBtn}">
                    </form>
                </li>
                <c:if test="${sessionScope.user eq null}">
                    <li>
                        <form action="controller" method="POST" style="margin: 0;">
                            <input type="hidden" name="command" value="gotosignup">
                            <input type="submit" value="${signUpBtn}">
                        </form>
                    </li>
                </c:if>
            </c:otherwise>
        </c:choose>

    </ul>
</div>
