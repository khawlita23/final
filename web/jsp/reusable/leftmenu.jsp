<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 20.05.2017
  Time: 22:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="text.sports" var="sportsText"/>
<fmt:message bundle="${locale}" key="button.tennis" var="tennisBtn"/>
<fmt:message bundle="${locale}" key="button.football" var="footballBtn"/>
<fmt:message bundle="${locale}" key="button.volleyball" var="volleyballBtn"/>
<fmt:message bundle="${locale}" key="button.basketball" var="basketballBtn"/>
<fmt:message bundle="${locale}" key="button.handball" var="handballBtn"/>
<fmt:message bundle="${locale}" key="button.hockey" var="hockeyBtn"/>
<fmt:message bundle="${locale}" key="button.esports" var="esportsBtn"/>
<fmt:message bundle="${locale}" key="button.cycling" var="cyclingBtn"/>

<div class="left-menu col-3">
    <h2>${sportsText}</h2>
    <form action="controller" method="POST">
        <input type="hidden" name="command" value="gotomatches"/>
        <ul>
            <li>
                <button type="submit" name="category" value="Теннис">${tennisBtn}</button>
            </li>
            <li>
                <button type="submit" name="category" value="Футбол">${footballBtn}</button>
            <li>
                <button type="submit" name="category" value="Волейбол">${volleyballBtn}</button>
            </li>
            <li>
                <button type="submit" name="category" value="Баскетбол">${basketballBtn}</button>
            </li>
            <li>
                <button type="submit" name="category" value="Гандбол">${handballBtn}</button>
            </li>
            <li>
                <button type="submit" name="category" value="Хоккей">${hockeyBtn}</button>
            </li>
            <li>
                <button type="submit" name="category" value="Киберспорт">${esportsBtn}</button>
            </li>
            <li>
                <button type="submit" name="category" value="Велоспорт">${cyclingBtn}</button>
            </li>
        </ul>
    </form>
</div>
