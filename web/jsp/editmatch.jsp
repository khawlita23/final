<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 04.06.2017
  Time: 1:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="title.editmatch" var="editMatchTitle"/>
<fmt:message bundle="${locale}" key="text.matchinfo" var="matchInfoText"/>
<fmt:message bundle="${locale}" key="label.matchdate" var="matchDateLabel"/>
<fmt:message bundle="${locale}" key="label.matchname" var="matchNameLabel"/>
<fmt:message bundle="${locale}" key="label.eventname" var="eventNameLabel"/>
<fmt:message bundle="${locale}" key="label.place" var="placeLabel"/>
<fmt:message bundle="${locale}" key="label.outcome" var="outcomeLabel"/>
<fmt:message bundle="${locale}" key="button.savechanges" var="saveChangesBtn"/>
<fmt:message bundle="${locale}" key="message.matchupdated" var="matchUpdatedMsg"/>
<fmt:message bundle="${locale}" key="message.fillfield" var="fillFieldMsg"/>
<fmt:message bundle="${locale}" key="message.incorrectmatchname" var="incorrectMatchNameMsg"/>
<fmt:message bundle="${locale}" key="message.incorrectmatchdate" var="incorrectMatchDateMsg"/>
<c:set var="match" value="${requestScope.match}"/>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/headers.css">
    <link rel="stylesheet" href="../css/menus.css">
    <link rel="stylesheet" href="../css/table.css">
    <link rel="stylesheet" href="../css/editmatch.css">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather|Roboto+Slab|Tinos" rel="stylesheet">
    <title>${editMatchTitle}</title>
</head>
<body>
<%@include file="reusable/headers.jsp" %>
<div class="row clearfix">
    <%@include file="reusable/leftmenu.jsp" %>
    <div class="main col-6">
        <h2>${matchInfoText}</h2>
        <span>${requestScope.matchInfoChanged}</span>
        <form action="controller" method="POST"
              onsubmit="return validateForm()" id="editMatchForm">
            <input type="hidden" name="command" value="editmatch">
            <input type="hidden" name="matchId" value="${match.matchId}">
            <table>
                <tr>
                    <td><label for="mDate">${matchDateLabel}</label></td>
                    <td>
                        <input type="text" name="matchDate" value="${fn:substring(match.date, 0, 16)}"
                               id="mDate">
                        <span class="error" id="errMDate"></span>
                    </td>
                </tr>
                <tr>
                    <td><label for="mName">${matchNameLabel}</label></td>
                    <td>
                        <input type="text" name="matchName" value="${match.name}"
                               id="mName">
                        <span class="error" id="errMName"></span>
                    </td>
                </tr>
                <tr>
                    <td>${eventNameLabel}</td>
                    <td>
                        <%--<input type="text" name="eventName" value="${match.event.name}" readonly>--%>
                        <textarea name="eventName" readonly>${match.event.name}</textarea></td>
                </tr>
                <tr>
                    <td>${placeLabel}</td>
                    <td><input type="text" name="eventPlace" value="${match.event.place}" readonly>
                    </td>
                </tr>
                <tr>
                    <td>${outcomeLabel}</td>
                    <td>
                        <c:set var="matchOutcomeValue" value="${match.outcome.value}"/>
                        <c:set var="matchOutcomeId" value="${match.outcome.outcomeId}"/>
                        <select name="outcomeId">
                            <option value="${matchOutcomeId}" selected>${matchOutcomeValue}</option>
                            <c:forEach items="${sessionScope.outcomesList}" var="outcome">
                                <c:if test="${matchOutcomeValue != outcome.value}">
                                    <option value="${outcome.outcomeId}">${outcome.value}</option>
                                </c:if>
                            </c:forEach>
                            <c:if test="${matchOutcomeValue != null}">
                                <option value="0"></option>
                            </c:if>
                        </select>
                    </td>
                </tr>
            </table>
            <div style="text-align: center;">
                <input name="saveChanges" type="submit" value="${saveChangesBtn}">
            </div>
            <c:if test="${requestScope.success == true}">
                <div style="text-align: center;">
                    <span>${matchUpdatedMsg}</span>
                </div>
            </c:if>
        </form>

    </div>
    <%@include file="reusable/rightmenu.jsp" %>
</div>
<script>
    var FILL_FIELD = "${fillFieldMsg}",
        MATCH_NAME = "${incorrectMatchNameMsg}",
        MATCH_DATE = "${incorrectMatchDateMsg}";
</script>
<script src="../js/editmatch.js"></script>
<script src="../js/modal.js"></script>
</body>
</html>
