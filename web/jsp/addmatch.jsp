<%--
  Created by IntelliJ IDEA.
  User: Hamster
  Date: 04.06.2017
  Time: 15:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.userLocale}" scope="session"/>
<fmt:setBundle basename="resources.i18n.pagecontent" var="locale" scope="session"/>
<fmt:message bundle="${locale}" key="title.addmatch" var="addMatchTitle"/>
<fmt:message bundle="${locale}" key="text.addmatch" var="addMatchText"/>
<fmt:message bundle="${locale}" key="label.matchname" var="matchNameLabel"/>
<fmt:message bundle="${locale}" key="label.matchdate" var="matchDateLabel"/>
<fmt:message bundle="${locale}" key="label.event" var="eventLabel"/>
<fmt:message bundle="${locale}" key="button.add" var="addBtn"/>
<fmt:message bundle="${locale}" key="message.fillfield" var="fillFieldMsg"/>
<fmt:message bundle="${locale}" key="message.incorrectmatchname" var="incorrectMatchNameMsg"/>
<fmt:message bundle="${locale}" key="message.incorrectmatchdate" var="incorrectMatchDateMsg"/>
<fmt:message bundle="${locale}" key="message.matchadded" var="matchAddedMsg"/>
<fmt:message bundle="${locale}" key="message.matchalreadyexists" var="matchAlreadyExistsMsg"/>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/headers.css">
    <link rel="stylesheet" href="../css/addmatch.css">
    <link href="https://fonts.googleapis.com/css?family=Lora|Merriweather|Roboto+Slab|Tinos" rel="stylesheet">
    <title>${addMatchTitle}</title>
</head>
<body>
<%@include file="reusable/headers.jsp" %>
<div class="row clearfix">
    <div class="col-2"></div>
    <div class="main col-8">
        <h2>${addMatchText}</h2>
        <div class="form-wrap">
            <c:if test="${requestScope.alreadyExists == true}">
                <div style="text-align: center; color: #f00;">
                    <span>${matchAlreadyExistsMsg}</span>
                </div>
            </c:if>
            <form onsubmit="return validateForm()" action="controller"
                  method="POST" class="clearfix" id="addMatchForm">
                <input type="hidden" name="command" value="addmatch"/>
                <label>${eventLabel}:
                    <select name="eventId">
                        <option value=""></option>
                        <c:forEach items="${requestScope.eventsList}" var="event">
                            <option value="${event.eventId}">${event.category}. ${event.name}. ${event.place}</option>
                        </c:forEach>
                    </select>
                </label>
                <span class="error" id="errEvent"></span>
                <br>
                <label>${matchNameLabel}:<input type="text" name="matchName"></label>
                <span class="error" id="errMName"></span>
                <br>
                <label>${matchDateLabel}:<input type="text" name="matchDate"></label>
                <span class="error" id="errMDate"></span>
                <br>
                <input type="submit" value="${addBtn}" class="addmatch-button">
            </form>

            <c:if test="${requestScope.success == true}">
                <div style="text-align: center;">
                    <span>${matchAddedMsg}</span>
                </div>
            </c:if>

        </div>
    </div>
    <div class="col-2"></div>
</div>
<script>
    var FILL_FIELD = "${fillFieldMsg}",
        MATCH_NAME = "${incorrectMatchNameMsg}",
        MATCH_DATE = "${incorrectMatchDateMsg}";
</script>
<script src="../js/addmatch.js"></script>
<script src="../js/modal.js"></script>
</body>
</html>
