Totalisator (project description)

It is a web-application with frontend written in HTML, CSS and JavaScript and backend written in Java using Servlets and JSP technologies. Application is covered by JUnit tests.
There are two roles in the application: the ordinary user and the administrator. It allows ordinary users to sign up, sign in, view events and games, make bets, cancel bets 
before the event has finished and edit information in their profile. Administrator in his turn can view all events and games as well as all the registered users. Administrator 
can also edit users’ profiles, cancel their bets, edit games’ information. When administrator changes game status to “finished”, program automatically counts all users’ prizes.

Application features: form validation, password encryption,	pagination mechanism, internationalization and localization: Russian, German and English.

Implementation details:	custom JSTL tags, response and request filters, thread-safe connection pool with connection wrapper class.

Implemented design patterns: MVC, DAO, Layered architecture, Factory method, Command, Singleton.

To run the application I used Apache Tomcat v8.5.11. MySQL was used for database management.
All exceptions are being handled properly and logged. For logging I used Log4J libraries.
