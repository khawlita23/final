package by.bsuir.servicelayer.factory;

import by.bsuir.servicelayer.*;

/**
 * Class {@code AbstractFactory} represents an abstract factory.
 *
 * @author Hamster
 * @since 1.0
 */
public interface AbstractFactory {

    public AdminService getAdminService();
    public BetService getBetService();
    public ClientService getClientService();
    public EventService getEventService();
    public MatchService getMatchService();
    public OutcomeService getOutcomeService();

}
