package by.bsuir.servicelayer.factory;

import by.bsuir.servicelayer.*;
import by.bsuir.servicelayer.impl.*;

/**
 * Class {@code ServiceFactory} represents a concrete factory for creating service objects.
 *
 * @author Hamster
 * @since 1.0
 */
public class ServiceFactory implements AbstractFactory {

    private static final ServiceFactory instance = new ServiceFactory();
    private final AdminService adminService = new AdminServiceImpl();
    private final BetService betService = new BetServiceImpl();
    private final ClientService clientService = new ClientServiceImpl();
    private final EventService eventService = new EventServiceImpl();
    private final MatchService matchService = new MatchServiceImpl();
    private final OutcomeService outcomeService = new OutcomeServiceImpl();

    public static ServiceFactory getInstance() {
        return instance;
    }

    private ServiceFactory() {}

    @Override
    public AdminService getAdminService() {
        return new AdminServiceImpl();
    }

    @Override
    public BetService getBetService() {
        return new BetServiceImpl();
    }

    @Override
    public ClientService getClientService() {
        return new ClientServiceImpl();
    }

    @Override
    public EventService getEventService() {
        return new EventServiceImpl();
    }

    @Override
    public MatchService getMatchService() {
        return new MatchServiceImpl();
    }

    @Override
    public OutcomeService getOutcomeService() {
        return new OutcomeServiceImpl();
    }
}
