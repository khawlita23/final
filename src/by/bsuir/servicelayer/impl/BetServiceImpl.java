package by.bsuir.servicelayer.impl;

import by.bsuir.bean.Bet;
import by.bsuir.db.dao.impl.BetDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.servicelayer.BetService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;

import java.util.List;
import java.util.Map;

/**
 * Class {@code BetServiceImpl} ia an implementation of {@link BetService}.
 *
 * @author Hamster
 * @since 1.0
 */
public class BetServiceImpl implements BetService {

    @Override
    public boolean deleteBet(String clientIdParam, String matchIdParam) throws ServiceLayerException {
        BetDAO betDAO = BetDAO.getInstance();
        boolean result;
        try {
            DataController.areNull(clientIdParam, matchIdParam);
            DataController.areEmptyStrings(clientIdParam, matchIdParam);
            int clientId = Integer.parseInt(clientIdParam);
            int matchId = Integer.parseInt(matchIdParam);
            result = betDAO.delete(clientId, matchId);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO on a service layer", e);
        }
        return result;
    }

    @Override
    public List<Bet> findByClientId(String clientIdParam) throws ServiceLayerException {
        BetDAO betDAO = BetDAO.getInstance();
        List<Bet> result;
        try {
            DataController.isNull(clientIdParam);
            DataController.isEmptyString(clientIdParam);
            int clientId = Integer.parseInt(clientIdParam);
            result = betDAO.findByClientId(clientId);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO on a service layer", e);
        }
        return result;
    }

    @Override
    public Map<Integer, Double> getStakesSums() throws ServiceLayerException {
        BetDAO betDAO = BetDAO.getInstance();
        Map<Integer, Double> result;
        try {
            result = betDAO.getStakesSums();
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO on a service layer", e);
        }
        return result;
    }

    @Override
    public boolean doesAlreadyExist(String clientIdParam, String matchIdParam) throws ServiceLayerException {
        BetDAO betDAO = BetDAO.getInstance();
        boolean result;
        try {
            DataController.areNull(clientIdParam, matchIdParam);
            DataController.areEmptyStrings(clientIdParam, matchIdParam);
            int clientId = Integer.parseInt(clientIdParam);
            int matchId = Integer.parseInt(matchIdParam);
            result = betDAO.doesAlreadyExist(clientId, matchId);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
        return result;
    }

    @Override
    public void addBet(String clientIdParam, String matchIdParam, String stakeParam, String outcomeIdParam)
            throws ServiceLayerException {
        BetDAO betDAO = BetDAO.getInstance();
        try {
            DataController.areNull(matchIdParam, outcomeIdParam, stakeParam);
            DataController.areEmptyStrings(matchIdParam, outcomeIdParam, stakeParam);
            int clientId = Integer.parseInt(clientIdParam);
            int matchId = Integer.parseInt(matchIdParam);
            double stake = Double.parseDouble(stakeParam);
            int outcomeId = Integer.parseInt(outcomeIdParam);
            betDAO.create(clientId, matchId, stake, outcomeId);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (NumberFormatException e) {
            throw e;
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
    }
}
