package by.bsuir.servicelayer.impl;

import by.bsuir.db.dao.impl.AdminDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.servicelayer.AdminService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.util.DataController;
import by.bsuir.util.PasswordEncrypter;
import by.bsuir.util.exception.DataControlException;

/**
 * Class {@code AdminServiceImpl} ia an implementation of {@link AdminService}.
 *
 * @author Hamster
 * @since 1.0
 */
public class AdminServiceImpl implements AdminService {
    @Override
    public boolean checkPassword(String login, String password) throws ServiceLayerException {
        AdminDAO adminDAO = AdminDAO.getInstance();
        boolean result;
        try {
            DataController.areNull(login, password);
            DataController.areEmptyStrings(login, password);
            String encryptedPassword = PasswordEncrypter.encryptPassword(password);
            result = adminDAO.checkPassword(login, encryptedPassword);
            return result;
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
    }
}
