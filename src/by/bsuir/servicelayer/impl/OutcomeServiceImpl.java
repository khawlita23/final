package by.bsuir.servicelayer.impl;

import by.bsuir.bean.Outcome;
import by.bsuir.db.dao.impl.OutcomeDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.servicelayer.OutcomeService;
import by.bsuir.servicelayer.exception.ServiceLayerException;

import java.util.List;


/**
 * Class {@code OutcomeServiceImpl} ia an implementation of {@link OutcomeService}.
 *
 * @author Hamster
 * @since 1.0
 */
public class OutcomeServiceImpl implements OutcomeService {


    @Override
    public List<Outcome> findAll() throws ServiceLayerException {
        OutcomeDAO outcomeDAO = OutcomeDAO.getInstance();
        List<Outcome> result;
        try {
            result = outcomeDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
        return result;
    }
}
