package by.bsuir.servicelayer.impl;

import by.bsuir.bean.Event;
import by.bsuir.bean.Match;
import by.bsuir.bean.Outcome;
import by.bsuir.db.dao.impl.MatchDAO;
import by.bsuir.db.dao.impl.OutcomeDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.servicelayer.MatchService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;

import java.sql.Timestamp;
import java.util.List;

/**
 * Class {@code MatchServiceImpl} ia an implementation of {@link MatchService}.
 *
 * @author Hamster
 * @since 1.0
 */
public class MatchServiceImpl implements MatchService {
    @Override
    public boolean doesAlreadyExist(String matchName) throws ServiceLayerException {
        MatchDAO matchDAO = MatchDAO.getInstance();
        boolean result;
        try {
            DataController.isNull(matchName);
            DataController.isEmptyString(matchName);
            result = matchDAO.doesAlreadyExist(matchName);
            return result;
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
    }

    @Override
    public void addMatch(String eventIdParam, String matchName, String matchDate) throws ServiceLayerException {
        MatchDAO matchDAO = MatchDAO.getInstance();
        try {
            DataController.areNull(eventIdParam, matchName, matchDate);
            DataController.areEmptyStrings(matchName, matchDate);
            int eventId = Integer.parseInt(eventIdParam);
            Match match = new Match();
            Event event = new Event();
            match.setName(matchName);
            match.setDate(Timestamp.valueOf(matchDate + ":00"));
            event.setEventId(eventId);
            match.setEvent(event);
            matchDAO.create(match);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
    }

    @Override
    public Match editMatch(String matchIdParam, String matchDate, String matchName, String outcomeIdParam,
                          String eventName, String eventPlace) throws ServiceLayerException {
        MatchDAO matchDAO = MatchDAO.getInstance();
        Match result;
        try {
            DataController.areNull(matchIdParam, matchDate, matchName, outcomeIdParam, eventName, eventPlace);
            DataController.areEmptyStrings(matchIdParam, matchDate, matchName, outcomeIdParam, eventName, eventPlace);
            int matchId = Integer.parseInt(matchIdParam);
            int outcomeId = Integer.parseInt(outcomeIdParam);
            Match match = new Match();
            Outcome outcome = new Outcome();
            Event event = new Event();
            match.setMatchId(matchId);
            match.setDate(Timestamp.valueOf(matchDate + ":00"));
            match.setName(matchName);
            if (outcomeId == 0) {
                outcome.setOutcomeId(null);
                outcome.setValue(null);
            } else {
                OutcomeDAO outcomeDAO = OutcomeDAO.getInstance();
                outcome = outcomeDAO.findById(outcomeId);
            }
            match.setOutcome(outcome);
            event.setName(eventName);
            event.setPlace(eventPlace);
            match.setEvent(event);
            result = matchDAO.update(match);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }

        return result;
    }

    @Override
    public Match findById(String matchIdParam) throws ServiceLayerException {
        MatchDAO matchDAO = MatchDAO.getInstance();
        Match result;
        try {
            DataController.isNull(matchIdParam);
            DataController.isEmptyString(matchIdParam);
            int matchId = Integer.parseInt(matchIdParam);
            result = matchDAO.findById(matchId);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
        return result;
    }

    @Override
    public List<Match> findOpenByCategory(String category) throws ServiceLayerException {
        MatchDAO matchDAO = MatchDAO.getInstance();
        List<Match> result;
        try {
            DataController.isNull(category);
            DataController.isEmptyString(category);
            result = matchDAO.findOpenByCategory(category);

        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }

        return result;
    }

    @Override
    public List<Match> findAllByCategory(String category) throws ServiceLayerException {
        MatchDAO matchDAO = MatchDAO.getInstance();
        List<Match> result;
        try {
            DataController.isNull(category);
            DataController.isEmptyString(category);
            result = matchDAO.findAllByCategory(category);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
        return result;
    }

    @Override
    public List<Match> findFinishedByCategory(String category) throws ServiceLayerException {
        MatchDAO matchDAO = MatchDAO.getInstance();
        List<Match> result;
        try {
            DataController.isNull(category);
            DataController.isEmptyString(category);
            result = matchDAO.findFinishedByCategory(category);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
        return result;
    }
}
