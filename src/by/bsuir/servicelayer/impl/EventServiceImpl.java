package by.bsuir.servicelayer.impl;

import by.bsuir.bean.Event;
import by.bsuir.db.dao.impl.EventDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.servicelayer.EventService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;

import java.util.List;

/**
 * Class {@code EventServiceImpl} ia an implementation of {@link EventService}.
 *
 * @author Hamster
 * @since 1.0
 */
public class EventServiceImpl implements EventService {
    @Override
    public boolean doesAlreadyExist(String eventName) throws ServiceLayerException {
        EventDAO eventDAO = EventDAO.getInstance();
        boolean result;
        try {
            DataController.isNull(eventName);
            DataController.isEmptyString(eventName);
            result = eventDAO.doesAlreadyExist(eventName);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
        return result;
    }

    @Override
    public void addEvent(String category, String eventName, String eventPlace) throws ServiceLayerException {
        EventDAO eventDAO = EventDAO.getInstance();
        try {
            DataController.areNull(category, eventName, eventPlace);
            DataController.areEmptyStrings(category, eventName, eventPlace);
            Event event = new Event();
            event.setName(eventName);
            event.setCategory(category);
            event.setPlace(eventPlace);
            eventDAO.create(event);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
    }

    @Override
    public List<Event> findAll() throws ServiceLayerException {
        List<Event> result;
        EventDAO eventDAO = EventDAO.getInstance();
        try {
            result = eventDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
        return result;
    }
}
