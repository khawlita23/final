package by.bsuir.servicelayer.impl;

import by.bsuir.bean.Client;
import by.bsuir.db.dao.impl.ClientDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.servicelayer.ClientService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.util.DataController;
import by.bsuir.util.DateParser;
import by.bsuir.util.PasswordEncrypter;
import by.bsuir.util.exception.DataControlException;
import by.bsuir.util.exception.DateParsingException;

import java.util.List;

/**
 * Class {@code ClientServiceImpl} ia an implementation of {@link ClientService}.
 *
 * @author Hamster
 * @since 1.0
 */
public class ClientServiceImpl implements ClientService {
    @Override
    public boolean checkPassword(String login, String password) throws ServiceLayerException {
        ClientDAO clientDAO = ClientDAO.getInstance();
        boolean result;
        try {
            DataController.areNull(login, password);
            DataController.areEmptyStrings(login, password);
            String encryptedPassword = PasswordEncrypter.encryptPassword(password);
            result = clientDAO.checkPassword(login, encryptedPassword);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
        return result;
    }

    @Override
    public void changePassword(String login, String newPassword) throws ServiceLayerException {
        ClientDAO clientDAO = ClientDAO.getInstance();
        try {
            DataController.areNull(login, newPassword);
            DataController.areEmptyStrings(login, newPassword);
            String encryptedPassword = PasswordEncrypter.encryptPassword(newPassword);
            clientDAO.changePassword(login, encryptedPassword);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
    }

    @Override
    public List<Client> findAll() throws ServiceLayerException {
        ClientDAO clientDAO = ClientDAO.getInstance();
        List<Client> result;
        try {
            result = clientDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
        return result;
    }

    @Override
    public Client findById(String clientIdParam) throws ServiceLayerException {
        ClientDAO clientDAO = ClientDAO.getInstance();
        Client result;
        try {
            DataController.isNull(clientIdParam);
            DataController.isEmptyString(clientIdParam);
            int clientId = Integer.parseInt(clientIdParam);
            result = clientDAO.findById(clientId);
            return result;
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
    }

    @Override
    public Client findClientByLogin(String login) throws ServiceLayerException {
        ClientDAO clientDAO = ClientDAO.getInstance();
        Client result;
        try {
            DataController.isNull(login);
            DataController.isEmptyString(login);
            result = clientDAO.findClientByLogin(login);
            return result;
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
    }

    @Override
    public int findIdByLogin(String login) throws ServiceLayerException {
        ClientDAO clientDAO = ClientDAO.getInstance();
        int result;
        try {
            DataController.isNull(login);
            DataController.isEmptyString(login);
            result = clientDAO.findIdByLogin(login);
            return result;
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
    }

    @Override
    public void addClient(String firstName, String lastName, String email, String login,
                          String password, String bDay, String bMonth, String bYear) throws ServiceLayerException {
        ClientDAO clientDAO = ClientDAO.getInstance();
        try {
            DataController.areNull(firstName, lastName, email, login, password, bDay, bMonth, bYear);
            DataController.areEmptyStrings(firstName, lastName, email, login, password, bDay, bMonth, bYear);
            String encryptedPassword = PasswordEncrypter.encryptPassword(password);
            Client client = new Client();
            client.setFirstName(firstName);
            client.setLastName(lastName);
            client.setEmail(email);
            client.setLogin(login);
            client.setPassword(encryptedPassword);
            java.sql.Date birthDate = DateParser.parseDate(bDay + "-" + bMonth + "-" + bYear);
            client.setBirthDate(birthDate);
            clientDAO.create(client);
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        } catch (DateParsingException e) {
            throw new ServiceLayerException("Problem parsing date", e);
        }
    }

    @Override
    public boolean checkLogin(String login) throws ServiceLayerException {
        ClientDAO clientDAO = ClientDAO.getInstance();
        boolean result;
        try {
            DataController.isNull(login);
            DataController.isEmptyString(login);
            result = clientDAO.checkLogin(login);
            return result;
        } catch (DataControlException e) {
            throw new ServiceLayerException("Some of the data is null or empty", e);
        } catch (DAOException e) {
            throw new ServiceLayerException("Problem with DAO", e);
        }
    }
}
