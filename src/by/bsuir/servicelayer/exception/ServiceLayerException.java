package by.bsuir.servicelayer.exception;

/**
 * Created by Hamster on 11.06.2017.
 */
public class ServiceLayerException extends Exception {

    public ServiceLayerException() {
        super();
    }

    public ServiceLayerException(String message) {
        super(message);
    }

    public ServiceLayerException(String message, Throwable cause) {
        super(message, cause);
    }
}
