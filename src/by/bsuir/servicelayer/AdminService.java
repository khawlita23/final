package by.bsuir.servicelayer;

import by.bsuir.servicelayer.exception.ServiceLayerException;

/**
 * Interface {@code AdminService} provides
 * methods to work with entity Admin.
 *
 * @author Hamster
 * @since 1.0
 */
public interface AdminService {
    /**
     * Checks admin's login and password.
     *
     * @param login admin's login.
     * @param password admin's password.
     *
     * @return {@code true} if password and login match data in the database, otherwise {@code false}.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     *
     * @since 1.0
     */
    public boolean checkPassword(String login, String password) throws ServiceLayerException;

}
