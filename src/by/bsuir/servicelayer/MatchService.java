package by.bsuir.servicelayer;

import by.bsuir.bean.Match;
import by.bsuir.servicelayer.exception.ServiceLayerException;

import java.util.ArrayList;
import java.util.List;

/**
 * Interface {@code MatchService} provides
 * methods to work with entity Match.
 *
 * @author Hamster
 * @since 1.0
 */
public interface MatchService {


    /**
     * Checks if an match with such name exists.
     *
     * @param matchName name of the match.
     *
     * @return {@code true} if the match exists, otherwise false.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public boolean doesAlreadyExist(String matchName) throws ServiceLayerException;

    /**
     * Inserts a new match into the database.
     *
     * @param eventIdParam event id.
     * @param matchName match name.
     * @param matchDate match date.
     *
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public void addMatch(String eventIdParam, String matchName, String matchDate) throws ServiceLayerException;

    /**
     * Updates information about match passed as an object.
     *
     * @param matchIdParam match's id.
     * @param matchDate match date.
     * @param matchName name of the match.
     * @param outcomeIdParam id of the outcome or null.
     * @param eventName event's name.
     * @param eventPlace place of the event.
     *
     * @return updated {@link Match} object.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public Match editMatch(String matchIdParam, String matchDate, String matchName, String outcomeIdParam,
                           String eventName, String eventPlace) throws ServiceLayerException;

    /**
     * Finds match by its id.
     *
     * @param matchIdParam match's id.
     *
     * @return {@link Match} object.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public Match findById(String matchIdParam) throws ServiceLayerException;

    /**
     * Finds unfinished matches of the specified category.
     *
     * @param category matches' category.
     *
     * @return {@link ArrayList} of {@link Match} objects.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public List<Match> findOpenByCategory(String category) throws ServiceLayerException;

    /**
     * Finds all matches of the specified category.
     *
     * @param category matches' category.
     *
     * @return {@link ArrayList} of {@link Match} objects.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public List<Match> findAllByCategory(String category) throws ServiceLayerException;

    /**
     * Finds finished matches of the specified category.
     *
     * @param category matches' category.
     *
     * @return {@link ArrayList} of {@link Match} objects.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public List<Match> findFinishedByCategory(String category) throws ServiceLayerException;

}
