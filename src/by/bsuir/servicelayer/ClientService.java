package by.bsuir.servicelayer;

import by.bsuir.bean.Client;
import by.bsuir.servicelayer.exception.ServiceLayerException;

import java.util.ArrayList;
import java.util.List;

/**
 * Interface {@code ClientService} provides
 * methods to work with entity Client.
 *
 * @author Hamster
 * @since 1.0
 */
public interface ClientService {

    public boolean checkPassword(String login, String password) throws ServiceLayerException;

    /**
     * Changes client's password.
     *
     * @param login client's login.
     * @param newPassword new password.
     *
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public void changePassword(String login, String newPassword) throws ServiceLayerException;

    /**
     * Finds all the clients in the database.
     *
     * @return {@link ArrayList} of {@link Client} objects.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public List<Client> findAll() throws ServiceLayerException;

    /**
     * Finds client by his id.
     *
     * @param clientIdParam client's id.
     *
     * @return {@link Client} object.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public Client findById(String clientIdParam) throws ServiceLayerException;

    /**
     * Finds client by his login.
     *
     * @param login client's login.
     *
     * @return {@link Client} object wit such login.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public Client findClientByLogin(String login) throws ServiceLayerException;

    /**
     * Finds client's login by his id.
     *
     * @param login client's login.
     *
     * @return id of the client with such name.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public int findIdByLogin(String login) throws ServiceLayerException;

    /**
     * Adds a new client.
     *
     * @param firstName client's first name.
     * @param lastName client's last name.
     * @param email client's e-mail.
     * @param login client's login.
     * @param password client's password.
     * @param bDay client's birth day.
     * @param bMonth client's birth month.
     * @param bYear client's birth year.
     *
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public void addClient(String firstName, String lastName, String email, String login,
                          String password, String bDay, String bMonth, String bYear) throws ServiceLayerException;

    /**
     * Checks if such login already exists.
     *
     * @param login login to check.
     *
     * @return {@code true} if such login exists, otherwise {@code false}.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public boolean checkLogin(String login) throws ServiceLayerException;

}
