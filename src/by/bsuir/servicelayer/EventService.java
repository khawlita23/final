package by.bsuir.servicelayer;

import by.bsuir.bean.Event;
import by.bsuir.servicelayer.exception.ServiceLayerException;

import java.util.ArrayList;
import java.util.List;

/**
 * Interface {@code EventService} provides
 * methods to work with entity Event.
 *
 * @author Hamster
 * @since 1.0
 */
public interface EventService {

    /**
     * Checks if an event with such name exists.
     *
     * @param eventName name of the event.
     *
     * @return {@code true} if the event exists, otherwise false.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public boolean doesAlreadyExist(String eventName) throws ServiceLayerException;

    /**
     * Adds a new event to the database.
     *
     * @param category category.
     * @param eventName event's name.
     * @param eventPlace event's place.
     *
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public void addEvent(String category, String eventName, String eventPlace) throws ServiceLayerException;

    /**
     * Finds all the events in the database.
     *
     * @return {@link ArrayList} of {@link Event} objects.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public List<Event> findAll() throws ServiceLayerException;

}
