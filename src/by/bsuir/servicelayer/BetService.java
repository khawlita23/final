package by.bsuir.servicelayer;

import by.bsuir.bean.Bet;
import by.bsuir.bean.Client;
import by.bsuir.servicelayer.exception.ServiceLayerException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Interface {@code BetService} provides
 * methods to work with entity Bet.
 *
 * @author Hamster
 * @since 1.0
 */
public interface BetService {

    /**
     * Dismisses a bet made by client.
     *
     * @param clientIdParam client's id.
     * @param matchIdParam id of a match.
     *
     * @return {@code true} if new row is successfully deleted, otherwise false.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public boolean deleteBet(String clientIdParam, String matchIdParam) throws ServiceLayerException;

    /**
     * Finds all bets made by the client whose id is {@code clientId}.
     *
     * @param clientIdParam client's id.
     *
     * @return {@link ArrayList} of {@link Client} objects.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public List<Bet> findByClientId(String clientIdParam) throws ServiceLayerException;

    /**
     * Returns sums of the stakes bet on each match.
     *
     * @return {@link HashMap} where the key is the id of a match and the value is sum of the stakes bet on this match.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public Map<Integer, Double> getStakesSums() throws ServiceLayerException;

    /**
     * Checks if bet with such client's and matches's id exists.
     *
     * @param clientIdParam client's id.
     * @param matchIdParam id of a match to bet on.
     *
     * @return {@code true} if bet exists, otherwise false.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public boolean doesAlreadyExist(String clientIdParam, String matchIdParam) throws ServiceLayerException;

    /**
     * Inserts a new bet into the database.
     *
     * @param clientIdParam client's id.
     * @param matchIdParam id of a match to bet on.
     * @param stakeParam stake.
     * @param outcomeIdParam id of an outcome.
     *
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public void addBet(String clientIdParam, String matchIdParam, String stakeParam,
                       String outcomeIdParam) throws ServiceLayerException;
}
