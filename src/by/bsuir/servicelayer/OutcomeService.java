package by.bsuir.servicelayer;

import by.bsuir.bean.Outcome;
import by.bsuir.db.exception.ConnectionPoolException;
import by.bsuir.db.exception.DAOException;
import by.bsuir.servicelayer.exception.ServiceLayerException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Interface {@code OutcomeService} provides
 * methods to work with entity Outcome.
 *
 * @author Hamster
 * @since 1.0
 */
public interface OutcomeService {
    /**
     * Finds all the outcomes in the database.
     *
     * @return {@link ArrayList} of {@link Outcome} objects.
     * @throws ServiceLayerException if there's any trouble.
     * @since 1.0
     */
    public List<Outcome> findAll() throws ServiceLayerException;
}
