package by.bsuir.test;

import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;
import org.junit.Test;

/**
 * Created by Hamster on 08.06.2017.
 */
public class TestDataController {

    @Test(expected = DataControlException.class)
    public void testIsNull() throws DataControlException {
        Object object = null;
        DataController.isNull(object);
    }

    @Test(expected = DataControlException.class)
    public void testAreNull() throws DataControlException {
        Object object1 = null;
        Object object2 = null;
        Object object3 = null;
        Object object4 = null;
        DataController.areNull(object1, object2, object3, object4);
    }

    @Test(expected = DataControlException.class)
    public void testIsEmptyString() throws DataControlException {
        String string = "";
        DataController.isEmptyString(string);
    }

    @Test(expected = DataControlException.class)
    public void testAreEmptyStrings() throws DataControlException {
        String str1 = "testing";
        String str2 = "for";
        String str3 = "";
        String str4 = "exception";
        DataController.areEmptyStrings(str1, str2, str3, str4);
    }

}
