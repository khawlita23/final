package by.bsuir.test;

import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.command.impl.ChangeLangCommand;
import by.bsuir.controller.command.impl.GoToAddEventCommand;
import by.bsuir.controller.command.impl.GoToSignUpCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.*;

/**
 * Created by Hamster on 08.06.2017.
 */
public class TestCommand {

    @Test
    public void testGoToAddEventCommand() {
        String expected = "/jsp/addevent.jsp";
        GoToAddEventCommand command = new GoToAddEventCommand();
        String actual = command.execute(new SessionRequestContent());
        assertEquals(expected, actual);
    }

    @Test
    public void testGoToSignUpCommand() {
        String expected = "/jsp/signup.jsp";
        GoToSignUpCommand command = new GoToSignUpCommand();
        String actual = command.execute(new SessionRequestContent());
        assertEquals(expected, actual);
    }

}
