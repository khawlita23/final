package by.bsuir.test;

import by.bsuir.bean.Client;
import by.bsuir.bean.Event;
import by.bsuir.bean.Match;
import by.bsuir.bean.Outcome;
import by.bsuir.db.connectionpool.ConnectionPool;
import by.bsuir.db.dao.impl.ClientDAO;
import by.bsuir.db.dao.impl.MatchDAO;
import by.bsuir.db.dao.impl.OutcomeDAO;
import by.bsuir.db.exception.ConnectionPoolException;
import by.bsuir.db.exception.DAOException;
import by.bsuir.util.DateParser;
import by.bsuir.util.exception.DateParsingException;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Hamster on 05.06.2017.
 */
public class TestDAO {

    private static Logger logger = Logger.getLogger(TestDAO.class.getName());
    private static ConnectionPool pool;

    @BeforeClass
    public static void initConnectionPool() {
        pool = ConnectionPool.getInstance();
        try {
            pool.initPool();
        } catch (ConnectionPoolException e) {
            logger.error("Failed initialising connection pool in test", e);
        }
    }

    @Test
    public void testCreateClient() {
        Client expected = new Client();
        String login = "khawlita23";
        String password = "password23";
        String email = "khawlita23@gmail.com";
        String firstName = "Хауля";
        String lastName = "Альшафи";
        String birthDateStr = "23-07-1996";
        ClientDAO clientDAO = ClientDAO.getInstance();

        try {
            java.sql.Date birthDate = DateParser.parseDate(birthDateStr);

            expected.setLogin(login);
            expected.setPassword(password);
            expected.setEmail(email);
            expected.setFirstName(firstName);
            expected.setLastName(lastName);
            expected.setBirthDate(birthDate);
            clientDAO.create(expected);
        } catch (DAOException e) {
            logger.warn("Failed creating new client while testing", e);
        } catch (DateParsingException e) {
            logger.warn("Failed parsing date while testing", e);
        }

        Client actual = null;
        int clientId;
        try {
            actual = clientDAO.findClientByLogin(login);
            actual.setPassword(password);
            clientId = actual.getClientId();
            expected.setClientId(clientId);
            clientDAO.delete(clientId);
        } catch (DAOException e) {
            logger.warn("Couldn't get or delete client from database while testing", e);
        }
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateMatch() {
        String matchName = "Name of the match";
        Match match = new Match();
        Event event = new Event();
        event.setEventId(1);
        match.setName(matchName);
        match.setEvent(event);
        match.setDate(Timestamp.valueOf("2017-06-21 13:00:00"));
        MatchDAO matchDAO = MatchDAO.getInstance();
        try {
            matchDAO.create(match);
        } catch (DAOException e) {
            logger.warn("Failed creating a new match while testing", e);
        }

        boolean test = false;
        try {
            test = matchDAO.doesAlreadyExist(matchName);
        } catch (DAOException e) {
            logger.warn("Failed checking for the existence of the match while testing.", e);
        }

        try {
            matchDAO.deleteByName(matchName);
        } catch (DAOException e) {
            logger.warn("Couldn't get or delete client from database while testing", e);
        }
        assertTrue(test);

    }

    @Test
    public void testChangePassword() {
        String newPassword  = "Ulaula13";
        String login = "ulaula";
        ClientDAO clientDAO = ClientDAO.getInstance();
        try {
            clientDAO.changePassword(login, newPassword);
        } catch (DAOException e) {
            logger.warn("Couldn't change password of the client \"" + login + "\" while testing");
        }
        boolean actual = false;
        try {
            actual = clientDAO.checkPassword(login, newPassword);
        } catch (DAOException e) {
            logger.warn("Couldn't check password of the client \"" + login + "\" while testing");
        }
        assertTrue(actual);
    }

    @Test
    public void testFindAllOutcomes() {
        List<Outcome> expected = new ArrayList<>();
        expected.add(new Outcome(1, "Ничья"));
        expected.add(new Outcome(2, "Победа 1"));
        expected.add(new Outcome(3, "Победа 2"));
        expected.add(new Outcome(4, "Фора +1.5"));
        expected.add(new Outcome(5, "Фора -1.5"));
        expected.add(new Outcome(6, "Ничья и Тотал четный"));
        expected.add(new Outcome(7, "Тотал четный"));
        expected.add(new Outcome(8, "Тотал нечетный"));
        expected.add(new Outcome(9, "Победа 1 и Тотал четный"));
        expected.add(new Outcome(10, "Победа 1 и Тотал нечетный"));
        expected.add(new Outcome(11, "Победа 2 и Тотал четный"));
        expected.add(new Outcome(12, "Победа 2 и Тотал нечетный"));

        OutcomeDAO outcomeDAO = OutcomeDAO.getInstance();
        List<Outcome> actual = null;
        try {
            actual = outcomeDAO.findAll();
        } catch (DAOException e) {
            logger.warn("Couldn't get all outcomes from database while testing.", e);
        }

        assertEquals(expected, actual);
    }

    @AfterClass
    public static void releaseResources() {
        pool = ConnectionPool.getInstance();
        pool.destroy();
    }

}
