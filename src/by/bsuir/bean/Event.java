package by.bsuir.bean;

import java.io.Serializable;

/**
 * Class {@code Event} is an entity that stores information about rows of
 * the 'events' table in the database.
 * It extends abstract class {@link Entity} and implements interface {@see Serializable}
 *
 * @author Hamster
 * @since 1.0
 */
public class Event extends Entity implements Serializable {

    private Integer eventId;
    private String name;
    private String category;
    private String place;

    public Event() {

    }

    public Event(Integer eventId, String name, String category, String place) {
        this.eventId = eventId;
        this.name = name;
        this.category = category;
        this.place = place;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (eventId != null ? !eventId.equals(event.eventId) : event.eventId != null) return false;
        if (name != null ? !name.equals(event.name) : event.name != null) return false;
        if (category != null ? !category.equals(event.category) : event.category != null) return false;
        return place != null ? place.equals(event.place) : event.place == null;
    }

    @Override
    public int hashCode() {
        int result = eventId != null ? eventId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (place != null ? place.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventId=" + eventId +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", place='" + place + '\'' +
                '}';
    }
}
