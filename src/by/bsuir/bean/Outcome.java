package by.bsuir.bean;

import java.io.Serializable;

/**
 * Class {@code Outcome} is an entity that stores information about rows of
 * the 'outcomes' table in the database.
 * It extends abstract class {@link Entity} and implements interface {@see Serializable}
 *
 * @author Hamster
 * @since 1.0
 */
public class Outcome extends Entity implements Serializable {

    private Integer outcomeId;
    private String value;

    public Outcome() {}

    public Outcome(int outcomeId, String value) {
        this.outcomeId = outcomeId;
        this.value = value;
    }

    public Integer getOutcomeId() {
        return outcomeId;
    }

    public void setOutcomeId(Integer outcomeId) {
        this.outcomeId = outcomeId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Outcome outcome = (Outcome) o;

        if (outcomeId != null ? !outcomeId.equals(outcome.outcomeId) : outcome.outcomeId != null) return false;
        return value != null ? value.equals(outcome.value) : outcome.value == null;
    }

    @Override
    public int hashCode() {
        int result = outcomeId != null ? outcomeId.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Outcome{" +
                "outcomeId=" + outcomeId +
                ", value='" + value + '\'' +
                '}';
    }
}
