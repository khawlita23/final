package by.bsuir.bean;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Class {@code Match} is an entity that stores information about rows of
 * the 'matches' table in the database.
 * It extends abstract class {@link Entity} and implements interface {@see Serializable}
 *
 * @author Hamster
 * @since 1.0
 */
public class Match extends Entity implements Serializable {

    private Integer matchId;
    private String name;
    private Timestamp date;
    private Event event;
    private Outcome outcome;

    public Match(Integer matchId, String name, Timestamp date, Event event, Outcome outcome) {
        this.matchId = matchId;
        this.name = name;
        this.date = date;
        this.event = event;
        this.outcome = outcome;
    }

    public Match() {

    }

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Match match = (Match) o;

        if (matchId != null ? !matchId.equals(match.matchId) : match.matchId != null) return false;
        if (name != null ? !name.equals(match.name) : match.name != null) return false;
        if (date != null ? !date.equals(match.date) : match.date != null) return false;
        if (event != null ? !event.equals(match.event) : match.event != null) return false;
        return outcome != null ? outcome.equals(match.outcome) : match.outcome == null;
    }

    @Override
    public int hashCode() {
        int result = matchId != null ? matchId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (event != null ? event.hashCode() : 0);
        result = 31 * result + (outcome != null ? outcome.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Match{" +
                "matchId=" + matchId +
                ", name='" + name + '\'' +
                ", date=" + date +
                ", event=" + event +
                ", outcome=" + outcome +
                '}';
    }
}
