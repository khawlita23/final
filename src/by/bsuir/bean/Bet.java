package by.bsuir.bean;

import java.io.Serializable;

/**
 * Class {@code Bet} is an entity that stores information about rows of
 * the 'bets' table in the database.
 * It extends abstract class {@link Entity} and implements interface {@see Serializable}
 *
 * @author Hamster
 * @since 1.0
 */
public class Bet extends Entity implements Serializable {

    private Client client;
    private Match match;
    private Double stake;
    private Outcome outcome;

    public Bet(Client client, Match match, Double stake, Outcome outcome) {
        this.client = client;
        this.match = match;
        this.stake = stake;
        this.outcome = outcome;
    }

    public Bet() {}

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Double getStake() {
        return stake;
    }

    public void setStake(Double stake) {
        this.stake = stake;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bet bet = (Bet) o;

        if (client != null ? !client.equals(bet.client) : bet.client != null) return false;
        if (match != null ? !match.equals(bet.match) : bet.match != null) return false;
        if (stake != null ? !stake.equals(bet.stake) : bet.stake != null) return false;
        return outcome != null ? outcome.equals(bet.outcome) : bet.outcome == null;
    }

    @Override
    public int hashCode() {
        int result = client != null ? client.hashCode() : 0;
        result = 31 * result + (match != null ? match.hashCode() : 0);
        result = 31 * result + (stake != null ? stake.hashCode() : 0);
        result = 31 * result + (outcome != null ? outcome.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Bet{" +
                "client=" + client +
                ", match=" + match +
                ", stake=" + stake +
                ", outcome=" + outcome +
                '}';
    }
}
