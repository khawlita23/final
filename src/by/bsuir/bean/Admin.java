package by.bsuir.bean;

import java.io.Serializable;
/**
 * Class {@code Admin} is an entity that stores information about rows of
 * the 'admins' table in the database.
 * It extends abstract class {@link Entity} and implements interface {@see Serializable}
 *
 * @author Hamster
 * @since 1.0
 */
public class Admin extends Entity implements Serializable {

    private Integer adminId;
    private String login;
    private String password;

    public Admin(int adminId, String login, String password) {
        this.adminId = adminId;
        this.login = login;
        this.password = password;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Admin admin = (Admin) o;

        if (adminId != null ? !adminId.equals(admin.adminId) : admin.adminId != null) return false;
        if (login != null ? !login.equals(admin.login) : admin.login != null) return false;
        return password != null ? password.equals(admin.password) : admin.password == null;
    }

    @Override
    public int hashCode() {
        int result = adminId != null ? adminId.hashCode() : 0;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "adminId=" + adminId +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
