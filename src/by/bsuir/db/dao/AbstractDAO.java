package by.bsuir.db.dao;

import by.bsuir.bean.Entity;
import by.bsuir.controller.Controller;
import by.bsuir.db.exception.DAOException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
/**
 * {@code AbstractDAO} is an abstract class which is extended by DAO implementations for
 * each table in the database.
 *
 * @author Hamster
 * @since 1.0
 */
public abstract class AbstractDAO <K, T extends Entity> {

    private Logger logger = Logger.getLogger(AbstractDAO.class.getName());

    protected Connection connection;

    public abstract List<T> findAll() throws DAOException;
    public abstract T findById(K id) throws DAOException;
    public abstract boolean delete(K id) throws DAOException;
    public abstract boolean create(T entity) throws DAOException;

    /**
     * Closes the statement. Before closing it checks if it's null.
     *
     * @param stmt {@link Statement} to close
     *
     * @since 1.0
     */
    protected void close(Statement stmt) {
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (SQLException e) {
            logger.warn("Can't close the Statement.", e);
        }
    }
    /**
     * Closes the connection. Before closing it checks if it's null.
     *
     * @param con {@link Connection} to close
     *
     * @since 1.0
     */
    protected void close(Connection con) {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException e) {
            logger.warn("Can't close the Connection.", e);
        }
    }
    /**
     * Closes the result set. Before closing it checks if it's null.
     *
     * @param rs {@link ResultSet} to close
     *
     * @since 1.0
     */
    protected void close(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            logger.warn("Can't close the ResultSet.", e);
        }
    }

}
