package by.bsuir.db.dao.impl;

import by.bsuir.bean.Outcome;
import by.bsuir.db.connectionpool.ConnectionPool;
import by.bsuir.db.dao.AbstractDAO;
import by.bsuir.db.exception.ConnectionPoolException;
import by.bsuir.db.exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * {@code OutcomeDAO} is a subclass of the {@link AbstractDAO}. It provides
 * methods to work with table 'outcomes' in the database.
 *
 * @author Hamster
 * @since 1.0
 */
public class OutcomeDAO extends AbstractDAO<Integer, Outcome> {

    private static final OutcomeDAO instance = new OutcomeDAO();

    public static OutcomeDAO getInstance() {
        return instance;
    }

    private OutcomeDAO() {
    }

    private static final String SQL_SELECT_ALL_OUTCOMES = "SELECT outcome_id, outcomes.value " +
            "FROM outcomes ORDER BY outcome_id";
    private static final String SQL_SELECT_OUTCOME_BY_ID = "SELECT value " +
            "FROM outcomes WHERE outcome_id = ?";

    /**
     * Finds all outcomes' values in the database.
     *
     *
     * @return {@link ArrayList} of {@link String} objects.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public List<String> findAllValues() throws DAOException {
        List<String> result = new ArrayList<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_SELECT_ALL_OUTCOMES);
            while (rs.next()) {
                String outcomeValue;
                outcomeValue = rs.getString("value");
                result.add(outcomeValue);
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Failed retrieving outcomes values from the database", e);
        }  finally {
            pool.close(con, stmt, rs);
        }

        return result;
    }

    /**
     * Finds all the outcome in the database.
     *
     *
     * @return {@link ArrayList} of {@link Outcome} objects.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public List<Outcome> findAll() throws DAOException {
        List<Outcome> result = new ArrayList<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_SELECT_ALL_OUTCOMES);
            while (rs.next()) {
                Outcome outcome = new Outcome();
                outcome.setOutcomeId(rs.getInt("outcome_id"));
                outcome.setValue(rs.getString("value"));
                result.add(outcome);
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Failed retrieving outcomes data from database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Finds outcome by its id.
     *
     * @param outcomeId match's id.
     *
     * @return {@link Outcome} object.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public Outcome findById(Integer outcomeId) throws DAOException {
        Outcome result = new Outcome();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_OUTCOME_BY_ID);
            stmt.setInt(1, outcomeId);
            rs = stmt.executeQuery();
            if (rs.next()) {
                result.setOutcomeId(outcomeId);
                result.setValue(rs.getString("value"));
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't get outcome by id from the database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean create(Outcome entity) throws DAOException {
        return false;
    }

}
