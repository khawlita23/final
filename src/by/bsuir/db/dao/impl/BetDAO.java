package by.bsuir.db.dao.impl;

import by.bsuir.bean.*;
import by.bsuir.db.connectionpool.ConnectionPool;
import by.bsuir.db.dao.AbstractDAO;
import by.bsuir.db.exception.ConnectionPoolException;
import by.bsuir.db.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * {@code BetDAO} is a subclass of the {@link AbstractDAO}. It provides
 * methods to work with table 'bets' in the database.
 *
 * @author Hamster
 * @since 1.0
 */
public class BetDAO extends AbstractDAO<Integer, Bet> {

    private static Logger logger = Logger.getLogger(BetDAO.class.getName());

    private static final BetDAO instance = new BetDAO();

    public static BetDAO getInstance() {
        return instance;
    }

    private BetDAO() {
    }

    private static final String SQL_SELECT_BETS_BY_CLIENT_ID = "SELECT bets.match_id, m_name, e_name, category, stake, outcomes.value " +
            "FROM bets JOIN matches ON bets.match_id = matches.match_id " +
            "JOIN outcomes ON bets.outcome_id = outcomes.outcome_id " +
            "JOIN events ON matches.event_id = events.event_id WHERE client_id = ?";
    private static final String SQL_INSERT_A_BET = "INSERT INTO bets VALUES (?, ?, ?, ?)";
    private static final String SQL_SELECT_STAKES_SUM_BY_MATCHID = "SELECT SUM(stake) AS total_stakes FROM bets " +
            "WHERE match_id = ?";
    private static final String SQL_SELECT_STAKES_SUMS = "SELECT match_id, SUM(stake) AS total_stakes FROM bets " +
            "GROUP BY match_id;";
    private static final String SQL_DELETE_BET_BY_PK = "DELETE FROM bets WHERE client_id = ? AND match_id = ?";
    private static final String SQL_SELECT_BET_BY_PK = "SELECT client_id, match_id FROM bets WHERE client_id = ? " +
            "AND match_id = ?";
    private static final String SQL_SELECT_STAKES_BY_MATCH_AND_OUTCOME_ID = "SELECT SUM(stake) FROM bets " +
            "WHERE match_id = ? AND outcome_id = ?";

    /**
     * Finds all bets made by the client whose id is {@code clientId}.
     *
     * @param clientId client's id.
     *
     * @return {@link ArrayList} of {@link Client} objects.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public List<Bet> findByClientId(int clientId) throws DAOException {
        List<Bet> result = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        ResultSet rs = null;
        try {
            con = pool.retrieveConnection();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            stmt = con.prepareStatement(SQL_SELECT_BETS_BY_CLIENT_ID);
            stmt.setInt(1, clientId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Bet bet = new Bet();
                Client client = new Client();
                client.setClientId(clientId);
                bet.setClient(client);
                Match match = new Match();
                match.setMatchId(rs.getInt("match_id"));
                match.setName(rs.getString("m_name"));
                Event event = new Event();
                event.setName(rs.getString("e_name"));
                event.setCategory(rs.getString("category"));
                match.setEvent(event);
                bet.setMatch(match);
                bet.setStake(rs.getDouble("stake"));
                Outcome outcome = new Outcome();
                outcome.setValue(rs.getString("value"));
                bet.setOutcome(outcome);
                result.add(bet);
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't get client's bets from database: " + e.getSQLState());
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Inserts a new bet into the database.
     *
     * @param clientId client's id.
     * @param matchId id of a match to bet on.
     * @param stake stake.
     * @param outcomeId id of an outcome.
     *
     * @return {@code true} if new row is successfully inserted, otherwise false.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public boolean create(int clientId, int matchId, double stake, int outcomeId) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_INSERT_A_BET);
            stmt.setInt(1, clientId);
            stmt.setInt(2, matchId);
            stmt.setDouble(3, stake);
            stmt.setInt(4, outcomeId);
            stmt.executeUpdate();
            result = true;
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't add the bet to the database.", e);
        } finally {
            pool.close(con, stmt);
        }
        return result;
    }

    /**
     * Returns sums of the stakes bet on each match.
     *
     * @return {@link HashMap} where the key is the id of a match and the value is sum of the stakes bet on this match.
     * @throws DAOException if there's an {@link SQLException} thrown.
     * @since 1.0
     */
    public Map<Integer, Double> getStakesSums() throws DAOException {
        Map<Integer, Double> result = new HashMap<>();
        Connection con = null;
        Statement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        ResultSet rs = null;
        try {
            con = pool.retrieveConnection();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_SELECT_STAKES_SUMS);
            while (rs.next()) {
                int matchId = rs.getInt("match_id");
                double totalStakes = rs.getDouble("total_stakes");
                result.put(matchId, totalStakes);
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't get stakes sum from the database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    public double getStakesSumByMatchId(int matchId) throws DAOException {
        double result = 0.0;
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        ResultSet rs = null;
        try {
            con = pool.retrieveConnection();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            stmt = con.prepareStatement(SQL_SELECT_STAKES_SUM_BY_MATCHID);
            stmt.setInt(1, matchId);
            rs = stmt.executeQuery();
            if (rs.next()) {
                result = rs.getDouble("total_stakes");
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't get stakes sum from the database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Deletes a bet from the database.
     *
     * @param clientId client's id.
     * @param matchId id of a match.
     *
     * @return {@code true} if new row is successfully deleted, otherwise false.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public boolean delete(int clientId, int matchId) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt =  con.prepareStatement(SQL_DELETE_BET_BY_PK);
            stmt.setInt(1, clientId);
            stmt.setInt(2, matchId);
            stmt.executeUpdate();
            result = true;
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't delete bet from database.", e);
        } finally {
            pool.close(con, stmt);
        }
        return result;
    }

    /**
     * Checks if bet with such client's and matches's id exists.
     *
     * @param clientId client's id.
     * @param matchId id of a match to bet on.
     *
     * @return {@code true} if bet exists, otherwise false.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public boolean doesAlreadyExist(int clientId, int matchId) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_BET_BY_PK);
            stmt.setInt(1, clientId);
            stmt.setInt(2, matchId);
            rs = stmt.executeQuery();
            if (rs.next()) {
                result = true;
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Failed retrieving bet data from database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    @Override
    public List<Bet> findAll() {
        return null;
    }

    @Override
    public Bet findById(Integer id) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean create(Bet entity) {
        return false;
    }


}
