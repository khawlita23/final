package by.bsuir.db.dao.impl;

import by.bsuir.bean.Event;
import by.bsuir.bean.Match;
import by.bsuir.bean.Outcome;
import by.bsuir.db.connectionpool.ConnectionPool;
import by.bsuir.db.dao.AbstractDAO;
import by.bsuir.db.exception.ConnectionPoolException;
import by.bsuir.db.exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * {@code MatchDAO} is a subclass of the {@link AbstractDAO}. It provides
 * methods to work with table 'matches' in the database.
 *
 * @author Hamster
 * @since 1.0
 */
public class MatchDAO extends AbstractDAO<Integer, Match> {

    private static final MatchDAO instance = new MatchDAO();

    public static MatchDAO getInstance() {
        return instance;
    }

    private MatchDAO() {
    }

    private static final String SQL_SELECT_MATCH_BY_ID = "SELECT m_date, m_name, e_name, place, " +
            "            outcomes.outcome_id, outcomes.value AS outcome FROM matches " +
            "            JOIN events " +
            "            ON matches.event_id = events.event_id " +
            "            LEFT OUTER JOIN outcomes " +
            "            ON matches.outcome_id = outcomes.outcome_id " +
            "            WHERE matches.match_id = ?";
    private static final String SQL_INSERT_A_MATCH = "INSERT INTO matches" +
            "(m_name, m_date, event_id, outcome_id)" +
            "VALUES(?, ?, ?, null)";
    private static final String SQL_SELECT_OPEN_MATCHES_BY_CATEGORY = "SELECT matches.match_id, m_date, m_name, e_name, place, " +
            "outcomes.value AS outcome, category FROM matches " +
            "JOIN events " +
            "ON matches.event_id = events.event_id " +
            "LEFT OUTER JOIN outcomes " +
            "ON matches.outcome_id = outcomes.outcome_id " +
            "WHERE category = ? AND matches.outcome_id IS NULL";
    private static final String SQL_SELECT_STAKES_SUMS = " SELECT m_date, m_name, e_name, SUM(stake) AS stakes " +
            "FROM (SELECT m_date, m_name, e_name, stake FROM matches\n" +
            "JOIN events ON matches.event_id = events.event_id\n" +
            "JOIN bets ON matches.match_id = bets.match_id\n" +
            "WHERE matches.outcome_id IS NULL) AS result\n" +
            "GROUP BY m_name";
    private static final String SQL_SELECT_ALL_BY_CATEGORY = "SELECT matches.match_id, m_date, m_name, e_name, place, " +
            "outcomes.outcome_id, outcomes.value AS outcome FROM matches " +
            "JOIN events " +
            "ON matches.event_id = events.event_id " +
            "LEFT OUTER JOIN outcomes " +
            "ON matches.outcome_id = outcomes.outcome_id " +
            "WHERE category = ?";
    private static final String SQL_UPDATE_MATCH = "UPDATE matches SET m_name = ?, " +
            "m_date = ?, outcome_id = ? WHERE match_id = ?;";
    private static final String SQL_SELECT_BY_NAME = "SELECT m_name FROM matches WHERE m_name = ?";
    private static final String SQL_DELETE_MATCH_BY_ID = "DELETE FROM matches WHERE match_id = ?";
    private static final String SQL_DELETE_MATCH_BY_NAME = "DELETE FROM matches WHERE m_name = ?";
    private static final String SQL_SELECT_FINISHED_BY_CATEGORY = "SELECT matches.match_id, m_name, m_date, e_name, " +
            "place, outcomes.value AS outcome FROM matches " +
            "JOIN events ON matches.event_id = events.event_id " +
            "JOIN outcomes ON matches.outcome_id = outcomes.outcome_id " +
            "WHERE events.category = ?";

    /**
     * Finds finished matches of the specified category.
     *
     * @param category matches' category.
     *
     * @return {@link ArrayList} of {@link Match} objects.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public List<Match> findFinishedByCategory(String category) throws DAOException {
        List<Match> result = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            stmt = con.prepareStatement(SQL_SELECT_FINISHED_BY_CATEGORY);
            stmt.setString(1, category);
            rs = stmt.executeQuery();
            while(rs.next()) {
                Match match = new Match();
                match.setMatchId(rs.getInt("match_id"));
                match.setDate(Timestamp.valueOf(rs.getString("m_date")));
                match.setName(rs.getString("m_name"));
                Event event = new Event();
                event.setName(rs.getString("e_name"));
                event.setPlace(rs.getString("place"));
                match.setEvent(event);
                Outcome outcome = new Outcome();
                outcome.setValue(rs.getString("outcome"));
                match.setOutcome(outcome);
                result.add(match);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException("Couldn't get finished matches from database", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Finds unfinished matches of the specified category.
     *
     * @param category matches' category.
     *
     * @return {@link ArrayList} of {@link Match} objects.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public List<Match> findOpenByCategory(String category) throws DAOException {
        List<Match> result = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            stmt = con.prepareStatement(SQL_SELECT_OPEN_MATCHES_BY_CATEGORY);
            stmt.setString(1, category);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Match match = new Match();
                match.setMatchId(rs.getInt("match_id"));
                match.setDate(Timestamp.valueOf(rs.getString("m_date")));
                match.setName(rs.getString("m_name"));
                Event event = new Event();
                event.setName(rs.getString("e_name"));
                event.setPlace(rs.getString("place"));
                match.setEvent(event);
                Outcome outcome = new Outcome();
                outcome.setValue(rs.getString("outcome"));
                match.setOutcome(outcome);
                result.add(match);
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch(SQLException e) {
            throw new DAOException("Failed retrieving matches from the database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Finds all matches of the specified category.
     *
     * @param category matches' category.
     *
     * @return {@link ArrayList} of {@link Match} objects.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public List<Match> findAllByCategory (String category) throws DAOException {
        List<Match> result = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            stmt = con.prepareStatement(SQL_SELECT_ALL_BY_CATEGORY);
            stmt.setString(1, category);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Match match = new Match();
                match.setMatchId(rs.getInt("match_id"));
                match.setDate(Timestamp.valueOf(rs.getString("m_date")));
                match.setName(rs.getString("m_name"));
                Event event = new Event();
                event.setName(rs.getString("e_name"));
                event.setPlace(rs.getString("place"));
                match.setEvent(event);
                Outcome outcome = new Outcome();
                outcome.setOutcomeId(rs.getInt("outcome_id"));
                outcome.setValue(rs.getString("outcome"));
                match.setOutcome(outcome);
                result.add(match);
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch(SQLException e) {
            throw new DAOException("Failed retrieving matches from the database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Returns sums of stakes bet on each match.
     *
     * @return {@link HashMap} with match as a key and sum stakes as a value.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public Map<Match, Double> getStakesSums() throws DAOException {
        Map<Match, Double> result =  new HashMap<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_SELECT_STAKES_SUMS);
            while (rs.next()) {
                Match match = new Match();
                match.setName(rs.getString("m_name"));
                Event event = new Event();
                event.setName(rs.getString("e_name"));
                match.setEvent(event);
                match.setDate(rs.getTimestamp("m_date"));
                double stakes = rs.getDouble("stakes");
                result.put(match, stakes);
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch(SQLException e) {
            throw new DAOException("Failed retrieving matches with stakes sums from the database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }

        return result;
    }

    /**
     * Finds all the matches in the database.
     *
     *
     * @return {@link ArrayList} of {@link Match} objects.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public List<Match> findAll() throws DAOException {
        return null;
    }

    /**
     * Finds match by its id.
     *
     * @param matchId match's id.
     *
     * @return {@link Match} object.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public Match findById(Integer matchId) throws DAOException {
        Match result = new Match();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_MATCH_BY_ID);
            stmt.setInt(1, matchId);
            rs = stmt.executeQuery();
            if(rs.next()) {
                result.setMatchId(matchId);
                result.setDate(Timestamp.valueOf(rs.getString("m_date")));
                result.setName(rs.getString("m_name"));
                Event event = new Event();
                event.setName(rs.getString("e_name"));
                event.setPlace(rs.getString("place"));
                result.setEvent(event);
                Outcome outcome = new Outcome();
                outcome.setOutcomeId(rs.getInt("outcome_id"));
                outcome.setValue(rs.getString("outcome"));
                result.setOutcome(outcome);
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't get match by id from the database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Checks if an match with such name exists.
     *
     * @param matchName name of the match.
     *
     * @return {@code true} if the match exists, otherwise false.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public boolean doesAlreadyExist(String matchName) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_BY_NAME);
            stmt.setString(1, matchName);
            rs = stmt.executeQuery();
            if (rs.next()) {
                result = true;
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Failed retrieving match data from database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Deletes match from the database by its id.
     *
     * @param matchId match's id.
     *
     * @return {@code true} if match is deleted, otherwise {@code false}.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public boolean delete(Integer matchId) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt =  con.prepareStatement(SQL_DELETE_MATCH_BY_ID);
            stmt.setInt(1, matchId);
            stmt.executeUpdate();
            result = true;
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't delete match from database.", e);
        } finally {
            pool.close(con, stmt);
        }
        return result;
    }

    /**
     * Inserts a new match into the database.
     *
     * @param match a {@link Match} object to be inserted.
     *
     * @return {@code true} if new row is successfully inserted, otherwise false.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public boolean create(Match match) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_INSERT_A_MATCH);
            stmt.setString(1, match.getName());
            stmt.setTimestamp(2, match.getDate());
            stmt.setInt(3, match.getEvent().getEventId());
            stmt.executeUpdate();
            result = true;
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't add the match to the database.", e);
        } finally {
            pool.close(con, stmt);
        }
        return result;
    }

    /**
     * Updates information about match passed as an object.
     *
     * @param match {@link Match} object.
     *
     * @return updated {@link Match} object.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public Match update(Match match) throws DAOException {
        Match result = new Match();
        int matchId = match.getMatchId();
        Connection con = null;
        PreparedStatement preparedStmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            con.setAutoCommit(false);
            preparedStmt = con.prepareStatement(SQL_UPDATE_MATCH);
            preparedStmt.setString(1, match.getName());
            preparedStmt.setTimestamp(2, match.getDate());
            preparedStmt.setInt(3, match.getOutcome().getOutcomeId());
            preparedStmt.setInt(4, matchId);
            if (preparedStmt.executeUpdate() == 1) {
                result = match;
            } else {
                preparedStmt = con.prepareStatement(SQL_SELECT_MATCH_BY_ID);
                preparedStmt.setInt(1, matchId);
                rs = preparedStmt.executeQuery();
                if (rs.next()) {
                    result.setMatchId(matchId);
                    result.setDate(rs.getTimestamp(1));
                    result.setName(rs.getString(2));
                    Event event = new Event();
                    event.setName(rs.getString(3));
                    event.setPlace(rs.getString(4));
                    result.setEvent(event);
                    Outcome outcome = new Outcome();
                    outcome.setOutcomeId(rs.getInt(5));
                    outcome.setValue(rs.getString(6));
                    result.setOutcome(outcome);
                }
            }
            con.commit();
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException e1) {
                throw new DAOException("Couldn't rollback.", e);
            }
            throw new DAOException("Couldn't update match properly", e);
        } finally {
            pool.close(con, preparedStmt, rs);
        }
        return result;
    }

    /**
     * Deletes match from the database by its name.
     *
     * @param name match's name.
     *
     * @return {@code true} if match is deleted, otherwise {@code false}.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public boolean deleteByName(String name) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt =  con.prepareStatement(SQL_DELETE_MATCH_BY_NAME);
            stmt.setString(1, name);
            stmt.executeUpdate();
            result = true;
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't delete match from database.", e);
        } finally {
            pool.close(con, stmt);
        }
        return result;
    }
}