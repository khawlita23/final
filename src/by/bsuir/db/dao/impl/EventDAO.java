package by.bsuir.db.dao.impl;

import by.bsuir.bean.Event;
import by.bsuir.db.connectionpool.ConnectionPool;
import by.bsuir.db.dao.AbstractDAO;
import by.bsuir.db.exception.ConnectionPoolException;
import by.bsuir.db.exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * {@code EventDAO} is a subclass of the {@link AbstractDAO}. It provides
 * methods to work with table 'events' in the database.
 *
 * @author Hamster
 * @since 1.0
 */
public class EventDAO extends AbstractDAO<Integer, Event> {

    private static final EventDAO instance = new EventDAO();

    public static EventDAO getInstance() {
        return instance;
    }

    private static final String SQL_SELECT_ALL_EVENTS = "SELECT * FROM events";
    private static final String SQL_SELECT_EVENT_BY_ID = "SELECT * FROM events WHERE event_id=?";
    private static final String SQL_INSERT_EVENT = "INSERT INTO events (e_name, category, place) VALUES " +
            "(?, ?, ?)";
    private static final String SQL_SELECT_BY_NAME = "SELECT e_name FROM events WHERE e_name = ?";

    private EventDAO() {
    }

    /**
     * Finds all the events in the database.
     *
     *
     * @return {@link ArrayList} of {@link Event} objects.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public List<Event> findAll() throws DAOException {
        List<Event> result = new ArrayList<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_SELECT_ALL_EVENTS);
            while (rs.next()) {
                Event event = new Event();
                event.setEventId(rs.getInt(1));
                event.setName(rs.getString(2));
                event.setCategory(rs.getString(3));
                event.setPlace(rs.getString(4));
                result.add(event);
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Failed retrieving events data from database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Finds event by its id.
     *
     * @param eventId event's id.
     *
     * @return {@link Event} object.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public Event findById(Integer eventId) throws DAOException {
        Event result = new Event();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_EVENT_BY_ID);
            stmt.setInt(1, eventId);
            rs = stmt.executeQuery();
            if (rs.next()) {
                result.setEventId(eventId);
                result.setName(rs.getString(2));
                result.setCategory(rs.getString(3));
                result.setPlace(rs.getString(4));
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't get outcome by id from the database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Checks if an event with such name exists.
     *
     * @param eventName name of the event.
     *
     * @return {@code true} if the event exists, otherwise false.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public boolean doesAlreadyExist(String eventName) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_BY_NAME);
            stmt.setString(1, eventName);
            rs = stmt.executeQuery();
            if (rs.next()) {
                result = true;
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Failed retrieving event data from database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    /**
     * Inserts a new event into the database.
     *
     * @param event an event to be inserted.
     *
     * @return {@code true} if new row is successfully inserted, otherwise false.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public boolean create(Event event) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_INSERT_EVENT);
            stmt.setString(1, event.getName());
            stmt.setString(2, event.getCategory());
            stmt.setString(3, event.getPlace());
            stmt.executeUpdate();
            result = true;
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't create new event", e);
        } finally {
            pool.close(con, stmt);
        }
        return result;
    }

}
