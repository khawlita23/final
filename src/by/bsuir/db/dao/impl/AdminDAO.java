package by.bsuir.db.dao.impl;

import by.bsuir.bean.Admin;
import by.bsuir.db.connectionpool.ConnectionPool;
import by.bsuir.db.dao.AbstractDAO;
import by.bsuir.db.exception.ConnectionPoolException;
import by.bsuir.db.exception.DAOException;
import by.bsuir.util.PasswordEncrypter;

import java.sql.*;
import java.util.List;

/**
 * {@code AdminDAO} is a subclass of the {@link AbstractDAO}. It provides
 * methods to work with table 'admins' in the database.
 *
 * @author Hamster
 * @since 1.0
 */
public class AdminDAO extends AbstractDAO<Integer, Admin> {

    private static final String SQL_SELECT_ADMIN_PASSWORD = "SELECT ad_password FROM admins WHERE ad_login=?";

    private static final AdminDAO instance = new AdminDAO();

    public static AdminDAO getInstance() {
        return instance;
    }

    private AdminDAO() {
    }

    /**
     * Checks login and password entered by user.
     *
     * @param login login to check.
     * @param password password to check.
     *
     * @return {@code true} if password and login match data in the database, otherwise {@code false}.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public boolean checkPassword(String login, String password) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_ADMIN_PASSWORD);
            stmt.setString(1, login);
            rs = stmt.executeQuery();
//            String encryptedPassword = PasswordEncrypter.encryptPassword(password);
            if (rs.next()) {
                String dbPass = rs.getString("ad_password");
                if(dbPass.equals(password)) {
                    result = true;
                }
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't retrieve admin's password from database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    @Override
    public List<Admin> findAll() throws DAOException {
        return null;
    }

    @Override
    public Admin findById(Integer id) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    @Override
    public boolean create(Admin entity) throws DAOException {
        return false;
    }
}
