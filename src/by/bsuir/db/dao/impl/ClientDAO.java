package by.bsuir.db.dao.impl;

import by.bsuir.bean.Client;
import by.bsuir.db.connectionpool.ConnectionPool;
import by.bsuir.db.dao.AbstractDAO;
import by.bsuir.db.exception.ConnectionPoolException;
import by.bsuir.db.exception.DAOException;
import by.bsuir.util.PasswordEncrypter;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * {@code ClientDAO} is a subclass of the {@link AbstractDAO}. It provides
 * methods to work with table 'clients' in the database.
 *
 * @author Hamster
 * @since 1.0
 */
public class ClientDAO extends AbstractDAO<Integer, Client> {

    private static final String SQL_SELECT_ALL_CLIENTS = "SELECT * FROM clients";
    private static final String SQL_INSERT_A_CLIENT = "INSERT INTO clients" +
            "(login, password, email, f_name, l_name, birth_date)" +
            "VALUES(?, ?, ?, ?, ?, ?)";
    private static final String SQL_SELECT_LOGIN = "SELECT login FROM clients WHERE login = ?";
    private static final String SQL_SELECT_CLIENT_PASSWORD = "SELECT password " +
            "FROM clients WHERE login = ?";
    private static final String SQL_SELECT_NAME_BY_LOGIN = "SELECT f_name FROM clients WHERE login = ?";
    private static final String SQL_SELECT_ID_BY_LOGIN = "SELECT client_id FROM clients WHERE login = ?";
    private static final String SQL_UPDATE_PASSWORD = "UPDATE clients SET password=? WHERE login=?";
    private static final String SQL_SELECT_CLIENT_BY_LOGIN = "SELECT * FROM clients WHERE login=?";
    private static final String SQL_SELECT_CLIENT_BY_ID = "SELECT client_id, login, email, f_name, l_name, birth_date " +
            "FROM clients WHERE client_id = ?";
    private static final String SQL_DELETE_CLIENT_BY_ID = "DELETE FROM clients WHERE client_id = ?";

    private static final ClientDAO instance = new ClientDAO();

    public static ClientDAO getInstance() {
        return instance;
    }

    private ClientDAO() {

    }

    /**
     * Checks login and password entered by user.
     *
     * @param login login to check.
     * @param password password to check.
     *
     * @return {@code true} if password and login match data in the database, otherwise {@code false}.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public boolean checkPassword(String login, String password) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_CLIENT_PASSWORD);
            stmt.setString(1, login);
            rs = stmt.executeQuery();
//            String encryptedPassword = PasswordEncrypter.encryptPassword(password);
            if (rs.next()) {
                String dbPass = rs.getString("password");
                if (dbPass.equals(password)) {
                    result = true;
                }
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't retrieve client's password from database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Checks if such login already exists.
     *
     * @param login login to check.
     *
     * @return {@code true} if such login exists, otherwise {@code false}.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public boolean checkLogin(String login) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_LOGIN);
            stmt.setString(1, login);
            rs = stmt.executeQuery();
            String dbLogin;
            if (rs.next()) {
                dbLogin = rs.getString("login");
                if (dbLogin.equals(login)) {
                    result = true;
                }
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't find login in database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Finds client's name by his login.
     *
     * @param login login to check.
     *
     * @return name of the client.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public String findNameByLogin(String login) throws DAOException {
        String result = "";
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        ResultSet rs = null;
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_NAME_BY_LOGIN);
            stmt.setString(1, login);
            rs = stmt.executeQuery();
            rs.next();
            result = rs.getString("f_name");
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't get client's first name from database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Finds client's login by his id.
     *
     * @param login client's login.
     *
     * @return id of the client with such name.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public int findIdByLogin(String login) throws DAOException {
        int result = 0;
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_ID_BY_LOGIN);
            stmt.setString(1, login);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            result = rs.getInt("client_id");
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't get client's id from database", e);
        } finally {
            pool.close(con, stmt);
        }
        return result;
    }

    /**
     * Finds all the clients in the database.
     *
     *
     * @return {@link ArrayList} of {@link Client} objects.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public List<Client> findAll() throws DAOException {
        List<Client> clients = new ArrayList<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_SELECT_ALL_CLIENTS);
            while (rs.next()) {
                Client client = new Client();
                client.setClientId(rs.getInt("client_id"));
                client.setLogin(rs.getString("login"));
                client.setPassword(rs.getString("password"));
                client.setEmail(rs.getString("email"));
                client.setFirstName(rs.getString("f_name"));
                client.setLastName(rs.getString("l_name"));
                client.setBirthDate(rs.getDate("birth_date"));
                clients.add(client);
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Failed retrieving clients data from database.", e);
        } finally {
            pool.close(con, stmt, rs);
        }

        return clients;
    }

    /**
     * Changes client's password.
     *
     * @param login client's login.
     * @param newPassword new password.
     *
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public void changePassword(String login, String newPassword) throws DAOException {
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_UPDATE_PASSWORD);
//            String newEncryptedPassword = PasswordEncrypter.encryptPassword(newPassword);
            stmt.setString(1, newPassword);
            stmt.setString(2, login);
            stmt.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Failed updating password in the database", e);
        } finally {
            pool.close(con, stmt);
        }
    }

    /**
     * Finds client by his login.
     *
     * @param login client's login.
     *
     * @return {@link Client} object wit such login.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    public Client findClientByLogin(String login) throws DAOException {
        Client result = new Client();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_CLIENT_BY_LOGIN);
            stmt.setString(1, login);
            rs = stmt.executeQuery();
            if (rs.next()) {
                result.setClientId(rs.getInt(1));
                result.setLogin(rs.getString(2));
                result.setEmail(rs.getString(4));
                result.setFirstName(rs.getString(5));
                result.setLastName(rs.getString(6));
                result.setBirthDate(rs.getDate(7));
            } else {
                throw new DAOException("There's no client with such login");
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Failed retrieving client by login from the database: " + e.getSQLState(), e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Finds client by his id.
     *
     * @param clientId client's id.
     *
     * @return {@link Client} object.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public Client findById(Integer clientId) throws DAOException {
        Client result = new Client();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_SELECT_CLIENT_BY_ID);
            stmt.setInt(1, clientId);
            rs = stmt.executeQuery();
            if (rs.next()) {
                result.setClientId(rs.getInt("client_id"));
                result.setFirstName(rs.getString("f_name"));
                result.setLastName(rs.getString("l_name"));
                result.setBirthDate(rs.getDate("birth_date"));
                result.setEmail(rs.getString("email"));
                result.setLogin(rs.getString("login"));
            }
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't get client by id.", e);
        } finally {
            pool.close(con, stmt, rs);
        }
        return result;
    }

    /**
     * Deletes client from the database by his id.
     *
     * @param clientId client's id.
     *
     * @return {@code true} if client is deleted, otherwise {@code false}.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public boolean delete(Integer clientId) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            con = pool.retrieveConnection();
            stmt =  con.prepareStatement(SQL_DELETE_CLIENT_BY_ID);
            stmt.setInt(1, clientId);
            stmt.executeUpdate();
            result = true;
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't delete client from database.", e);
        } finally {
            pool.close(con, stmt);
        }
        return result;
    }

    /**
     * Inserts a new client into the database.
     *
     * @param client {@link Client} object to be inserted.
     *
     * @return {@code true} if new row is successfully inserted, otherwise false.
     * @throws DAOException if there's an {@link SQLException} or {@link ConnectionPoolException} thrown.
     * @since 1.0
     */
    @Override
    public boolean create(Client client) throws DAOException {
        boolean result = false;
        Connection con = null;
        PreparedStatement stmt = null;
        ConnectionPool pool = ConnectionPool.getInstance();
//        String encryptedPassword = PasswordEncrypter.encryptPassword(client.getPassword());
        try {
            con = pool.retrieveConnection();
            stmt = con.prepareStatement(SQL_INSERT_A_CLIENT);
            stmt.setString(1, client.getLogin());
            stmt.setString(2, client.getPassword());
            stmt.setString(3, client.getEmail());
            stmt.setString(4, client.getFirstName());
            stmt.setString(5, client.getLastName());
            stmt.setString(6, client.getBirthDate().toString());
            stmt.executeUpdate();
            result = true;
        } catch (ConnectionPoolException e) {
            throw new DAOException("Failed getting the connection from the pool", e);
        } catch (SQLException e) {
            throw new DAOException("Couldn't add client to the database.", e);
        } finally {
            pool.close(con, stmt);
        }
        return result;
    }


}
