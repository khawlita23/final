package by.bsuir.db.connectionpool;
/**
 * Class {@code DBParameter} holds names of database parameters properties.
 *
 * @author Hamster
 * @since 1.0
 */
public final class DBParameter {

    public static final String DB_DRIVER = "db.driver";
    public static final String DB_URL = "db.url";
    public static final String DB_USERNAME = "db.username";
    public static final String DB_PASSWORD = "db.password";
    public static final String DB_MIN_POOLSIZE = "db.minpoolsize";
    public static final String DB_MAX_POOLSIZE = "db.maxpoolsize";

}
