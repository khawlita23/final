package by.bsuir.db.connectionpool;

import by.bsuir.db.exception.ConnectionPoolException;
import by.bsuir.resource.DatabaseManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class {@code ConnectionPool} is created for connection pooling.
 * It is a Singleton and it has method {@code init()}, which fills queues with opened connections.
 * Also this class provides such methods for closing {@code Connection}, {@code Statement} and {@code ResultSet}
 * objects.
 *
 * @author Hamster
 * @since 1.0
 */
public final class ConnectionPool {

    private String driver;
    private String url;
    private String username;
    private String password;
    private int minPoolSize;
    private int maxPoolSize;
    private ArrayBlockingQueue<Connection> idleConnections;
    private ArrayBlockingQueue<Connection> givenAwayConnections;

    private Logger logger = Logger.getLogger(ConnectionPool.class.getName());

    private Lock lock = new ReentrantLock();
    private Condition free = lock.newCondition();

    private final static ConnectionPool instance = new ConnectionPool();

    public static ConnectionPool getInstance() {
        return instance;
    }

    private ConnectionPool() {
        DatabaseManager dbRM = DatabaseManager.getInstance();
        this.driver = dbRM.getProperty(DBParameter.DB_DRIVER);
        this.url = dbRM.getProperty(DBParameter.DB_URL);
        this.username = dbRM.getProperty(DBParameter.DB_USERNAME);
        this.password = dbRM.getProperty(DBParameter.DB_PASSWORD);
        try {
            this.minPoolSize = Integer.parseInt(dbRM.getProperty(DBParameter.DB_MIN_POOLSIZE));
            this.maxPoolSize = Integer.parseInt(dbRM.getProperty(DBParameter.DB_MAX_POOLSIZE));
        } catch (NumberFormatException e) {
            logger.warn("Failed parsing the value of pool size parameter", e);
            this.minPoolSize = 5;
        }
    }
    /**
     * Initialises pool, filling <code>idleConnectionsQueue</code>
     * with opened connections.
     *
     * @throws ConnectionPoolException if driver can't be found.
     * @since 1.0
     */
    public void initPool() throws ConnectionPoolException {
        try {
            Class.forName(driver);
            idleConnections = new ArrayBlockingQueue<>(maxPoolSize);
            givenAwayConnections = new ArrayBlockingQueue<>(maxPoolSize);
            for (int i = 0; i < minPoolSize; i++) {
                ConnectionWrapper connectionWrapper = new ConnectionWrapper(getConnection());
                idleConnections.add(connectionWrapper);
            }
        } catch (ClassNotFoundException e) {
            throw new ConnectionPoolException("Unable to find database driver class.", e);
        } catch (SQLException e) {
            logger.warn("Proplem getting new connection", e);
        }
    }
    /**
     * Returns a new connection with the database.
     *
     * @return a new connection with the database.
     *
     * @since 1.0
     */
    private Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            logger.warn("Couldn't get new connection", e);
        }
        return connection;
    }
    /**
     * Returns a connection from the <code>idleConnectionsQueue</code> and before that puts it into
     * <code>givenAwayConnections</code>.
     *
     * @return a connection from the <code>idleConnectionsQueue</code>.
     *
     * @throws ConnectionPoolException in case of interrupted access to the queue.
     *
     * @since 1.0
     */
    public Connection retrieveConnection() throws ConnectionPoolException {
        Connection connection = null;
        try {
            lock.lock();
            if (idleConnections.isEmpty()) {
                connection = getConnection();
            } else {
                connection = idleConnections.take();
            }
            givenAwayConnections.add(connection);
            free.signal();
        } catch (InterruptedException e) {
            throw new ConnectionPoolException("Couldn't get connection from the queue", e);
        } finally {
            lock.unlock();
        }
        return connection;

    }
    /**
     * Closes <code>Connection</code>, <code>Statement</code> and <code>ResultSet</code>
     * objects, if they are not <code>null</code>.
     *
     * @param con <code>Connection</code> object
     * @param stmt <code>Statement</code> object
     * @param rs <code>ResultSet</code> object
     *
     * @since 1.0
     */
    public void close(Connection con, Statement stmt, ResultSet rs) {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException e) {
            logger.error("Couldn't return connection to the pool");
        }
        try {
            if (rs != null) {
                rs.close();
            }
        } catch(SQLException e) {
            logger.error("Couldn't close ResultSet", e);
        }
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (SQLException e) {
            logger.error("Couldn't close Statement", e);
        }
    }
    /**
     * Closes <code>Connection</code> and <code>Statement</code>
     * objects, if they are not <code>null</code>.
     *
     * @param con <code>Connection</code> object
     * @param stmt <code>Statement</code> object
     *
     * @since 1.0
     */
    public void close(Connection con, Statement stmt) {
        close(con, stmt, null);
    }
    /**
     * Closes <code>Connection</code> and <code>Statement</code>
     * objects, if they are not <code>null</code>.
     *
     * @param queue queue with connections
     *
     * @throws SQLException in case {@code getAutoCommit()} throws it
     *
     * @since 1.0
     */
    private void closeConnectionsQueue(ArrayBlockingQueue<Connection> queue) throws SQLException {
        Connection connection;
        while ((connection = queue.poll()) != null) {
            if (!connection.getAutoCommit()) {
                connection.commit();
            }
            ((ConnectionWrapper) connection).reallyClose();
        }
    }
    /**
     * Destroys all the data in {@code ConnectionPool}, i.e.
     * closes all the connections in both queues.
     *
     * @since 1.0
     */
    public void clearConnectionsQueue() {
        try {
            closeConnectionsQueue(idleConnections);
            closeConnectionsQueue(givenAwayConnections);
        } catch (SQLException e) {
            logger.error("Failed closing all the connections.", e);
        }
    }
    /**
     * Destroys all the data in {@code ConnectionPool}, i.e.
     * closes all the connections in both queues.
     *
     * @since 1.0
     */
    public void destroy() {
        clearConnectionsQueue();
    }

    /**
     * Inner class {@code ConnectionWrapper} is created for connection pooling.
     * It acts as a wrapper for the {@link Connection} object.
     * This class implements {@link Connection} interface's methods by
     * invoking them on inner {@link Connection} object. The only difference is in
     * method {@code close()} which here returns connection to the pool instead of
     * closing it.
     *
     * @author Hamster
     * @since 1.0
     */
    private class ConnectionWrapper implements Connection {

        private Connection connection;

        /**
         * Creates {@code ConnectionWrapper} objects and sets the stored connection.
         * It also invokes method {@link Connection#setAutoCommit(boolean)} to true.
         * @param con stored connection.
         * @since 1.0
         */
        public ConnectionWrapper(Connection con) throws SQLException {
            this.connection = con;
            this.connection.setAutoCommit(true);
        }

        public void reallyClose() throws SQLException {
            connection.close();
        }

        @Override
        public void close() throws SQLException {
            if(this.isClosed()) {
                throw new SQLException("Trying to put closed connection in the pool");
            }
            if (!givenAwayConnections.remove(this)) {
                throw new SQLException("Failed removing connection from given away queue.");
            }
            if (!idleConnections.offer(this)) {
                throw new SQLException("Couldn't put connection in the pool.");
            }
        }

        public void commit() throws SQLException {
            connection.commit();
        }

        @Override
        public Statement createStatement() throws SQLException {
            return connection.createStatement();
        }

        @Override
        public PreparedStatement prepareStatement(String sql) throws SQLException {
            return connection.prepareStatement(sql);
        }

        @Override
        public CallableStatement prepareCall(String sql) throws SQLException {
            return connection.prepareCall(sql);
        }

        @Override
        public String nativeSQL(String sql) throws SQLException {
            return connection.nativeSQL(sql);
        }

        @Override
        public void setAutoCommit(boolean autoCommit) throws SQLException {
            connection.setAutoCommit(autoCommit);
        }

        @Override
        public boolean getAutoCommit() throws SQLException {
            return connection.getAutoCommit();
        }

        @Override
        public void rollback() throws SQLException {
            connection.rollback();
        }

        @Override
        public boolean isClosed() throws SQLException {
            return connection.isClosed();
        }

        @Override
        public DatabaseMetaData getMetaData() throws SQLException {
            return connection.getMetaData();
        }

        @Override
        public void setReadOnly(boolean readOnly) throws SQLException {
            connection.setReadOnly(readOnly);
        }

        @Override
        public boolean isReadOnly() throws SQLException {
            return connection.isReadOnly();
        }

        @Override
        public void setCatalog(String catalog) throws SQLException {
            connection.setCatalog(catalog);
        }

        @Override
        public String getCatalog() throws SQLException {
            return connection.getCatalog();
        }

        @Override
        public void setTransactionIsolation(int level) throws SQLException {
            connection.setTransactionIsolation(level);
        }

        @Override
        public int getTransactionIsolation() throws SQLException {
            return connection.getTransactionIsolation();
        }

        @Override
        public SQLWarning getWarnings() throws SQLException {
            return connection.getWarnings();
        }

        @Override
        public void clearWarnings() throws SQLException {
            connection.clearWarnings();
        }

        @Override
        public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
        }

        @Override
        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
        }

        @Override
        public Map<String, Class<?>> getTypeMap() throws SQLException {
            return connection.getTypeMap();
        }

        @Override
        public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
            connection.setTypeMap(map);
        }

        @Override
        public void setHoldability(int holdability) throws SQLException {
            connection.setHoldability(holdability);
        }

        @Override
        public int getHoldability() throws SQLException {
            return connection.getHoldability();
        }

        @Override
        public Savepoint setSavepoint() throws SQLException {
            return connection.setSavepoint();
        }

        @Override
        public Savepoint setSavepoint(String name) throws SQLException {
            return connection.setSavepoint(name);
        }

        @Override
        public void rollback(Savepoint savepoint) throws SQLException {
            connection.rollback();
        }

        @Override
        public void releaseSavepoint(Savepoint savepoint) throws SQLException {
            connection.releaseSavepoint(savepoint);
        }

        @Override
        public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
            return connection.prepareStatement(sql, autoGeneratedKeys);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
            return connection.prepareStatement(sql, columnIndexes);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
            return connection.prepareStatement(sql, columnNames);
        }

        @Override
        public Clob createClob() throws SQLException {
            return connection.createClob();
        }

        @Override
        public Blob createBlob() throws SQLException {
            return connection.createBlob();
        }

        @Override
        public NClob createNClob() throws SQLException {
            return connection.createNClob();
        }

        @Override
        public SQLXML createSQLXML() throws SQLException {
            return connection.createSQLXML();
        }

        @Override
        public boolean isValid(int timeout) throws SQLException {
            return connection.isValid(timeout);
        }

        @Override
        public void setClientInfo(String name, String value) throws SQLClientInfoException {
            connection.setClientInfo(name, value);
        }

        @Override
        public void setClientInfo(Properties properties) throws SQLClientInfoException {
            connection.setClientInfo(properties);
        }

        @Override
        public String getClientInfo(String name) throws SQLException {
            return connection.getClientInfo(name);
        }

        @Override
        public Properties getClientInfo() throws SQLException {
            return connection.getClientInfo();
        }

        @Override
        public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
            return connection.createArrayOf(typeName, elements);
        }

        @Override
        public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
            return connection.createStruct(typeName, attributes);
        }

        @Override
        public void setSchema(String schema) throws SQLException {
            connection.setSchema(schema);
        }

        @Override
        public String getSchema() throws SQLException {
            return connection.getSchema();
        }

        @Override
        public void abort(Executor executor) throws SQLException {
            connection.abort(executor);
        }

        @Override
        public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
            connection.setNetworkTimeout(executor, milliseconds);
        }

        @Override
        public int getNetworkTimeout() throws SQLException {
            return connection.getNetworkTimeout();
        }

        @Override
        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return connection.isWrapperFor(iface);
        }

        @Override
        public <T> T unwrap(Class<T> iface) throws SQLException {
            return connection.unwrap(iface);
        }
    }
}
