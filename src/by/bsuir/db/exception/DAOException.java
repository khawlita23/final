package by.bsuir.db.exception;

/**
 * Exception {@code DAOException} is thrown when there are any problems while working with the database.
 *
 * @author Hamster
 * @since 1.0
 */
public class DAOException extends Exception {

    public DAOException() {
        super();
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
