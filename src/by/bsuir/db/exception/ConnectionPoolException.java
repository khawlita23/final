package by.bsuir.db.exception;

/**
 * Exception {@code ConnectionPoolException} is thrown when there are any problems with getting connection
 * from the connection pool or doing sonething else with it.
 *
 * @author Hamster
 * @since 1.0
 */
public class ConnectionPoolException extends Exception {

    public ConnectionPoolException() {
        super();
    }

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionPoolException(Throwable cause) {
        super(cause);
    }
}
