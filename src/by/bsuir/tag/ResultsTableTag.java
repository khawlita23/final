package by.bsuir.tag;

import by.bsuir.bean.Match;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * {@code GreetingTag} is a handler for the 'results-table' tag. This tag takes the list of finished matches,
 * map with the total stakes on each match and headings for the table and displays a table with all this
 * information.
 *
 * @author Hamster
 * @since 1.0
 */
public class ResultsTableTag extends TagSupport {

    private static Logger logger = Logger.getLogger(ResultsTableTag.class.getName());

    private List<String> headings;
    private List<Match> resultslist;
    private Map<Integer, Double> totalstakes;

    public void setHeadings(List<String> headings) {
        this.headings = headings;
    }

    public void setResultslist(List<Match> resultslist) {
        this.resultslist = resultslist;
    }

    public void setTotalstakes(Map<Integer, Double> totalstakes) {
        this.totalstakes = totalstakes;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.write("<table>");
            out.write("<tr>");
            for (String head : headings) {
                out.write("<th>" + head + "</th>");
            }
            for (Match match : resultslist) {
                int matchId = match.getMatchId();
                out.write("<tr>");
                out.write("<td>" + match.getDate().toString().substring(0, 16) + "</td>");
                out.write("<td>");
                out.write("<div class=\"cat\">" + match.getEvent().getName() + "</div>");
                out.write(match.getName());
                out.write("</td>");
                out.write("<td>" + match.getEvent().getPlace() + "</td>");
                out.write("<td>" + match.getOutcome().getValue() + "</td>");
                out.write("<td style=\"border: 1px solid #000;\">");
                double stakes = totalstakes.get(matchId);
                if (stakes != 0) {
                    out.write(Double.toString(stakes));
                } else {
                    out.write("0");
                }
                out.write("</td>");
                out.write("</tr>");
                /*out.write("<form action=\"controller\" method=\"POST\" id=\"form" + clientId + "\">");
                out.write("<input id=\"input" + clientId + "\" form=\"form" + clientId + "\" type=\"hidden\" name=\"command\" value=\"gotouserprofile\">");
                out.write("<input id=\"input" + clientId + "\" form=\"form" + clientId + "\" type=\"hidden\" name=\"clientId\" value=\"" + clientId + "\">");
                out.write("<input id=\"input" + clientId + "\" form=\"form" + clientId + "\" type=\"submit\" value=\"" + client.getLogin() + "\" class=\"login-link\">");
                out.write("</form");
                out.write("</td>");
                out.write("<td>" + client.getFirstName() + "</td>");
                out.write("<td>" + client.getLastName() + "</td>");
                out.write("<td>" + client.getBirthDate() + "</td>");
                out.write("<td>" + client.getEmail() + "</td>");
                out.write("</tr>");*/
            }

        } catch (IOException e) {
            logger.error("Tag failed writing headings to the output stream", e);
        }
        return SKIP_BODY;
    }

    /*@Override
    public int doAfterBody() throws JspException {
        JspWriter out = pageContext.getOut();
        for (Client client : clientslist) {
            try {
                out.write("<tr>");
                out.write("<td>");
                out.write("<form action=\"controller\" method=\"POST\">");
                out.write("<input type=\"hidden\" name=\"command\" value=\"gotouserprofile\">");
                out.write("<input type=\"hidden\" name=\"clientId\" value=\"" + client.getClientId() + "\">");
                out.write("<input type=\"submit\" value=\"" + client.getLogin() + "\" class=\"login-link\">");
                out.write("</td>");
                out.write("<td><c:out value=\"" + client.getFirstName() + "\"/></td>");
                out.write("<td><c:out value=\"" + client.getLastName() + "\"/></td>");
                out.write("<td><c:out value=\"" + client.getBirthDate() + "\"/></td>");
                out.write("<td><c:out value=\"" + client.getEmail() + "\"/></td>");
                out.write("</tr>");
            } catch (IOException e) {
                logger.error("Tag failed writing table data to the output stream", e);
            }
        }

    }*/

    @Override
    public int doEndTag() throws JspException {
        try {
            pageContext.getOut().write("</table>");
        } catch (IOException e) {
            logger.error("Tag failed writing the end of the table to the output stream", e);
        }
        return EVAL_PAGE;
    }
}
