package by.bsuir.tag;

import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * {@code GreetingTag} is a handler for the 'greeting' tag. This tag takes the name of the user and the message
 * and displays them on the page.
 *
 * @author Hamster
 * @since 1.0
 */
public class GreetingTag extends TagSupport {

    private static final Logger logger = Logger.getLogger(GreetingTag.class.getName());

    private String username;
    private String greeting;

    public void setUsername(String username) {
        this.username = username;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    @Override
    public int doStartTag() throws JspException {
        String result = null;
        try {
            if (username != null) {
                result = greeting + ", " + username + "!";
            }
            JspWriter out = pageContext.getOut();
            out.write("<div class=\"greeting\">" + result + "</div>");
        } catch (IOException e) {
            logger.error("Tag failed writing to the output stream", e);
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
