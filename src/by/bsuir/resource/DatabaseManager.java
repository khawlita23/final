package by.bsuir.resource;

import java.util.ResourceBundle;

/**
 * {@code DatabaseManager} is a class responsible for getting properties from the file
 * {@code db.properties} which contains properties for establishing a connection pool.
 *
 * @author Hamster
 * @since 1.0
 */
public class DatabaseManager {

    private final static DatabaseManager instance = new DatabaseManager();

    public static DatabaseManager getInstance() {
        return instance;
    }

    private ResourceBundle bundle = ResourceBundle.getBundle("resources.db");

    public String getProperty(String key) {
        return bundle.getString(key);
    }

}
