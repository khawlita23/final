package by.bsuir.resource;

import java.util.ResourceBundle;

/**
 * {@code MessageManager} is a class responsible for getting properties from the file
 * {@code messages.properties} which contains properties with some of the messages displayed
 * to the user.
 *
 * @author Hamster
 * @since 1.0
 */
public class MessageManager {

    private final static ResourceBundle bundle = ResourceBundle.getBundle("resources.messages");

    private MessageManager() {}

    public static String getProperty(String key) {
        return bundle.getString(key);
    }

}
