package by.bsuir.resource;

import java.util.ResourceBundle;

/**
 * {@code ConfigurationManager} is a class responsible for getting properties from the file
 * {@code config.properties} which contains properties with names of all the pages in the application.
 *
 * @author Hamster
 * @since 1.0
 */
public class ConfigurationManager {

    private final static ResourceBundle bundle = ResourceBundle.getBundle("resources.config");

    private ConfigurationManager() {}

    public static String getProperty(String key) {
        return bundle.getString(key);
    }

}
