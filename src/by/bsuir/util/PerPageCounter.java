package by.bsuir.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class {@code DateParser} is an util class which is responsible for getting
 * the {@link ArrayList} of needed length.
 *
 * @author Hamster
 * @since 1.0
 */
public class PerPageCounter {

    /**
     * This method encrypts the given password using MD5 algorithm.
     *
     * @param originalList oriiginal list of objects.
     * @param recordsPerPage records per page.
     * @param toPage page to move to.
     * @param numberOfPages needed number of pages.
     *
     * @return {@link ArrayList} of objects.
     *
     * @since 1.0
     */
    public static <T> List<T> getPerPage(List<T> originalList, int recordsPerPage, int toPage, int numberOfPages) {

        List<T> result;

        int numberOfRecords = originalList.size();
//        int numberOfPages = (int) Math.ceil(numberOfRecords * 1.0 / recordsPerPage);

        int fromIndex;
        int toIndex;

        fromIndex = (toPage - 1) * recordsPerPage;

        if (numberOfRecords % recordsPerPage != 0 && toPage == numberOfPages) {
            toIndex = fromIndex + numberOfRecords % recordsPerPage;
        } else {
            toIndex = fromIndex + recordsPerPage;
        }

        result = originalList.subList(fromIndex, toIndex);

        return result;
    }

}
