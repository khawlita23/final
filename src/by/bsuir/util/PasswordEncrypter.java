package by.bsuir.util;

import org.apache.log4j.Logger;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class {@code DateParser} is an util class which is responsible for encrypting password
 * using MD5 algorithm.
 *
 * @author Hamster
 * @since 1.0
 */
public class PasswordEncrypter {

    private static final Logger logger = Logger.getLogger(PasswordEncrypter.class.getName());

    /**
     * This method encrypts the given password using MD5 algorithm.
     *
     * @param password password to be encrypted.
     *
     * @since 1.0
     */
    public static String encryptPassword(String password) {
        String hashText = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(password.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            hashText = number.toString(16);
            while (hashText.length() < 32) {
                hashText = "0" + hashText;
            }
        } catch (NoSuchAlgorithmException e) {
            logger.warn("Couldn't encrypt password value.", e);
        }
        return hashText;
    }

}
