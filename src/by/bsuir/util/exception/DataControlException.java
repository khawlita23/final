package by.bsuir.util.exception;

/**
 * {@code DataControlException} is thrown in case at least one of the values is null or empty.
 *
 * @author Hamster
 * @since 1.0
 */
public class DataControlException extends Exception {

    public DataControlException() {
        super();
    }

    public DataControlException(String message) {
        super(message);
    }

    public DataControlException(String message, Throwable cause) {
        super(message, cause);
    }
}
