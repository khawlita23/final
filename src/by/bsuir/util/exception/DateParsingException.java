package by.bsuir.util.exception;

/**
 * {@code DateParsingException} is thrown in case there's problem parsing the date.
 *
 * @author Hamster
 * @since 1.0
 */
public class DateParsingException extends Exception {

    public DateParsingException() {
        super();
    }

    public DateParsingException(String message) {
        super(message);
    }

    public DateParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
