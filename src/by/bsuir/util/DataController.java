package by.bsuir.util;

import by.bsuir.util.exception.DataControlException;


/**
 * {@code DataController} is a class responsible for checking if the data is null or empty.
 *
 * @author Hamster
 * @since 1.0
 */
public final class DataController {

    /**
     * This method checks if an array of {@link String} objects is empty.
     *
     * @param stringArray array of {@link String} objects to be checked.
     *
     * @throws DataControlException if at least one of the strings is empty.
     *
     * @since 1.0
     */
    public static void isEmptyStringArray(String[] stringArray) throws DataControlException {
        for (String string : stringArray) {
            if (string.isEmpty()) {
                throw new DataControlException("At least one string is empty in the array");
            }
        }
    }

    /**
     * This method checks if an object objects is null.
     *
     * @param object an {@link Object} to be checked.
     *
     * @throws DataControlException if the object is null.
     *
     * @since 1.0
     */
    public static void isNull(Object object) throws DataControlException {

        if (object == null) {
            throw new DataControlException("Object is null.");
        }
    }

    /**
     * This method checks if one or more objects are null.
     *
     * @param objects one or moe objects to be checked.
     *
     * @throws DataControlException if at least one of the objects is null.
     *
     * @since 1.0
     */
    public static void areNull(Object... objects) throws DataControlException {
        for (Object obj : objects) {
            if (obj == null) {
                throw new DataControlException("At least one of the objects is null.");
            }
        }
    }

    /**
     * This method checks if one or more {@link String} objects are empty.
     *
     * @param strings one or more {@link String} objects to be checked.
     *
     * @throws DataControlException if at least one of the strings is empty.
     *
     * @since 1.0
     */
    public static void areEmptyStrings(String... strings) throws DataControlException {
        for (String string : strings) {
            if(string.isEmpty()) {
                throw new DataControlException("At least one string is empty.");
            }
        }
    }

    /**
     * This method checks if the given {@link String} is empty.
     *
     * @param string {@link String} to be checked.
     *
     * @throws DataControlException if given string is empty.
     *
     * @since 1.0
     */
    public static void isEmptyString(String string) throws DataControlException {
        if (string.isEmpty()) {
            throw new DataControlException("String is empty.");
        }
    }
}
