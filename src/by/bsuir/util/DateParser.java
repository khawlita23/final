package by.bsuir.util;

import by.bsuir.util.exception.DateParsingException;
import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Class {@code DateParser} is an util class which is responsible for converting
 * String into java.sql.Date.
 *
 * @author Hamster
 * @since 1.0
 */
public class DateParser {

    private static final String DATE_FORMAT = "dd-MM-yyyy";
    private static Logger logger = Logger.getLogger(DateParser.class.getName());

    /**
     * This method parses {@code String} representaion of the date into {@link java.sql.Date} object.
     *
     * @param date {@code String} date.
     *
     * @throws DateParsingException in case a {@link ParseException} is thrown.
     *
     * @since 1.0
     */
    public static java.sql.Date parseDate(String date) throws DateParsingException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        java.util.Date utilDate = null;
        java.sql.Date birthDate = null;
        try {
            utilDate = dateFormat.parse(date);
            birthDate = new java.sql.Date(utilDate.getTime());
        } catch (ParseException e) {
            throw new DateParsingException("Failed parsing date from String", e);
        }
        return birthDate;
    }

}
