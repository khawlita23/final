package by.bsuir.controller;

/**
 * Class {@code SessionRequestParameter} holds names of session and request parameters and attributes.
 *
 * @author Hamster
 * @since 1.0
 */
public class SessionRequestParameter {

    public static final String ERROR_INFO = "errorInfo";
    public static final String PARAM_NAME_CATEGORY = "category";
    public static final String PARAM_NAME_EVENT_NAME = "eventName";
    public static final String PARAM_NAME_EVENT_PLACE = "eventPlace";
    public static final String PARAM_NAME_EVENT_ID = "eventId";
    public static final String PARAM_NAME_MATCH_NAME = "matchName";
    public static final String PARAM_NAME_MATCH_DATE = "matchDate";
    public static final String PARAM_NAME_LANG = "lang";
    public static final String PARAM_NAME_USER_LOCALE = "userLocale";
    public static final String PARAM_NAME_CURRENT_PASSWORD = "currentPassword";
    public static final String PARAM_NAME_NEW_PASSWORD = "newPassword";
    public static final String PARAM_NAME_USER = "user";
    public static final String PARAM_NAME_PASSWORD_CHANGED = "passwordChanged";
    public static final String PARAM_NAME_WRONG_PASSWORD = "wrongPassword";
    public static final String PARAM_NAME_CLIENT_ID = "clientId";
    public static final String PARAM_NAME_MATCH_ID = "matchId";
    public static final String PARAM_NAME_SUCCESS = "success";
    public static final String PARAM_NAME_BETS_LIST = "betsList";
    public static final String PARAM_NAME_OUTCOME_ID = "outcomeId";
    public static final String PARAM_NAME_EVENTS_LIST = "eventsList";
    public static final String PARAM_NAME_ROLE = "role";
    public static final String PARAM_NAME_CLIENT = "client";
    public static final String PARAM_NAME_LOGIN = "login";
    public static final String PARAM_NAME_PASSWORD = "password";
    public static final String PARAM_NAME_ADMIN = "admin";
    public static final String PARAM_NAME_STAKE = "stake";
    public static final String PARAM_NAME_CLIENTS_LIST = "clientsList";
    public static final String PARAM_NAME_TO_PAGE = "toPage";
    public static final String PARAM_NAME_CURRENT_PAGE = "currentPage";
    public static final String PARAM_NAME_NUMBER_OF_PAGES = "numberOfPages";
    public static final String PARAM_NAME_TOTAL_STAKES_MAP = "totalStakesMap";
    public static final String PARAM_NAME_OUTCOMES_LIST = "outcomesList";
    public static final String PARAM_NAME_MATCHES_LIST = "matchesList";
    public static final String PARAM_NAME_FNAME = "fname";
    public static final String PARAM_NAME_LNAME = "lname";
    public static final String PARAM_NAME_BDAY = "bday";
    public static final String PARAM_NAME_BMONTH = "bmonth";
    public static final String PARAM_NAME_BYEAR = "byear";
    public static final String PARAM_NAME_EMAIL = "email";
    public static final String PARAM_NAME_PSWRD1 = "pswrd1";
    public static final String PARAM_NAME_MATCH = "match";
    public static final String PARAM_NAME_ALREADY_EXISTS = "alreadyExists";

}
