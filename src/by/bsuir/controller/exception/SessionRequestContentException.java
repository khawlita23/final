package by.bsuir.controller.exception;

/**
 * Created by Hamster on 01.06.2017.
 */
public class SessionRequestContentException extends Exception {

    public SessionRequestContentException() {
        super();
    }

    public SessionRequestContentException(String message) {
        super(message);
    }

    public SessionRequestContentException(String message, Throwable cause) {
        super(message, cause);
    }

    public SessionRequestContentException(Throwable cause) {
        super(cause);
    }
}
