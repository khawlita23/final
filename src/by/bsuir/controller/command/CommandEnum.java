package by.bsuir.controller.command;

import by.bsuir.controller.command.impl.*;

/**
 * Enumeration {@code CommandEnum} returns needed command instance by its name.
 *
 * @author Hamster
 * @since 1.0
 */
public enum CommandEnum {

    LOGIN {
        {
            this.command = new LogInCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogOutCommand();
        }
    },
    SIGNUP {
        {
            this.command = new SignUpCommand();
        }
    },
    GOTOLOGIN {
        {
            this.command = new GoToLogInCommand();
        }
    },
    GOTOSIGNUP {
        {
            this.command = new GoToSignUpCommand();
        }
    },
    GOTOMATCHES {
        {
            this.command = new GoToMatchesCommand();
        }
    },
    MAKEABET {
        {
            this.command = new MakeABetCommand();
        }
    },
    CHANGEPASSWORD {
        {
            this.command = new ChangePasswordCommand();
        }
    },
    GOTOUSERPROFILE {
        {
            this.command = new GoToUserProfileCommand();
        }
    },
    CHANGELANG {
        {
            this.command = new ChangeLangCommand();
        }
    },
    GOTOMANAGEMENT {
        {
            this.command = new GoToManagementCommand();
        }
    },
    GOTOEDITMATCH {
        {
            this.command = new GoToEditMatchCommand();
        }
    },
    EDITMATCH {
        {
            this.command = new EditMatchCommand();
        }
    },
    GOTOADDMATCH {
        {
            this.command = new GoToAddMatchCommand();
        }
    },
    ADDMATCH {
        {
            this.command = new AddMatchCommand();
        }
    },
    GOTOADDEVENT {
        {
            this.command = new GoToAddEventCommand();
        }
    },
    ADDEVENT {
        {
            this.command = new AddEventCommand();
        }
    },
    DELETEBET {
        {
            this.command = new DeleteBetCommand();
        }
    },
    GOTORULES {
        {
            this.command = new GoToRulesCommand();
        }
    },
    GOTORESULTS {
        {
            this.command = new GoToResultsCommand();
        }
    };

    ActionCommand command;

    /**
     * This method returns current set {@link ActionCommand} implementation.
     *
     * @return current set {@link ActionCommand} implementation.
     *
     * @since 1.0
     */
    public ActionCommand getCurrentCommand() {
        return command;
    }

}
