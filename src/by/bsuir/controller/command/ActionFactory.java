package by.bsuir.controller.command;

import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.command.impl.EmptyCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.resource.MessageManager;
import org.apache.log4j.Logger;

/**
 * Class {@code ActionFactory} contains a factory method which defines a command passed by client onin request object.
 *
 * @author Hamster
 * @since 1.0
 */
public class ActionFactory {

    private static Logger logger = Logger.getLogger(ActionFactory.class.getName());
    private static final ActionFactory instance = new ActionFactory();
    /**
     * Returns an instance of {@code ActionFactory} class.
     *
     * @return instance of {@code ActionFactory} class.
     *
     * @since 1.0
     */
    public static ActionFactory getInstance() {
        return instance;
    }

    private ActionFactory() {}

    /**
     * Returns a command implementation defined by the request parameter command that is passed in {@code src} object.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return implementation of an {@code ActionCommand} interface.
     *
     * @since 1.0
     */
    public ActionCommand defineCommand(SessionRequestContent src) {

        ActionCommand current = new EmptyCommand();

        String action = null;
        try {
            action = src.getRequestParameter("command");
        } catch (SessionRequestContentException e) {
            logger.error("Couldn't define a command.", e);
        }

        if (action == null || action.isEmpty()) {
            return current;
        }

        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            src.setRequestAttribute("wrongAction", action + MessageManager.getProperty("message.wrongaction"));
        }

        return current;
    }


}
