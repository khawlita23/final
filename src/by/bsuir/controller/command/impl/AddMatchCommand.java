package by.bsuir.controller.command.impl;

import by.bsuir.bean.Event;
import by.bsuir.bean.Match;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.SessionRequestParameter;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.EventDAO;
import by.bsuir.db.dao.impl.MatchDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.servicelayer.EventService;
import by.bsuir.servicelayer.MatchService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.List;

import static by.bsuir.controller.SessionRequestParameter.*;

/**
 * Class {@code AddMatchCommand} represents a command for adding a new match by the administrator.
 *
 * @author Hamster
 * @since 1.0
 */
public class AddMatchCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(AddMatchCommand.class.getName());

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}.
     * Processes command to add match.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = null;
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        MatchService matchService = serviceFactory.getMatchService();
        EventService eventService = serviceFactory.getEventService();
        try {
            String eventIdParam = src.getRequestParameter(PARAM_NAME_EVENT_ID);
            String matchName = src.getRequestParameter(PARAM_NAME_MATCH_NAME);
            String matchDate = src.getRequestParameter(PARAM_NAME_MATCH_DATE);
            if (matchService.doesAlreadyExist(matchName)) {
                src.setRequestAttribute(PARAM_NAME_ALREADY_EXISTS, true);
            } else {
                matchService.addMatch(eventIdParam, matchName, matchDate);
                src.setRequestAttribute(PARAM_NAME_SUCCESS, true);
            }

            List<Event> eventsList = eventService.findAll();
            src.setRequestAttribute(PARAM_NAME_EVENTS_LIST, eventsList);
            page = ConfigurationManager.getProperty("path.page.addmatch");
        } catch (SessionRequestContentException e) {
            logger.error("Couldn't get request content.", e);
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem", e);
        }
        return page;
    }
}
