package by.bsuir.controller.command.impl;

import by.bsuir.bean.Client;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.ClientDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.resource.MessageManager;
import by.bsuir.servicelayer.ClientService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.DataController;
import by.bsuir.util.DateParser;
import by.bsuir.util.exception.DataControlException;
import by.bsuir.util.exception.DateParsingException;
import org.apache.log4j.Logger;

import static by.bsuir.controller.SessionRequestParameter.*;

/**
 * Class {@code SignUpCommand} represents a command for signing up a new client.
 *
 * @author Hamster
 * @since 1.0
 */
public class SignUpCommand implements ActionCommand {

    private Logger logger = Logger.getLogger(SignUpCommand.class.getName());

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}
     * Processes command to sign up a new client.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = ConfigurationManager.getProperty("path.page.signup");
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        ClientService clientService = serviceFactory.getClientService();
        Client client = new Client();
        String bDay = null;
        String bMonth = null;
        String bYear = null;
        try {
            String fName = src.getRequestParameter(PARAM_NAME_FNAME);
            String lName = src.getRequestParameter(PARAM_NAME_LNAME);
            String email = src.getRequestParameter(PARAM_NAME_EMAIL);
            String login = src.getRequestParameter(PARAM_NAME_LOGIN);
            String password = src.getRequestParameter(PARAM_NAME_PSWRD1);
            bDay = src.getRequestParameter(PARAM_NAME_BDAY);
            bMonth = src.getRequestParameter(PARAM_NAME_BMONTH);
            bYear = src.getRequestParameter(PARAM_NAME_BYEAR);
            if (!clientService.checkLogin(login)) {
                clientService.addClient(fName, lName, email, login, password, bDay, bMonth, bYear);
                src.setRequestAttribute(PARAM_NAME_SUCCESS, true);
                page = ConfigurationManager.getProperty("path.page.login");

            } else {
                src.setRequestAttribute("loginduplicateerror", MessageManager.getProperty("message.loginduplicate"));
            }
        } catch (SessionRequestContentException e) {
            logger.error("Couldn't get request content.", e);
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem", e);
            src.setRequestAttribute("signinerror", MessageManager.getProperty("message.signinerror"));
        }

        return page;
    }
}
