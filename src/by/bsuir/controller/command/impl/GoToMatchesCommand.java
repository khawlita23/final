package by.bsuir.controller.command.impl;

import by.bsuir.bean.Match;
import by.bsuir.bean.Outcome;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.BetDAO;
import by.bsuir.db.dao.impl.MatchDAO;
import by.bsuir.db.dao.impl.OutcomeDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.servicelayer.BetService;
import by.bsuir.servicelayer.MatchService;
import by.bsuir.servicelayer.OutcomeService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

import static by.bsuir.controller.SessionRequestParameter.*;

/**
 * Class {@code GoToMatchesCommand} represents a command for going to an {@code matches.jsp} or
 * {@code adminmatches.jsp} page.
 *
 * @author Hamster
 * @since 1.0
 */
public class GoToMatchesCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(GoToMatchesCommand.class.getName());
    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}.
     * Processes command to an {@code matches.jsp} or
     * {@code adminmatches.jsp} page.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = null;

        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        MatchService matchService = serviceFactory.getMatchService();
        BetService betService = serviceFactory.getBetService();
        OutcomeService outcomeService = serviceFactory.getOutcomeService();
        try {
            String role = (String) src.getSessionAttribute(PARAM_NAME_ROLE);
            String category = src.getRequestParameter(PARAM_NAME_CATEGORY);
            List<Match> matchesList;
            Map<Integer, Double> totalStakes;
            List<Outcome> outcomesList;
            if (role != null && role.equals("admin")) {
                matchesList = matchService.findAllByCategory(category);
                totalStakes = betService.getStakesSums();
                src.setSessionAttribute(PARAM_NAME_TOTAL_STAKES_MAP, totalStakes);
                page = ConfigurationManager.getProperty("path.page.adminmatches");
            } else {
                matchesList = matchService.findOpenByCategory(category);
                outcomesList = outcomeService.findAll();
                src.setSessionAttribute(PARAM_NAME_OUTCOMES_LIST, outcomesList);
                page = ConfigurationManager.getProperty("path.page.matches");
            }
            src.setSessionAttribute(PARAM_NAME_CATEGORY, category);
            src.setSessionAttribute(PARAM_NAME_MATCHES_LIST, matchesList);

        } catch (SessionRequestContentException e) {
            logger.warn("Couldn't get session or request content", e);
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem", e);
        }
        return page;
    }
}
