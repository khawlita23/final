package by.bsuir.controller.command.impl;

import by.bsuir.bean.Event;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.SessionRequestParameter;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.db.dao.impl.EventDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.servicelayer.EventService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Class {@code GoToAddMatchCommand} represents a command for going to an {@code addevent.jsp} page.
 *
 * @author Hamster
 * @since 1.0
 */
public class GoToAddMatchCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(GoToAddMatchCommand.class.getName());

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}
     * Processes command to {@code addevent.jsp} page.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {

        String page;
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        EventService eventService = serviceFactory.getEventService();
        try {
            List<Event> eventsList = eventService.findAll();
            src.setRequestAttribute(SessionRequestParameter.PARAM_NAME_EVENTS_LIST, eventsList);
        } catch (ServiceLayerException e) {
            logger.error("Service layer proplem when tryint to get all events", e);
        }
        page = ConfigurationManager.getProperty("path.page.addmatch");
        return page;
    }
}
