package by.bsuir.controller.command.impl;

import by.bsuir.bean.Bet;
import by.bsuir.bean.Client;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.SessionRequestParameter;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.BetDAO;
import by.bsuir.db.dao.impl.ClientDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.servicelayer.BetService;
import by.bsuir.servicelayer.ClientService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.DataController;
import org.apache.log4j.Logger;

import java.util.List;

import static by.bsuir.controller.SessionRequestParameter.*;

/**
 * Class {@code GoToUserProfileCommand} represents a command for going to an {@code userprofile.jsp} page.
 *
 * @author Hamster
 * @since 1.0
 */
public class GoToUserProfileCommand implements ActionCommand {

    Logger logger = Logger.getLogger(GoToUserProfileCommand.class.getName());

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}
     * Processes command to {@code userprofile.jsp} page.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = null;
        List<Bet> bets;
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        ClientService clientService = serviceFactory.getClientService();
        BetService betService = serviceFactory.getBetService();
        try {
            String role = (String) src.getSessionAttribute(PARAM_NAME_ROLE);
            String clientIdParam = src.getRequestParameter(PARAM_NAME_CLIENT_ID);
            String login = (String) src.getSessionAttribute(PARAM_NAME_USER);
            Client client;
            if (role != null && role.equals("admin")) {
                client = clientService.findById(clientIdParam);
            } else {
                client = clientService.findClientByLogin(login);
            }
            bets = betService.findByClientId(String.valueOf(client.getClientId()));
            src.setSessionAttribute(PARAM_NAME_CLIENT, client);
            src.setSessionAttribute(PARAM_NAME_BETS_LIST, bets);

            page = ConfigurationManager.getProperty("path.page.userprofile");
        } catch (SessionRequestContentException e) {
            logger.warn("Couldn't get session or request content.", e);
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem", e);
        }
        return page;
    }
}
