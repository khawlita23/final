package by.bsuir.controller.command.impl;

import by.bsuir.bean.Match;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.BetDAO;
import by.bsuir.db.dao.impl.MatchDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.servicelayer.BetService;
import by.bsuir.servicelayer.MatchService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

import static by.bsuir.controller.SessionRequestParameter.*;

/**
 * Class {@code GoToResultsCommand} represents a command for going to an {@code results.jsp} or
 * {@code results.jsp} page.
 *
 * @author Hamster
 * @since 1.0
 */
public class GoToResultsCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(GoToResultsCommand.class.getName());
    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}.
     * Processes command to an {@code results.jsp} or
     * {@code results.jsp} page that show the results of matches of given category.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = null;
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        MatchService matchService = serviceFactory.getMatchService();
        BetService betService = serviceFactory.getBetService();
        List<Match> matchesList;
        Map<Integer, Double> totalStakes;
        Map<Integer, Double> odds;
        try {
            String category = (String) src.getRequestParameter(PARAM_NAME_CATEGORY);
            matchesList = matchService.findFinishedByCategory(category);
            totalStakes = betService.getStakesSums();

            src.setRequestAttribute(PARAM_NAME_CATEGORY, category);
            src.setRequestAttribute(PARAM_NAME_MATCHES_LIST, matchesList);
            src.setRequestAttribute(PARAM_NAME_TOTAL_STAKES_MAP, totalStakes);
            page = ConfigurationManager.getProperty("path.page.results");
        } catch (SessionRequestContentException e) {
            logger.error("Problem receiving session content.", e);
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem", e);
        }

        return page;
    }
}
