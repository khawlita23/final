package by.bsuir.controller.command.impl;

import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.SessionRequestParameter;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;
import org.apache.log4j.Logger;

/**
 * Class {@code ChangeLangCommand} represents a command for changing user's locale (language).
 *
 * @author Hamster
 * @since 1.0
 */
public class ChangeLangCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(ChangeLangCommand.class.getName());

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}.
     * Processes command to change language.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = ConfigurationManager.getProperty("path.page.main");;
        try {
            String lang = src.getRequestParameter(SessionRequestParameter.PARAM_NAME_LANG);
            DataController.isNull(lang);
            DataController.isEmptyString(lang);
            src.setSessionAttribute(SessionRequestParameter.PARAM_NAME_USER_LOCALE, lang.toLowerCase() + "-" + lang);
        } catch (SessionRequestContentException e) {
            logger.error("Couldn't get session or request content.", e);
        } catch (DataControlException e) {
            logger.error("Data received from client in request or session is either empty or null", e);
        }
        return page;
    }
}
