package by.bsuir.controller.command.impl;

import by.bsuir.bean.Bet;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.SessionRequestParameter;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.BetDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.servicelayer.BetService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Class {@code DeleteBetCommand} represents a command for dismissing bet by client.
 *
 * @author Hamster
 * @since 1.0
 */
public class DeleteBetCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(DeleteBetCommand.class.getName());

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}.
     * Processes command to dismiss a made bet.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = ConfigurationManager.getProperty("path.page.userprofile");
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        BetService betService = serviceFactory.getBetService();
        try {
            String clientIdParam = src.getRequestParameter(SessionRequestParameter.PARAM_NAME_CLIENT_ID);
            String matchIdParam = src.getRequestParameter(SessionRequestParameter.PARAM_NAME_MATCH_ID);

            if(betService.deleteBet(clientIdParam, matchIdParam)) {
                src.setRequestAttribute(SessionRequestParameter.PARAM_NAME_SUCCESS, true);
            } else {
                src.setRequestAttribute(SessionRequestParameter.PARAM_NAME_SUCCESS, false);
            }
            List<Bet> bets = betService.findByClientId(clientIdParam);
            src.setSessionAttribute(SessionRequestParameter.PARAM_NAME_BETS_LIST, bets);
        } catch (SessionRequestContentException e) {
            logger.warn("Couldn't get session or request content.", e);
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem when deleting bet", e);
        }
        return page;
    }
}
