package by.bsuir.controller.command.impl;

import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.resource.ConfigurationManager;

/**
 * Class {@code GoToSignUpCommand} represents a command for going to an {@code signup.jsp} page.
 *
 * @author Hamster
 * @since 1.0
 */
public class GoToSignUpCommand implements ActionCommand {

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}
     * Processes command to {@code signup.jsp} page.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
       return ConfigurationManager.getProperty("path.page.signup");
    }

}
