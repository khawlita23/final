package by.bsuir.controller.command.impl;

import by.bsuir.bean.Match;
import by.bsuir.bean.Outcome;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.MatchDAO;
import by.bsuir.db.dao.impl.OutcomeDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.servicelayer.MatchService;
import by.bsuir.servicelayer.OutcomeService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;
import org.apache.log4j.Logger;

import java.util.List;

import static by.bsuir.controller.SessionRequestParameter.*;


/**
 * Class {@code GoToEditMatchCommand} represents a command for going to an {@code editmatch.jsp} page.
 *
 * @author Hamster
 * @since 1.0
 */
public class GoToEditMatchCommand implements ActionCommand {

    private static final Logger logger = Logger.getLogger(GoToEditMatchCommand.class.getName());

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}
     * Processes command to {@code editmatch.jsp} page.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = ConfigurationManager.getProperty("path.page.main");
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        MatchService matchService = serviceFactory.getMatchService();
        OutcomeService outcomeService = serviceFactory.getOutcomeService();
        try {
            String matchIdParam = src.getRequestParameter(PARAM_NAME_MATCH_ID);
            Match match = matchService.findById(matchIdParam);
            List<Outcome> outcomes = outcomeService.findAll();
            src.setRequestAttribute(PARAM_NAME_MATCH, match);
            src.setSessionAttribute(PARAM_NAME_OUTCOMES_LIST, outcomes);
            page = ConfigurationManager.getProperty("path.page.editmatch");
        } catch (SessionRequestContentException e) {
            logger.warn("Couldn't get request content.", e);
            page = ConfigurationManager.getProperty("path.page.error");
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem", e);
        }
        return page;
    }
}
