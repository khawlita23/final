package by.bsuir.controller.command.impl;

import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.SessionRequestParameter;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.ClientDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.resource.MessageManager;
import by.bsuir.servicelayer.AdminService;
import by.bsuir.servicelayer.ClientService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.exception.DataControlException;
import org.apache.log4j.Logger;

import static by.bsuir.controller.SessionRequestParameter.*;

/**
 * Class {@code LogInCommand} represents a command for entering the system.
 *
 *
 * @author Hamster
 * @since 1.0
 */
public class LogInCommand implements ActionCommand {

    private static final Logger logger = Logger.getLogger(LogInCommand.class.getName());

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}
     * Processes command to log in as administrator or as a client from a modal or a {@code login.jsp} page.
     * If login or password is wrong error message is shown and user is redirected to the {@code login.jsp} page.
     *
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = null;
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        try {
            String login = src.getRequestParameter(PARAM_NAME_LOGIN);
            String password = src.getRequestParameter(SessionRequestParameter.PARAM_NAME_PASSWORD);
            String admin = src.getRequestParameter(SessionRequestParameter.PARAM_NAME_ADMIN);
            if (admin != null) {
                logger.debug("Start logging in as admin");
                AdminService adminService = serviceFactory.getAdminService();
                if (adminService.checkPassword(login, password)) {
                    src.setSessionAttribute(SessionRequestParameter.PARAM_NAME_USER, login);
                    src.setSessionAttribute(SessionRequestParameter.PARAM_NAME_ROLE, "admin");
                    page = ConfigurationManager.getProperty("path.page.main");
                } else {
                    src.setRequestAttribute("errorLoginPassMessage", MessageManager.getProperty("message.loginerror"));
                    page = ConfigurationManager.getProperty("path.page.login");
                }
            } else {
                logger.debug("Start logging in as client");
                ClientService clientService = serviceFactory.getClientService();
                if (clientService.checkPassword(login, password)) {
                    src.setSessionAttribute(SessionRequestParameter.PARAM_NAME_USER, login);
                    src.setSessionAttribute(SessionRequestParameter.PARAM_NAME_ROLE, "client");
                    page = ConfigurationManager.getProperty("path.page.main");
                } else {
                    src.setRequestAttribute("errorLoginPassMessage", MessageManager.getProperty("message.loginerror"));
                    page = ConfigurationManager.getProperty("path.page.login");
                }
            }
        } catch (SessionRequestContentException e) {
            logger.warn("Couldn't get session or request content.", e);
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem", e);
        }
        return page;
    }
}
