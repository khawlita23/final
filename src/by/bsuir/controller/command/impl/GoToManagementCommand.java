package by.bsuir.controller.command.impl;

import by.bsuir.bean.Client;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.SessionRequestParameter;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.ClientDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.servicelayer.ClientService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.PerPageCounter;
import org.apache.log4j.Logger;

import java.util.List;

import static by.bsuir.controller.SessionRequestParameter.*;

/**
 * Class {@code GoToEditMatchCommand} represents a command for going to an {@code editmatch.jsp} page.
 *
 * @author Hamster
 * @since 1.0
 */
public class GoToManagementCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(GoToManagementCommand.class.getName());
    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}
     * Processes command to {@code editmatch.jsp} page.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = ConfigurationManager.getProperty("path.page.main");
        int toPage = 1;
        int recordsPerPage = Integer.parseInt(ConfigurationManager.getProperty("pagination.clientsperpage"));
        int numberOfPages;
        List<Client> clientsPerPage;
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        ClientService clientService = serviceFactory.getClientService();
        try {
            List<Client> clientsList = clientService.findAll();
            String toPageParam = src.getRequestParameter(PARAM_NAME_TO_PAGE);
            if (toPageParam != null) {
                toPage = Integer.parseInt(toPageParam);
            }

            numberOfPages = (int) Math.ceil(clientsList.size() * 1.0 / recordsPerPage);
            clientsPerPage = PerPageCounter.getPerPage(clientsList, recordsPerPage, toPage, numberOfPages);

            src.setRequestAttribute(PARAM_NAME_CLIENTS_LIST, clientsPerPage);
            src.setRequestAttribute(PARAM_NAME_CURRENT_PAGE, toPage);
            src.setRequestAttribute(PARAM_NAME_NUMBER_OF_PAGES, numberOfPages);
            page = ConfigurationManager.getProperty("path.page.management");
        } catch (SessionRequestContentException e) {
            logger.error("Null data received from client", e);
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem", e);
        }
        return page;
    }
}
