package by.bsuir.controller.command.impl;

import by.bsuir.bean.Event;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.SessionRequestParameter;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.EventDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.servicelayer.EventService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;
import org.apache.log4j.Logger;

import static by.bsuir.controller.SessionRequestParameter.*;

/**
 * Class {@code AddEventCommand} represents a command for adding a new event by the administrator.
 *
 * @author Hamster
 * @since 1.0
 */
public class AddEventCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(AddEventCommand.class.getName());

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}.
     * Processes command to add event.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     *  @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = ConfigurationManager.getProperty("path.page.index");
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        EventService eventService = serviceFactory.getEventService();
        try {
            String category = src.getRequestParameter(PARAM_NAME_CATEGORY);
            String eventName = src.getRequestParameter(PARAM_NAME_EVENT_NAME);
            String eventPlace = src.getRequestParameter(PARAM_NAME_EVENT_PLACE);
            if (eventService.doesAlreadyExist(eventName)) {
                src.setRequestAttribute(PARAM_NAME_ALREADY_EXISTS, true);
            } else {
                eventService.addEvent(category, eventName, eventPlace);
                src.setRequestAttribute(PARAM_NAME_SUCCESS, true);
            }
            page = ConfigurationManager.getProperty("path.page.addevent");
        } catch (SessionRequestContentException e) {
            logger.error("Couldn't get session or request content.", e);
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem", e);
        }
        return page;
    }
}
