package by.bsuir.controller.command.impl;

import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.resource.ConfigurationManager;

/**
 * Created by Hamster on 02.05.2017.
 */
public class LogOutCommand implements ActionCommand {
    @Override
    public String execute(SessionRequestContent src) {
        String page = ConfigurationManager.getProperty("path.page.index");
        src.setInvalidateSession(true);
        return page;
    }
}
