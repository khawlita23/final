package by.bsuir.controller.command.impl;

import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.resource.ConfigurationManager;

/**
 * Class {@code EmptyCommand} represents an empty command.
 *
 * @author Hamster
 * @since 1.0
 */
public class EmptyCommand implements ActionCommand {

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}
     * It is an empty command, it returns index.jsp address
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        return ConfigurationManager.getProperty("path.page.index");
    }
}
