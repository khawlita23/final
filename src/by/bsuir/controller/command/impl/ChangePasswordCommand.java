package by.bsuir.controller.command.impl;

import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.SessionRequestParameter;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.ClientDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.resource.MessageManager;
import by.bsuir.servicelayer.ClientService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;
import org.apache.log4j.Logger;

/**
 * Class {@code ChangePasswordCommand} represents a command for changing password by client.
 *
 * @author Hamster
 * @since 1.0
 */
public class ChangePasswordCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(ChangePasswordCommand.class.getName());

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}.
     * Processes command to change password.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {

        String page = ConfigurationManager.getProperty("path.page.userprofile");
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        ClientService clientService = serviceFactory.getClientService();
        try {
            String login = (String) src.getSessionAttribute(SessionRequestParameter.PARAM_NAME_USER);
            String currentPassword = src.getRequestParameter(SessionRequestParameter.PARAM_NAME_CURRENT_PASSWORD);
            String newPassword = src.getRequestParameter(SessionRequestParameter.PARAM_NAME_NEW_PASSWORD);
            if (clientService.checkPassword(login, currentPassword)) {
                clientService.changePassword(login, newPassword);
                src.setRequestAttribute(SessionRequestParameter.PARAM_NAME_PASSWORD_CHANGED,
                        MessageManager.getProperty("message.passwordchanged"));
            } else {
                src.setRequestAttribute(SessionRequestParameter.PARAM_NAME_WRONG_PASSWORD,
                        MessageManager.getProperty("message.wrongpassword"));
            }
        } catch (SessionRequestContentException e) {
            logger.error("Couldn't get request or session content.");
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem when trying to change password", e);
        }
        return page;
    }
}
