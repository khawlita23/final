package by.bsuir.controller.command.impl;

import by.bsuir.bean.Event;
import by.bsuir.bean.Match;
import by.bsuir.bean.Outcome;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.SessionRequestParameter;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.MatchDAO;
import by.bsuir.db.dao.impl.OutcomeDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.servicelayer.MatchService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;
import org.apache.log4j.Logger;

import java.sql.Timestamp;

import static by.bsuir.controller.SessionRequestParameter.*;

/**
 * Class {@code EditMatchCommand} represents a command for editing match by administrator.
 *
 * @author Hamster
 * @since 1.0
 */
public class EditMatchCommand implements ActionCommand {

    private static final Logger logger = Logger.getLogger(EditMatchCommand.class.getName());

    /**
     * Overrides method {@link ActionCommand#execute(SessionRequestContent)}.
     * Processes command to edit a match.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    @Override
    public String execute(SessionRequestContent src) {
        String page = ConfigurationManager.getProperty("path.page.editmatch");
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        MatchService matchService = serviceFactory.getMatchService();
        try {
            String matchIdParam = src.getRequestParameter(PARAM_NAME_MATCH_ID);
            String matchDate = src.getRequestParameter(PARAM_NAME_MATCH_DATE);
            String matchName = src.getRequestParameter(PARAM_NAME_MATCH_NAME);
            String outcomeIdParam = src.getRequestParameter(PARAM_NAME_OUTCOME_ID);
            String eventName = src.getRequestParameter(PARAM_NAME_EVENT_NAME);
            String eventPlace = src.getRequestParameter(PARAM_NAME_EVENT_PLACE);

            Match match = matchService.editMatch(matchIdParam, matchDate, matchName,
                    outcomeIdParam, eventName, eventPlace);
            src.setRequestAttribute(PARAM_NAME_MATCH, match);
            src.setRequestAttribute(PARAM_NAME_SUCCESS, true);
        } catch (SessionRequestContentException e) {
            logger.warn("Couldn't get request content.", e);
        } catch (ServiceLayerException e) {
            logger.error("Service layer problem when editing match", e);
        }
        return page;
    }
}
