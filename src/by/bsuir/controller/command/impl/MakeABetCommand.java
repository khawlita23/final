package by.bsuir.controller.command.impl;

import by.bsuir.bean.Bet;
import by.bsuir.bean.Client;
import by.bsuir.controller.SessionRequestContent;
import by.bsuir.controller.SessionRequestParameter;
import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.db.dao.impl.BetDAO;
import by.bsuir.db.dao.impl.ClientDAO;
import by.bsuir.db.exception.DAOException;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.resource.MessageManager;
import by.bsuir.servicelayer.BetService;
import by.bsuir.servicelayer.ClientService;
import by.bsuir.servicelayer.exception.ServiceLayerException;
import by.bsuir.servicelayer.factory.AbstractFactory;
import by.bsuir.servicelayer.factory.ServiceFactory;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;
import org.apache.log4j.Logger;

import java.util.List;

import static by.bsuir.controller.SessionRequestParameter.*;

/**
 * Created by Hamster on 13.05.2017.
 */
public class MakeABetCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(MakeABetCommand.class.getName());

    @Override
    public String execute(SessionRequestContent src) {
        String page = ConfigurationManager.getProperty("path.page.matches");
        String user = null;
        AbstractFactory serviceFactory = ServiceFactory.getInstance();
        try {
            user = (String) src.getSessionAttribute(SessionRequestParameter.PARAM_NAME_USER);

            if (user == null || user.isEmpty()) {
                page = ConfigurationManager.getProperty("path.page.login");
            } else {
                String matchIdParam = src.getRequestParameter(SessionRequestParameter.PARAM_NAME_MATCH_ID);
                String outcomeIdParam = src.getRequestParameter(SessionRequestParameter.PARAM_NAME_OUTCOME_ID);
                String stakeParam = src.getRequestParameter(SessionRequestParameter.PARAM_NAME_STAKE);
                ClientService clientService = serviceFactory.getClientService();
                BetService betService = serviceFactory.getBetService();
                int clientId = clientService.findIdByLogin(user);
                if (betService.doesAlreadyExist(String.valueOf(clientId), matchIdParam)) {
                    src.setRequestAttribute(PARAM_NAME_ALREADY_EXISTS, true);
                } else {
                    betService.addBet(String.valueOf(clientId), matchIdParam, stakeParam, outcomeIdParam);
                    List<Bet> clientsBets = betService.findByClientId(String.valueOf(clientId));
                    Client client = clientService.findById(String.valueOf(clientId));
                    src.setSessionAttribute(PARAM_NAME_BETS_LIST, clientsBets);
                    src.setSessionAttribute(PARAM_NAME_CLIENT, client);
                    page = ConfigurationManager.getProperty("path.page.userprofile");
                }

            }
        } catch (SessionRequestContentException e) {
            logger.error("Couldn't get request content.", e);
        } catch (NumberFormatException e) {
            logger.warn("Failed parsing the value of bet stake", e);
            src.setRequestAttribute("bettingError", MessageManager.getProperty("message.bettingstakeerror"));
            String category = null;
            try {
                category = src.getRequestParameter(PARAM_NAME_CATEGORY);
            } catch (SessionRequestContentException e1) {
                logger.error("Couldn't get request content.", e1);
            }
            src.setSessionAttribute(PARAM_NAME_CATEGORY, category);
        } /*catch (DAOException e) {
            logger.warn("Failed to add new bet to the database.", e);
            src.setRequestAttribute("error", MessageManager.getProperty("message.bettingtwice"));
            page = ConfigurationManager.getProperty("path.page.index");
        } catch (DataControlException e) {
            logger.error("Null or empty data received from client in request or session content", e);
        }*/ catch (ServiceLayerException e) {
            logger.error("Service layer problem", e);
        }

        return page;
    }
}
