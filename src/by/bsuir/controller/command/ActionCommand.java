package by.bsuir.controller.command;

import by.bsuir.controller.SessionRequestContent;

/**
 * {@code ActionCommand} is an interface that has only one method {@code execute()}.
 *
 * @author Hamster
 * @since 1.0
 */
public interface ActionCommand {

    /**
     * This method is overriden by command implementations.
     *
     * @param src {@link SessionRequestContent} object, that contains request data.
     *
     * @return String value of JSP page name.
     *
     * @since 1.0
     */
    String execute(SessionRequestContent src);

}
