package by.bsuir.controller;

import by.bsuir.db.connectionpool.ConnectionPool;
import by.bsuir.db.exception.ConnectionPoolException;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Class {@code ApplicationData} implements {@see ServletContextListener}.
 * It's used to initialize and release connection pool.
 *
 * @author Hamster
 * @since 1.0
 */
@WebListener
public class ApplicationData implements ServletContextListener {

    private static Logger logger = Logger.getLogger(ApplicationData.class.getName());

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.info("Initializing context...");
        ConnectionPool pool = ConnectionPool.getInstance();
        try {
            pool.initPool();
        } catch (ConnectionPoolException e) {
            logger.warn("Failed initializing connection pool.", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        logger.info("Destroying context...");
        ConnectionPool pool = ConnectionPool.getInstance();
//        pool.clearConnectionsQueue();
        pool.destroy();
    }
}
