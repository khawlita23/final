package by.bsuir.controller;

import by.bsuir.controller.exception.SessionRequestContentException;
import by.bsuir.util.DataController;
import by.bsuir.util.exception.DataControlException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Class {@code SessionRequestContent} is used to store some of session and request data in order to mot pass the request object
 * in different methods.
 *
 * @author Hamster
 * @since 1.0
 */
public class SessionRequestContent {

    private Map<String, Object> requestAttributes;
    private Map<String, String[]> requestParameters;
    private Map<String, Object> sessionAttributes;
    private boolean invalidateSession;
    private HttpSession session;

    /**
     * This method is capable of managing session existence.
     *
     * @param invalidate if {@code true} then session will be invalidated.
     *
     * @since 1.0
     */
    public void setInvalidateSession(boolean invalidate) {
        this.invalidateSession = invalidate;
    }

    /**
     * This method allows the controller to invalidate session.
     *
     * @return wether to invalidate session or not.
     *
     * @since 1.0
     */
    public boolean invalidateSession() {
        return invalidateSession;
    }

    /**
     * This method extracts values from the {@link HttpServletRequest} object.
     *
     * @param request {@code HttpServletRequest} instance.
     *
     * @since 1.0
     */
    public void extractValues(HttpServletRequest request) {

        this.requestAttributes = new HashMap<>();
        Enumeration<String> requestAttributeNames = request.getAttributeNames();
        while (requestAttributeNames.hasMoreElements()) {
            String name = requestAttributeNames.nextElement();
            Object attribute = request.getAttribute(name);
            requestAttributes.put(name, attribute);
        }

        this.requestParameters = request.getParameterMap();

        this.sessionAttributes = new HashMap<>();
        HttpSession session = request.getSession();
        Enumeration<String> sessionAttributeNames = session.getAttributeNames();
        while (sessionAttributeNames.hasMoreElements()) {
            String name = sessionAttributeNames.nextElement();
            Object attribute = session.getAttribute(name);
            sessionAttributes.put(name, attribute);
        }

    }

    /**
     * This method inserts attributes into the {@link HttpServletRequest} object.
     *
     * @param request {@code HttpServletRequest} instance.
     *
     * @since 1.0
     */
    public void insertAttributes(HttpServletRequest request) {

        for (Map.Entry<String, Object> param : requestAttributes.entrySet()) {
            String name = param.getKey();
            Object value = param.getValue();
            request.setAttribute(name, value);
        }

        HttpSession session = request.getSession(false);
        for (Map.Entry<String, Object> param : sessionAttributes.entrySet()) {
            String name = param.getKey();
            Object value = param.getValue();
            session.setAttribute(name, value);
        }
    }

    /**
     * Returns request parameter extracted from the {@link HttpServletRequest} object.
     *
     * @param name name of the parameter to be returned.
     *
     * @return request parameter.
     *
     * @throws SessionRequestContentException if there's something wrong with extracting parameters.
     *
     * @since 1.0
     */
    public String getRequestParameter(String name) throws SessionRequestContentException {
        String result = null;
        try {
            DataController.isNull(requestParameters);
            String[] parameterValues = requestParameters.get(name);
            if (parameterValues != null) {
                result = parameterValues[0];
            }
        } catch (DataControlException e) {
            throw new SessionRequestContentException("Request parameters map is null", e);
        }
        return result;
    }

    /**
     * Returns request parameter values extracted from the {@link HttpServletRequest} object.
     *
     * @param name name of the parameter values to be returned.
     *
     * @return String array of request parameter values.
     *
     * @throws SessionRequestContentException if there's something wrong with extracting parameters.
     *
     * @since 1.0
     */
    public String[] getRequestParameterValues(String name) throws SessionRequestContentException {

        String[] result;
        try {
            DataController.isNull(requestParameters);
            result = requestParameters.get(name);
        } catch (DataControlException e ) {
            throw new SessionRequestContentException("Request parameters map is null", e);
        }
        return result;

    }

    /**
     * Returns request attribute extracted from the {@link HttpServletRequest} object.
     *
     * @param name name of the attribute to be returned.
     *
     * @return request attribute.
     *
     * @throws SessionRequestContentException if there's something wrong with extracting parameters.
     *
     * @since 1.0
     */
    public Object getRequestAttribute(String name) throws SessionRequestContentException {

        try {
            DataController.isNull(requestAttributes);
        } catch (DataControlException e) {
            throw new SessionRequestContentException("Request attributes map is null", e);
        }
        return requestAttributes.get(name);
    }

    /**
     * Sets request attribute which then will be inserted in the {@link HttpServletRequest} object.
     *
     * @param name name of the attribute to be set.
     * @param o object to be set as an attribute.
     *
     * @since 1.0
     */
    public void setRequestAttribute(String name, Object o) {
        requestAttributes.put(name, o);
    }

    /**
     * Returns session attribute extracted from the {@link HttpSession} object.
     *
     * @param name name of the attribute to be returned.
     *
     * @return session attribute.
     *
     * @throws SessionRequestContentException if there's something wrong with extracting parameters.
     *
     * @since 1.0
     */
    public Object getSessionAttribute(String name) throws SessionRequestContentException {
        try {
            DataController.isNull(sessionAttributes);
        } catch (DataControlException e) {
            throw new SessionRequestContentException("Session attributes map is null", e);
        }
        return sessionAttributes.get(name);
    }

    /**
     * Sets session attribute which then will be inserted in the {@link HttpSession} object.
     *
     * @param name name of the attribute to be set.
     * @param o object to be set as an attribute.
     *
     * @since 1.0
     */
    public void setSessionAttribute(String name, Object o) {
        sessionAttributes.put(name, o);
    }

    /**
     * Returns session from {@link HttpServletRequest}.
     *
     * @return {@link HttpSession} object.
     *
     * @since 1.0
     */
    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

}
