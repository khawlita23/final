package by.bsuir.controller;

import by.bsuir.controller.command.ActionCommand;
import by.bsuir.controller.command.ActionFactory;
import by.bsuir.resource.ConfigurationManager;
import by.bsuir.resource.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class {@code Controller} is a Servlet of an application.
 *
 * @author Hamster
 * @since 1.0
 */
@WebServlet(name = "Controller", urlPatterns = "/controller")
public class Controller extends HttpServlet {

    private static final Logger logger = Logger.getLogger(Controller.class.getName());

    /**
     * This method is called by {@link Controller#doGet(HttpServletRequest, HttpServletResponse)}
     * and {@link Controller#doPost(HttpServletRequest, HttpServletResponse)} methods. It executes client's command
     * and redirects him to another page.
     *
     * @param request {@code HttpServletRequest} instance.
     * @param response {@code HttpServletResponse} instance.
     *
     * @throws ServletException in case client can't be forwarded to another page.
     * @throws IOException in case client can't be forwarded to another page.
     *
     * @since 1.0
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String page;
        ActionFactory client = ActionFactory.getInstance();
        SessionRequestContent src = new SessionRequestContent();
        src.extractValues(request);

        ActionCommand command = client.defineCommand(src);
        page = command.execute(src);
        src.insertAttributes(request);

        if (src.invalidateSession()) {
            request.getSession().invalidate();
        }

        if (page != null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            page = ConfigurationManager.getProperty("path.page.index");
            request.getSession().setAttribute("nullPage", MessageManager.getProperty("message.nullpage"));
            response.sendRedirect(request.getContextPath() + page);
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
